from functools import reduce
from typing import Iterator

from eavlib.gremlin.optimiser import Optimiser
from eavlib.gremlin.traversal import (Traversal, TraverserCollection, T)


class Computer:
    def __init__(self, traversal: Traversal) -> None:
        self.traversal = traversal

    def _exec(self) -> TraverserCollection[T]:
        optimiser = Optimiser(self.traversal)
        init = None

        for step in self.traversal.ast.source_instructions:
            if step.op == "withTraversers":
                init = step.args[0]
            else:
                raise NotImplementedError(
                    "source operation %s is not implemented" % step.op)

        return reduce(lambda prev, step: step.run(prev),
                      optimiser.optimise(), init)

    def run(self) -> Iterator[T]:
        return iter(map(lambda t: t.value, self._exec()))
