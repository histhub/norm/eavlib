from eavlib.graph import (
    Graph, Node, NodeLabel, NodeSelection, NodeValueMap, Relation)

from functools import partial, partialmethod
from typing import (
    Any, Dict, Generator, Generic, Iterable, Iterator, List, Optional,
    Tuple, Type, TypeVar, Union)

Number = TypeVar("Number", int, float)


def snake_case(word):
    """Make an underscored, lowercase form (snake-case) from the expression in
    the string.

    Example::

        >>> underscore("camelCase")
        "camel_case"

    This code originates from:
    https://github.com/jpvanhal/inflection/blob/master/inflection.py
    """
    import re
    word = re.sub(r"([A-Z]+)([A-Z][a-z])", r"\1_\2", word)
    word = re.sub(r"([a-z\d])([A-Z])", r"\1_\2", word)
    word = word.replace("-", "_")
    return word.lower()


class Traversal:
    def __init__(self, graph: Graph, ast: "Optional[TraversalAST]" = None) \
           -> None:
        self.graph = graph
        self.ast = ast if ast is not None else TraversalAST()

    def __repr__(self) -> str:
        return "Traversal[%s]" % self.graph

    @property
    def bytecode(self) -> "List[Union[List, Step]]":
        return self.ast.bytecode


class GraphTraversalSource(Traversal):
    def __repr__(self) -> str:
        return "GraphTraversalSource[%s]" % self.graph

    def get_graph_traversal_source(self) -> "GraphTraversalSource":
        return self.__class__(self.graph, TraversalAST(self.ast))

    # def withBulk(self, *args):
    #     source = self.get_graph_traversal_source()
    #     source.bytecode.add_source("withBulk", *args)
    #     return source

    # def withPath(self, *args):
    #     source = self.get_graph_traversal_source()
    #     source.bytecode.add_source("withPath", *args)
    #     return source

    def with_sack(self, *args) -> "GraphTraversalSource":
        source = self.get_graph_traversal_source()
        source.ast.add_source("withSack", *args)
        return source

    # def with_side_effect(self, *args):
    #     source = self.get_graph_traversal_source()
    #     source.bytecode.add_source("withSideEffect", *args)
    #     return source

    # def with_(self, *args):
    #     source = self.get_graph_traversal_source()
    #     source.bytecode.add_source("with", *args)
    #     return source

    def with_traversers(self, traversers: "TraverserCollection[T]") \
            -> "GraphTraversalSource":
        source = self.get_graph_traversal_source()
        source.ast.add_source("withTraversers", traversers)
        return source

    def _opadd(self, op: str, *args) -> "GraphTraversal":
        ast = TraversalAST(self.ast)
        ast.add_step(op, *args)
        return GraphTraversal(self.graph, ast)


for _ in ("E", "V", "addE", "addV", "inject", "io"):
    setattr(GraphTraversalSource, snake_case(_), partialmethod(
        GraphTraversalSource._opadd, _.rstrip("_")))


class GraphTraversal(Traversal):
    def run(self):
        from eavlib.gremlin.computer import Computer
        computer = Computer(self)
        self._result = tuple(computer.run())
        self._iter = iter(self._result)
        return self._result

    def __iter__(self):
        if not hasattr(self, "_iter"):
            self.run()
        return self._iter

    def __next__(self):
        return next(iter(self))

    def __len__(self):
        return len(self.result)

    def __repr__(self):
        preview = 20
        len_ = len(self)
        s = repr(self.result[:preview])
        if len_ > preview:
            s = s[:-1] + " ...) and %i more items" % (len_ - preview)
        return s

    @property
    def result(self) -> Iterable:
        if not hasattr(self, "_result"):
            self.run()
        return self._result

    def _opadd(self, op: str, *args) -> "GraphTraversal":
        ast = TraversalAST(self.ast)
        ast.add_step(op, *args)
        return GraphTraversal(self.graph, ast)


class __(Traversal):
    def _opadd(op: str, *args) -> GraphTraversal:
        return getattr(GraphTraversal, snake_case(op))(
            GraphTraversal(None), *args)


for _ in ("V", "addE", "addV", "aggregate", "and_", "as_", "barrier", "both",
          "bothE", "bothV", "branch", "by", "cap", "choose", "coalesce",
          "coin", "connectedComponent", "constant", "count", "cyclicPath",
          "dedup", "drop", "emit", "filter", "flatMap", "fold", "from_",
          "group", "groupCount", "has", "hasId", "hasKey", "hasLabel",
          "hasNot", "hasValue", "id", "identity", "inE", "inV", "in_", "index",
          "inject", "is_", "key", "label", "limit", "local", "loops", "map",
          "match", "math", "max", "mean", "min", "not_", "option", "optional",
          "or_", "order", "otherV", "out", "outE", "outV", "pageRank", "path",
          "peerPressure", "profile", "program", "project", "properties",
          "property", "propertyMap", "range", "read", "repeat", "sack",
          "sample", "select", "shortestPath", "sideEffect", "simplePath",
          "skip", "store", "subgraph", "sum", "tail", "timeLimit", "times",
          "to", "toE", "toV", "tree", "unfold", "union", "until", "value",
          "valueMap", "values", "where", "with_", "write",
          # non-standard
          "ctime", "mtime"):
    setattr(GraphTraversal, snake_case(_), partialmethod(
        GraphTraversal._opadd, _.rstrip("_")))
    if _ not in ("by", "connectedComponent", "from_", "option", "pageRank",
                 "peerPressure", "profile", "program", "read", "shortestPath",
                 "with_", "write"):
        setattr(__, snake_case(_), partial(__._opadd, _))


T = TypeVar("T")


class Traverser(Generic[T]):
    def __init__(self, type: type, **kwargs: Any) -> None:
        self.__type__ = type
        self.bindings = {}
        for k, v in kwargs.items():
            if k == "bindings":
                self.bindings.update(**v)
            else:
                setattr(self, k, v)

    def bind_as(self, name: str) -> None:
        if not hasattr(self, "oid"):
            raise KeyError("bindings are only supported on Node traversers")
        self.bindings[name] = self.oid

    def __repr__(self) -> str:
        return "Traverser(%r)" % self.value

    def properties(self, filter_keys=None):
        def filter_func(name):
            if name.startswith("__"):
                return False
            elif filter_keys:
                return name in filter_keys
            else:
                return True

        return {k: getattr(self, k) for k in filter(filter_func, vars(self))}

    @property
    def value(self) -> T:
        extractors = {
            Node: lambda: Node(id=self.oid),
            Relation: lambda: Relation(
                subject=Node(self.subj),
                predicate=self.pred,
                object=Node(self.obj)),
            # NodeAttribute: lambda: NodeAttribute(
            #     "", self.aval, self.path.end),  # FIXME
            NodeValueMap: lambda: NodeValueMap(self.path[-1], self.map),
            NodeSelection: lambda: NodeSelection(
                (k, Node(v)) for k, v in self.map.items()),
            Path: lambda: Path(self.path),
        }

        if self.__type__ in extractors:
            return extractors[self.__type__]()

        # Fallback for less specific types
        elif hasattr(self, "lbl"):  # label()
            return NodeLabel(self.lbl, node=self.path[-1])
        elif hasattr(self, "id"):  # id()
            return int(self.id)
        elif hasattr(self, "val"):  # constant()
            return self.val
        elif hasattr(self, "cnt"):  # count()
            return int(self.cnt)
        elif hasattr(self, "datetime"):  # ctime() or mtime()
            return self.datetime

        else:
            raise NotImplementedError("cannot get value of %r" % self)


class TraverserCollection(Generic[T]):
    def __init__(self, type: Type[T]) -> None:
        self.__type__ = type

    def __iter__(self) -> Iterator[Traverser[T]]:
        return self

    def __next__(self) -> Traverser[T]:
        raise NotImplementedError()

    def _base_repr(self, items: Any) -> str:
        return "%s%r%r" % (self.__class__.__name__, self.__type__, items)

    def __repr__(self) -> str:
        return self._base_repr("(...)")

    def values(self) -> Tuple[T]:
        return tuple(t.value for t in self)


class TraverserTuple(tuple, TraverserCollection[T]):
    def __new__(cls, *args):
        if len(args) == 1:
            if isinstance(args[0], TraverserCollection):
                collection = args[0]
                obj = tuple.__new__(cls, collection)
                obj.__type__ = collection.__type__
                return obj
            elif isinstance(args[0], Traverser):
                obj = tuple.__new__(cls, (args[0],))
                obj.__type__ = args[0].__type__
                return obj
            elif isinstance(args[0], type):
                obj = tuple.__new__(cls)
                obj.__type__ = args[0]
                return obj
        elif len(args) >= 2:
            obj = tuple.__new__(cls, args[1:])
            obj.__type__ = args[0]
            return obj

        raise Exception("Invalid arguments passed to TraverserTuple()")

    def __init__(self, *args):
        # initialisation is done in __new__
        pass

    def __repr__(self) -> str:
        return super()._base_repr(tuple(self))


class TraverserCollectionGenerator(TraverserCollection[T]):
    __slots__ = ("__type__", "_generator")

    def __init__(self, type: Type[T], generator: Generator[T, None, None]) \
            -> None:
        super().__init__(type)
        self._generator = generator

    def __next__(self) -> Traverser[T]:
        el = next(self._generator)
        if not issubclass(el.__type__, self.__type__):
            raise ValueError(
                "Element is not a Traverser of required type %r (got %r)" % (
                    self.__type__, el.__type__))
        return el


class SQLTraverserCollectionGenerator(TraverserCollection[T]):
    __slots__ = ("__type__", "_graph", "_query", "_parameters")

    def __init__(self, type: Type[T], graph: Graph, query: str,
                 parameters: Dict = dict()) -> None:
        super().__init__(type)
        self._graph = graph
        self._query = query
        self._parameters = parameters

    def __next__(self) -> Traverser[T]:
        if not hasattr(self, "_dbres"):
            # Query the database to get the traversers
            self._dbres = self._graph.connection.query(
                self._query, **self._parameters)

        def fmt(dbrow):
            for k, v in dbrow.items():
                if k == "path":
                    yield (k, tuple(map(Node, v)))
                else:
                    yield (k, v)

        try:
            row = next(self._dbres)
            t_attrs = dict(fmt(row))
            return Traverser(self.__type__, **t_attrs)
        except StopIteration:
            self._dbres.close()
            raise


class Path:
    __slots__ = ("_path",)

    def __init__(self, path: Tuple[Node]) -> None:
        self._path = path

    def __eq__(self, other) -> bool:
        if isinstance(other, self.__class__):
            return self._path == other._path
        return False

    def __hash__(self) -> int:
        return hash(self._path)

    def __len__(self) -> int:
        return len(self._path)

    def __repr__(self) -> str:
        return "Path[%s]" % (", ".join(map(str, self._path)))

    @property
    def value(self) -> "Path":
        return self

    @property
    def start(self) -> Node:
        try:
            return self._path[0]
        except IndexError:
            raise IndexError("path is empty")

    @property
    def end(self) -> Node:
        try:
            return self._path[-1]
        except IndexError:
            raise IndexError("path is empty")

    def __getitem__(self, index: int) -> Node:
        return self._path[index]


class Step(tuple):
    def __new__(cls, op: str, *args) -> "Step":
        return tuple.__new__(cls, (op,) + args)

    @property
    def op(self) -> str:
        return self[0]

    @property
    def args(self) -> tuple:
        return self[1:]

    @property
    def bytecode(self) -> list:
        return (self.op,) + tuple(map(
            lambda arg:
                arg if not isinstance(arg, TraversalAST) else arg.bytecode,
            self.args))

    def __repr__(self) -> str:
        return "(%s)" % (" ".join(
            (self.op.upper(),) + tuple(map(repr, self.args))))


class TraversalAST:
    def __init__(self, ast: "Optional[TraversalAST]" = None) -> None:
        self.bindings = dict()

        if isinstance(ast, TraversalAST) and ast is not None:
            self.source_instructions = list(ast.source_instructions)
            self.step_instructions = list(ast.step_instructions)
        else:
            self.source_instructions = list()
            self.step_instructions = list()

    def add_source(self, source_name: str, *args) -> None:
        self.source_instructions.append(
            Step(source_name, *tuple(map(self.__convert_arg, args))))

    def add_step(self, step_name: str, *args) -> None:
        self.step_instructions.append(
            Step(step_name, *tuple(map(self.__convert_arg, args))))

    def __eq__(self, other) -> bool:
        if isinstance(other, self.__class__):
            return self.source_instructions == other.source_instructions \
                and self.step_instructions == other.step_instructions
        return False

    def __convert_arg(self, arg) -> "Any":
        if isinstance(arg, GraphTraversal):
            # self.bindings.update(arg.bytecode.bindings)
            return arg.ast
        elif isinstance(arg, dict):
            return {self.__convert_arg(k): self.__convert_arg(v)
                    for k, v in arg.items()}
        elif isinstance(arg, list):
            return list(map(self.__convert_arg, arg))
        elif isinstance(arg, set):
            return set(map(self.__convert_arg, arg))
        # elif isinstance(arg, tuple) and 2 == len(arg):
        #     key, value = arg
        #     self.bindings[key] = value
        #     return tuple(key, self.__convert_arg(value))
        return arg

    @property
    def bytecode(self):
        return tuple(
            tuple(inst.bytecode for inst in insts)
            for insts in (self.source_instructions, self.step_instructions))

    def __repr__(self):
        return "((%s) (%s))" % (
            " ".join(map(repr, self.source_instructions)),
            " ".join(map(repr, self.step_instructions)))
        # return "%s;%s" % (self.source_instructions, self.step_instructions)


def str2num(s: Union[str, Number]) -> Number:
    # FIXME: This trivial implementation probably does not handle edge-cases…
    if isinstance(s, (int, float)):
        return s
    elif s.isdigit():
        return int(s)
    else:
        return float(s)


class BaseP:
    def __init__(self, operator: str, *args, pyimpl: callable = None,
                 sqlimpl: str = None) -> None:
        self.operator = operator
        self.args = args
        self.pyimpl = pyimpl
        self.sqlimpl = sqlimpl

    def __call__(self, base: Any) -> Any:
        if callable(self.pyimpl):
            return self.pyimpl(
                base.value if isinstance(base, Traverser) else base)
        raise NotImplementedError("No Python implementation for %s.%s" % (
            self.__class__.__name__, self.operator))

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, self.__class__):
            return all((
                self.operator == other.operator, self.args == other.args))
        return False

    def __repr__(self) -> str:
        return "%s.%s(%s)" % (
            self.__class__.__name__, self.operator, ", ".join(self.args))


class P(BaseP):
    @staticmethod
    def between(lower: Number, upper: Number) -> "P":
        return P("between", (lower, upper),
                 pyimpl=lambda x: lower <= str2num(x) < upper,
                 sqlimpl=(
                     "CAST(%(x)s AS NUMERIC) >= %%(l)s "
                     "AND CAST(%(x)s AS NUMERIC) < %%(u)s",
                     dict(l=lower, u=upper)))

    @staticmethod
    def eq(obj: Any) -> "P":
        return P("eq", obj,
                 pyimpl=lambda x: x == obj,
                 sqlimpl=("%(x)s = %%(o)s", dict(o=obj)))

    @staticmethod
    def gt(num: Number) -> "P":
        return P("gt", num,
                 pyimpl=lambda x: str2num(x) > num,
                 sqlimpl=("CAST(%(x)s AS NUMERIC) > %%(n)s", dict(n=num)))

    @staticmethod
    def gte(num: Number) -> "P":
        return P("gte", num,
                 pyimpl=lambda x: str2num(x) >= num,
                 sqlimpl=("CAST(%(x)s AS NUMERIC) >= %%(n)s", dict(n=num)))

    @staticmethod
    def inside(lower: Number, upper: Number) -> "P":
        return P("inside", (lower, upper),
                 pyimpl=lambda x: lower < str2num(x) < upper,
                 sqlimpl=(
                     "CAST(%(x)s AS NUMERIC) > %%(l)s "
                     "AND CAST(%(x)s AS NUMERIC) < %%(u)s", dict(
                         l=lower, u=upper)))

    @staticmethod
    def lt(num: Number) -> "P":
        return P("lt", num,
                 pyimpl=lambda x: str2num(x) < num,
                 sqlimpl=("CAST(%(x)s AS NUMERIC) < %%(n)s", dict(n=num)))

    @staticmethod
    def lte(num: Number) -> "P":
        return P("lte", num,
                 pyimpl=lambda x: str2num(x) <= num,
                 sqlimpl=("CAST(%(x)s AS NUMERIC) <= %%(n)s", dict(n=num)))

    @staticmethod
    def neq(obj: Any) -> "P":
        return P("eq", obj,
                 pyimpl=lambda x: x != obj,
                 sqlimpl=("%(x)s != %%(o)s", dict(o=obj)))

    @staticmethod
    def not_(other: "P") -> "P":
        other_sql, other_params = other.sqlimpl

        return P("not", (other,),
                 pyimpl=lambda x: not other(x),
                 sqlimpl=(
                     "NOT (%s)" % (other_sql), other_params))

    @staticmethod
    def outside(lower: Number, upper: Number) -> "P":
        return P("between", (lower, upper),
                 pyimpl=lambda x: not (lower <= str2num(x) <= upper),
                 sqlimpl=(
                     "CAST(%(x)s AS NUMERIC) NOT BETWEEN %%(l)s AND %%(u)s",
                     dict(l=lower, u=upper)))

    @staticmethod
    def within(*args: Any) -> "P":
        if not len(args):
            raise ValueError("P.within requires at least one argument")

        return P("within", args,
                 pyimpl=lambda x: x in args,
                 sqlimpl=("%(x)s IN %%(seq)s",
                          dict(seq=tuple(map(str, args)))))

    @staticmethod
    def without(*args: Any) -> "P":
        if not args:
            raise ValueError("P.without requires at least one argument")

        return P("without", args,
                 pyimpl=lambda x: x not in args,
                 sqlimpl=("%(x)s NOT IN %%(seq)s", dict(
                     seq=tuple(map(str, args)))))


class TextP(BaseP):
    @staticmethod
    def starting_with(substr: str) -> "TextP":
        return TextP(
            "startingWith", (substr,),
            pyimpl=lambda x: str(x).startswith(substr),
            sqlimpl=(
                "CAST(%(x)s AS TEXT) LIKE %%(likes)s", dict(
                    likes="%s%%" % substr)))

    @staticmethod
    def ending_with(substr: str) -> "TextP":
        return TextP(
            "endingWith", (substr,),
            pyimpl=lambda x: str(x).endswith(substr),
            sqlimpl=(
                "CAST(%(x)s AS TEXT) LIKE %%(likes)s", dict(
                    likes="%%%s" % substr)))

    @staticmethod
    def containing(substr: str) -> "TextP":
        return TextP(
            "containing", (substr,),
            pyimpl=lambda x: str(x) in substr,
            sqlimpl=(
                "CAST(%(x)s AS TEXT) LIKE %%(likes)s", dict(
                    likes="%%%s%%" % substr)))

    @staticmethod
    def not_starting_with(substr: str) -> "TextP":
        return TextP(
            "notStartingWith", (substr,),
            pyimpl=lambda x: not str(x).startswith(substr),
            sqlimpl=(
                "CAST(%(x)s AS TEXT) NOT LIKE %%(likes)s", dict(
                    likes="%s%%" % substr)))

    @staticmethod
    def not_ending_with(substr: str) -> "TextP":
        return TextP(
            "notEndingWith", (substr,),
            pyimpl=lambda x: not str(x).endswith(substr),
            sqlimpl=(
                "CAST(%(x)s AS TEXT) NOT LIKE %%(likes)s", dict(
                    likes="%%%s" % substr)))

    @staticmethod
    def not_containing(substr: str) -> "TextP":
        return TextP(
            "notContaining", (substr,),
            pyimpl=lambda x: str(x) not in substr,
            sqlimpl=(
                "CAST(%(x)s AS TEXT) NOT LIKE %%(likes)s", dict(
                    likes="%%%s%%" % substr)))
