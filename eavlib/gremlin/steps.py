# flake8: noqa: E501

import abc
import datetime
import itertools

from collections import OrderedDict
from psycopg2 import sql  # FIXME
from psycopg2.extras import Json  # FIXME
from typing import (Any, Dict, Generic, Optional, List, Union, TypeVar)

from eavlib.graph import (
    Graph, Node, NodeSelection, NodeValueMap, Relation)
from eavlib.gremlin.traversal import (
    GraphTraversalSource, BaseP, Path, Step, Traversal, TraversalAST,
    Traverser, TraverserCollection, TraverserTuple,
    TraverserCollectionGenerator, SQLTraverserCollectionGenerator)
from eavlib.gremlin.util import (
    PgQuery, placeholder_generator, randomize_placeholders)

ModifierDict = Dict[str, List[Step]]
NodeRel = TypeVar("NodeRel", Node, Relation)
Value = TypeVar("Value", int, str)

S = TypeVar("S")
E = TypeVar("E")


class ComputerStep(Generic[S, E]):
    __require__ = object

    def __init__(self, graph: Graph, modifiers: ModifierDict = {}) -> None:
        self.graph = graph
        self.schema = graph.connection.schema
        self.modifiers = modifiers

        if hasattr(self, "__require_schema__"):
            if self.graph.connection.schema_version < self.__require_schema__:
                raise RuntimeError("%s requires at least schema version %u" % (
                    self.__class__.__name__, self.__require_schema__))

    # NOTE: Implementations must implement a run() method:
    # def run(self, in_: TraverserCollection[S]) -> TraverserCollection[E]:
    #     raise NotImplementedError()

    def _assert_require(self, in_: TraverserCollection[S]) -> None:
        # raises on error
        if self.__require__ is None:
            return

        if isinstance(self.__require__, (tuple, list)):
            req = list(self.__require__)
        else:
            req = [self.__require__]

        if None in req:
            req.remove(None)
            if in_ is None:
                return
        elif in_ is None:
            raise TypeError("Expected in_, but got: %r" % in_)

        if not issubclass(in_.__type__, tuple(req)):
            raise TypeError("%s requires in_ of type %r, got %r" % (
                self.__class__.__name__, self.__require__, in_.__type__))


class SQLStepMixin(Generic[S, E]):
    @abc.abstractmethod
    def pg_query(self) -> PgQuery:
        ...

    def run(self, in_: Optional[TraverserCollection[S]] = None) \
            -> SQLTraverserCollectionGenerator[E]:
        self._assert_require(in_)

        new_query, new_params = self.pg_query()

        if in_ is None:
            # This is the first step
            return SQLTraverserCollectionGenerator(
                self.__provides__, self.graph, new_query, new_params)

        if isinstance(in_, SQLTraverserCollectionGenerator):
            # Previous step is an SQL query
            # TODO: Maybe check if chainable or if values need to be pulled out
            # of SQL and shoved back in.
            return SQLTraverserCollectionGenerator(
                self.__provides__, self.graph,
                """WITH in_ AS (%s) %s""" % (in_._query, new_query),
                {**new_params, **in_._parameters})

        in_traversers = tuple(in_)

        if not in_traversers:
            return TraverserTuple(self.__provides__)

        # FIXME: This VALUES hack is ugly and possibly unsafe!
        in_param_keys = tuple(in_traversers[0].properties().keys())

        def _prep_traverser(traverser):
            row = []
            for k in in_param_keys:
                if k == "bindings":
                    class CastedJson(Json):
                        def getquoted(self):
                            return super().getquoted() + "::jsonb".encode()
                    row.append(CastedJson(traverser.bindings))
                elif k == "path":
                    row.append([node.id for node in traverser.path])
                else:
                    row.append(getattr(traverser, k))
            return tuple(row)

        in_params = {
            placeholder_generator(): _prep_traverser(t) for t in in_traversers}

        query = sql.SQL("WITH in_ ({}) AS (VALUES {}) {}").format(
            sql.SQL(", ").join(map(sql.Identifier, in_param_keys)),
            sql.SQL(", ").join(sql.Placeholder(k) for k in in_params),
            sql.SQL(new_query)).as_string(self.graph.connection.connection)  # FIXME: Shouldn't need to access connection here

        return SQLTraverserCollectionGenerator(
            self.__provides__, self.graph, query, {**new_params, **in_params})


class ComputerSteps:
    class AsStep(SQLStepMixin[Node, Node], ComputerStep[Node, Node]):
        __require__ = Node
        __provides__ = Node

        def __init__(self, graph: Graph, name: str) -> None:
            super().__init__(graph)
            self.name = name

        def pg_query(self) -> PgQuery:
            return randomize_placeholders("""
            SELECT
              in_.oid,
              in_.path,
              in_.bindings || ('{"' || %(name)s || '":' || in_.oid || '}')::jsonb AS bindings,
              in_.sack
            FROM in_
            """, dict(name=self.name))

        def run(self, in_: TraverserCollection[Node]) -> TraverserCollection[Node]:
            self._assert_require(in_)

            if isinstance(in_, SQLTraverserCollectionGenerator):
                return super().run(in_)
            else:
                def _bind(t: Traverser[S]) -> Traverser[S]:
                    t.bind_as(self.name)
                    return t
                return TraverserCollectionGenerator(in_.__type__, map(_bind, in_))

    class ConstantStep(ComputerStep[S, Value]):
        __require__ = object

        def __init__(self, graph: Graph, const: Value) -> None:
            super().__init__(graph)
            self.const = const
            self.__provides__ = type(const)

        def run(self, in_: TraverserCollection[S]) -> TraverserCollectionGenerator[Value]:
            self._assert_require(in_)

            def _conv_traverser(t: Traverser[S]) -> Traverser[Value]:
                return Traverser(
                    type(self.const), val=self.const, path=t.path, sack=t.sack)

            return TraverserCollectionGenerator(type(self.const), map(_conv_traverser, in_))

    class CountStep(SQLStepMixin[S, int], ComputerStep[S, int]):
        __require__ = object  # Any
        __provides__ = int

        def pg_query(self) -> PgQuery:
            return ("""
            SELECT
              COUNT(*) AS cnt,
              ARRAY[]::int[] AS path,
              '{{}}'::jsonb AS bindings,
              {sackop} AS sack
            FROM in_""".format(sackop="NULL"), {})

        def run(self, in_: TraverserCollection[S]) -> TraverserCollection[S]:
            if isinstance(in_, SQLTraverserCollectionGenerator):
                return super().run(in_)
            else:
                return TraverserTuple((Traverser(
                    cnt=len(tuple(in_)), path=None,
                    bindings={}, sack=None),))

    class CtimeStep(SQLStepMixin[Node, datetime.datetime], ComputerStep[Node, datetime.datetime]):
        __require__ = Node
        __provides__ = datetime.datetime
        __require_schema__ = 3

        def pg_query(self) -> PgQuery:
            return ("""
            SELECT
              o.ctime AS datetime,
              in_.path,
              in_.bindings,
              {sackop} AS sack
            FROM
              in_
            LEFT JOIN {schema}.object o ON o.id = in_.oid
            """.format(schema=self.schema, sackop="NULL"), {})

    class VStep(SQLStepMixin[None, Node], ComputerStep[None, Node]):
        __require__ = None
        __provides__ = Node

        def __init__(self, graph: Graph, *args: Union[int, Node],
                     modifiers: ModifierDict = {}) -> None:
            super().__init__(graph, modifiers=modifiers)

            def _idof(x: Union[int, Node]) -> int:
                if isinstance(x, int):
                    return x
                elif isinstance(x, Node):
                    return int(x.id)
                raise TypeError("Invalid object %r given to V()" % (x,))

            self.args = tuple(map(_idof, args))

        def pg_query(self) -> PgQuery:
            params = {"model_id": self.graph.model_id}

            if self.args:
                query = """
                SELECT
                  filter_.id AS oid,
                  ARRAY[filter_.id] AS path,
                  '{{{{}}}}'::jsonb AS bindings,
                  NULL AS sack
                FROM
                  (VALUES {args}) filter_(id),
                  {{schema}}.object o,
                  {{schema}}.model_entity me
                WHERE o.id = filter_.id AND me.id = o.entity_id
                  AND me.model_id = %(model_id)s
                """.format(args=", ".join(map(
                    # HACK: We format this in because we know for sure that it's only ints.
                    lambda id: "(%d)" % id, self.args)))
            else:
                query = """
                SELECT
                  o.id AS oid,
                  ARRAY[o.id] AS path,
                  '{{}}'::jsonb AS bindings,
                  {sackop} AS sack
                FROM
                  {schema}.object o
                LEFT JOIN {schema}.model_entity me ON me.id = o.entity_id
                WHERE me.model_id = %(model_id)s
                """

            for modname, mods in self.modifiers.items():
                if not mods:
                    continue
                if modname == "hasLabel":
                    for mod in mods:
                        query += """AND me.name IN %(entity_names)s"""
                        params["entity_names"] = mod.args
                else:
                    raise ValueError(
                        "V() does not support the %s step modifier." % modname)

            return randomize_placeholders(
                query.format(schema=self.schema, sackop="NULL"), params)

    class HasLabelStep(SQLStepMixin[NodeRel, NodeRel], ComputerStep[NodeRel, NodeRel]):
        __require__ = (None, Node, Relation)
        __provides__ = Node
        __require_schema__ = 6

        def __init__(self, graph: Graph, *args: str) -> None:
            super().__init__(graph)
            self.labels = args

        def run(self, in_: Optional[TraverserCollection[NodeRel]] = None) \
                -> SQLTraverserCollectionGenerator[NodeRel]:
            self._in_ = in_
            return super().run(in_)

        def pg_query(self) -> PgQuery:
            return randomize_placeholders(sql.SQL("""
            SELECT
              o.id AS oid,
              in_.path,
              in_.bindings,
              {{sackop}} AS sack
            FROM
              {}
            LEFT JOIN {{schema}}.object o ON o.id = in_.oid
            WHERE o.entity_id IN (
                SELECT entity_id
                FROM {{schema}}.model_ancestors
                WHERE parent_id IN (
                    SELECT id
                    FROM {{schema}}.model_entity
                    WHERE model_id = %(model_id)s AND name IN %(entity_names)s
                )
            )
            """.format(
                "in_" if self._in_ else """
                (
                    SELECT
                        id AS oid,
                        ARRAY[id] AS path,
                        '{{}}'::jsonb AS bindings,
                        {sackop} AS sack
                    FROM {schema}.object
                ) in_"""
            )).format(
                schema=sql.Identifier(self.schema),
                sackop=sql.Literal("NULL"),
            ).as_string(self.graph.connection.connection), {
                "entity_names": self.labels,
                "model_id": self.graph.model_id,
                })

    class HasStep(SQLStepMixin[Node, Node], ComputerStep[Node, Node]):
        __require__ = (None, Node)
        __provides__ = Node

        @classmethod
        def _parse_args(cls, *args: Any) -> Dict[str, Any]:
            # This function contains all the argument parsing mess
            res = {}

            if len(args) == 3:
                res["label"] = args.pop(0)
                raise NotImplementedError("label check in has() does not work")

            res["key"] = args[0]
            if len(args) == 2:
                if isinstance(args[1], (int, str, BaseP)):
                    res["value"] = args[1]
                elif isinstance(args[1], Traversal):
                    raise NotImplementedError(
                        "has() does not support Traversal values")
                else:
                    raise TypeError(
                        "Value '%r' is not acceptable" % (args[1],))

            return res

        def __init__(self, graph: Graph, *args: Any,
                     modifiers: ModifierDict = {}) -> None:
            super().__init__(graph, modifiers=modifiers)

            for k, v in self.__class__._parse_args(*args).items():
                setattr(self, k, v)

        def run(self, in_: Optional[TraverserCollection[NodeRel]] = None) \
                -> SQLTraverserCollectionGenerator[NodeRel]:
            self._in_ = in_
            return super().run(in_)

        def pg_query(self) -> PgQuery:
            params = {}

            def _subquery(key: str, value: Union[BaseP, Value, None] = None):
                query = """
                SELECT 1
                FROM {schema}.object_attribute oa
                WHERE oa.object_id = in_.oid
                  AND oa.attribute_id IN (SELECT id FROM {schema}.model_attribute WHERE name = %(key)s)
                """
                _subparams = dict(key=key)

                if isinstance(value, BaseP):
                    sqlimpl = value.sqlimpl
                    query += "  AND " + (sqlimpl[0] % dict(x="oa.value"))
                    _subparams.update(sqlimpl[1])
                elif value:
                    query += "  AND oa.value = %(value)s"
                    _subparams["value"] = str(value)

                res, pars = randomize_placeholders(query, _subparams)
                params.update(pars)  # update pg_query()'s params
                return res

            if self._in_:
                query = """
                SELECT
                  in_.oid AS oid,
                  in_.path,
                  in_.bindings,
                  {sackop} AS sack
                FROM
                  in_
                """
            else:
                # start step
                query = """
                SELECT in_.oid AS oid,
                ARRAY[in_.oid] AS path,
                '{{}}'::jsonb AS bindings,
                {sackop} AS sack
                FROM (SELECT id FROM {schema}.object) in_(oid)
                """

            query += "WHERE EXISTS (" + _subquery(self.key, getattr(self, "value", None)) + ")"

            for modname, mods in self.modifiers.items():
                if not mods:
                    continue
                if modname == "has":
                    for i, mod in enumerate(mods):
                        modargs = self.__class__._parse_args(*mod.args)
                        if "label" in modargs:
                            raise ValueError("has() with label is not supported, yet")
                        else:
                            query += " AND EXISTS (" + _subquery(**modargs) + ")"

            return randomize_placeholders(
                query.format(schema=self.schema, sackop="NULL"), params)

    class SelectStep(SQLStepMixin[S, Node], ComputerStep[S, Node]):
        __require__ = object  # Any

        def __init__(self, graph: Graph, *args: str) -> None:
            super().__init__(graph)
            if len(args) < 1:
                raise ValueError("You must pass at least one name to select")

            # We remove duplicates from args because they would be lost in the
            # resulting dict anyway
            self.names = tuple(OrderedDict.fromkeys(args))

            self.__provides__ = NodeSelection if len(self.names) > 1 else Node

        def pg_query(self) -> PgQuery:
            if len(self.names) < 2:
                query = """
                SELECT
                  CAST(bindings->>%(name)s AS integer) AS oid,
                  in_.path,
                  in_.bindings,
                  {sackop} AS sack
                FROM in_
                """.format(sackop="NULL")
                params = dict(name=self.names[0])
            else:
                query = """
                SELECT
                  (SELECT
                     jsonb_object_agg(binding_name, in_.bindings->binding_name)
                   FROM LATERAL jsonb_object_keys(in_.bindings) AS binding_name
                   WHERE binding_name IN %(names)s) AS map,
                  in_.path,
                  in_.bindings,
                  in_.sack
                FROM in_
                """
                params = dict(names=self.names)

            return randomize_placeholders(query, params)

        def run(self, in_: TraverserCollection[S]) -> TraverserCollection[Union[Dict[str, Node], Node]]:
            if isinstance(in_, SQLTraverserCollectionGenerator):
                return super().run(in_)
            else:
                def _select(t: Traverser) -> Traverser:
                    res = {name: t.bindings[name] for name in self.names}

                    if len(res) > 1:
                        tkeys = {"type": dict, "map": res}
                    else:
                        tkeys = {"type": Node, "oid": res.popitem()[1]}
                    tkeys.update(t.properties(("bindings", "sack")))
                    return Traverser(**tkeys)

                return TraverserCollectionGenerator(
                    dict if len(self.names) > 1 else Node, map(_select, in_))

    class ValuesStep(SQLStepMixin[Node, Value], ComputerStep[Node, Value]):
        __require__ = Node
        __provides__ = str

        def __init__(self, graph: Graph, *args: str) -> None:
            super().__init__(graph)
            self.attrs = args

        def pg_query(self) -> PgQuery:
            query = """
            SELECT
              oa.value AS val,
              in_.path,
              in_.bindings,
              {sackop} AS sack
            FROM
              {schema}.object_attribute oa
            RIGHT JOIN in_ ON oa.object_id = in_.oid
            WHERE in_.oid IS NOT NULL"""

            if self.attrs:
                query += """
                  AND oa.attribute_id IN (
                    SELECT ma.id FROM {schema}.model_attribute ma
                    WHERE ma.name IN %(keys)s)"""

            return randomize_placeholders(
                query.format(schema=self.schema, sackop="NULL"),
                dict(keys=self.attrs))

    class ValueMapStep(SQLStepMixin[Node, Dict], ComputerStep[Node, Dict]):
        __require__ = Node
        __provides__ = NodeValueMap

        def __init__(self, graph: Graph, *args: str) -> None:
            super().__init__(graph)
            self.attrs = args

        def pg_query(self) -> PgQuery:
            return randomize_placeholders(
                """
                SELECT
                  (SELECT jsonb_object_agg(ma.name, oa.value)
                   FROM
                    {schema}.object_attribute oa
                   LEFT JOIN {schema}.model_attribute ma ON ma.id = oa.attribute_id
                   WHERE in_.oid = oa.object_id%s) AS map,
                  in_.path,
                  in_.bindings,
                  in_.sack
                FROM in_
                """.format(schema=self.schema) % (" AND ma.name IN %(attrs)s" if self.attrs else ""),
                dict(attrs=self.attrs))

    class PathStep(ComputerStep[Any, Path]):
        __require__ = object  # Any

        def __init__(self, graph: Graph) -> None:
            super().__init__(graph)

        def run(self, in_: TraverserCollection[Any]) \
                -> TraverserCollectionGenerator[Path]:
            self._assert_require(in_)

            return TraverserCollectionGenerator(Path, (
                Traverser(Path, **item.properties())
                for item in in_))

    class PropertiesStep(ComputerStep[Node, E]):
        __require__ = Node

        def __init__(self, graph: Graph) -> None:
            super().__init__(graph)

        def run(self, in_: TraverserCollection[Node]) -> TraverserCollection:
            self._assert_require(in_)

            raise NotImplementedError("the properties() step has not been implemented yet")

    class SackStep(ComputerStep[S, Value]):
        __require__ = object

        def __init__(self, graph: Graph) -> None:
            super().__init__(graph)

        def run(self, in_: TraverserCollection[S]) -> TraverserCollectionGenerator[Value]:
            self._assert_require(in_)

            def _conv_traverser(t: Traverser[Any]):
                if not hasattr(t, "sack") or t.sack is None:
                    raise ValueError("sack must be initialised before use")
                return Traverser(
                    type(t.sack), val=t.sack, path=t.path, sack=t.sack)

            return TraverserCollectionGenerator(object, map(_conv_traverser, in_))  # FIXME: object

    class ChooseStep(ComputerStep[S, E]):
        __require__ = object

        def __init__(self, graph: Graph, cond: Traversal,
                     if_true: Traversal = None, if_false: Traversal = None) \
                -> None:
            super().__init__(graph)
            self.cond = cond
            self.if_true = if_true
            self.if_false = if_false

        def run(self, in_: TraverserCollection[S]) -> TraverserCollection[E]:
            self._assert_require(in_)

            raise NotImplementedError()

    class LabelStep(SQLStepMixin[NodeRel, str], ComputerStep[NodeRel, str]):
        __require__ = (Node, Relation)
        __provides__ = str

        def pg_query(self) -> PgQuery:
            return ("""
            SELECT
              me.name AS lbl,
              in_.path,
              in_.bindings,
              {sackop} AS sack
            FROM
              in_
            LEFT JOIN {schema}.object o ON o.id = in_.oid
            LEFT JOIN {schema}.model_entity me ON me.id = o.entity_id
            """.format(schema=self.schema, sackop="NULL"), {})

    class IdStep(ComputerStep[Node, int]):
        __require__ = Node
        __provides__ = int

        def run(self, in_: TraverserCollection[Node]) -> TraverserCollectionGenerator[int]:
            self._assert_require(in_)

            def _process(el):
                if hasattr(el, "oid"):
                    return Traverser(int, id=el.oid, path=el.path, sack=el.sack)
                raise RuntimeError()

            return TraverserCollectionGenerator(int, map(_process, in_))

    class IdentityStep(ComputerStep[S, S]):
        def run(self, in_: TraverserCollection[S]) -> TraverserCollection[S]:
            return in_

    class RepeatStep(ComputerStep[S, E]):
        __require__ = object  # Any

        def __init__(self, graph: Graph) -> None:
            super().__init__(graph)

        def run(self, in_: TraverserCollection[S]) -> TraverserCollection[E]:
            self._assert_require(in_)

            raise NotImplementedError("The repeat() step has not been implemented yet")

    class InStep(SQLStepMixin[Node, Node], ComputerStep[Node, Node]):
        __require__ = Node
        __provides__ = Node

        def __init__(self, graph: Graph, *args: str) -> None:
            super().__init__(graph)
            self.labels = args

        def pg_query(self) -> PgQuery:
            # Base query for all "in vertices"
            query = """
            SELECT
              orel.object_id AS oid,
              in_.path || ARRAY[orel.object_id] AS path,
              in_.bindings,
              {sackop} AS sack
            FROM
              {schema}.object_relation orel
            INNER JOIN in_ ON orel.target_id = in_.oid"""

            if self.labels:
                # Filter by rel[name] IN self.labels
                query += """
                  WHERE orel.relation_id IN (
                    SELECT
                      mrel.id
                    FROM
                      {schema}.model_relation mrel
                    WHERE mrel.name IN %(labels)s)
                """

            return randomize_placeholders(
                query.format(schema=self.schema, sackop="NULL"),
                dict(labels=self.labels))

    class OutStep(SQLStepMixin[Node, Node], ComputerStep[Node, Node]):
        __require__ = Node
        __provides__ = Node

        def __init__(self, graph: Graph, *args: str) -> None:
            super().__init__(graph)
            self.labels = args

        def pg_query(self) -> PgQuery:
            # Base query for all "out vertices"
            query = """
            SELECT
              orel.target_id AS oid,
              in_.path || ARRAY[orel.target_id] AS path,
              in_.bindings AS bindings,
              {sackop} AS sack
            FROM
              {schema}.object_relation orel
            INNER JOIN in_ ON orel.object_id = in_.oid"""

            if self.labels:
                # Filter by rel[name] IN self.labels
                query += """
                  WHERE orel.relation_id IN (
                    SELECT
                      mrel.id
                    FROM
                      {schema}.model_relation mrel
                    WHERE mrel.name IN %(labels)s)
                """

            return randomize_placeholders(
                query.format(schema=self.schema, sackop="NULL"),
                dict(labels=self.labels))

    class OutEStep(SQLStepMixin[Node, Relation], ComputerStep[Node, Relation]):
        __requires__ = Node
        __provides__ = Relation

        def __init__(self, graph: Graph, *args: str) -> None:
            super().__init__(graph)
            self.labels = args

        def pg_query(self) -> PgQuery:
            # Base query for all "out vertices"
            query = """
            SELECT
              orel.object_id AS subj,
              mrel.name AS pred,
              orel.target_id AS obj,
              in_.path,
              in_.bindings,
              {sackop} AS sack
            FROM
              {schema}.object_relation orel
            LEFT JOIN {schema}.model_relation mrel ON orel.relation_id = mrel.id
            INNER JOIN in_ ON orel.object_id = in_.oid"""

            if self.labels:
                # Filter by rel[name] IN self.labels
                query += """
                WHERE mrel.name IN %(labels)s
                """

            return randomize_placeholders(
                query.format(schema=self.schema, sackop="NULL"),
                dict(labels=self.labels))

    class IsStep(ComputerStep[Value, Value]):
        __require__ = (int, str)

        def __init__(self, graph: Graph, predicate: BaseP) -> None:
            super().__init__(graph)
            self.predicate = predicate

        def run(self, in_: TraverserCollection[Value]) \
                -> TraverserCollectionGenerator[Value]:
            self._assert_require(in_)
            return TraverserCollectionGenerator(
                in_.__type__, filter(self.predicate, in_))

    class OrderStep(SQLStepMixin[S, S], ComputerStep[S, S]):
        __require__ = (Node, int, str)

        def __init__(self, graph: Graph, modifiers: ModifierDict = {}) -> None:
            super().__init__(graph, modifiers=modifiers)

        def pg_query(self) -> PgQuery:
            query = """
            SELECT
              in_.*
            FROM
              in_
            """
            params = {}

            by_mods = self.modifiers.get("by", list())
            if by_mods:
                # User specified ordering
                order_exprs = list()
                for by_mod in by_mods:
                    if len(by_mod.args) == 1:
                        # Only sort order specified
                        # sort_col = ""
                        # if self._input.__type__ == "":
                        #     pass
                        # elif hasattr(self, "lbl"):  # label()
                        #     return str(self.lbl)
                        # elif hasattr(self, "id"):  # id()
                        #     return int(self.id)
                        # elif hasattr(self, "val"):  # constant()
                        #     return self.val

                        # order_exprs.append("in_.%s %s" % (sort_col, by_mod.args[1]))
                        raise Exception()
                    elif len(by_mod.args) == 2:
                        expr = by_mod.args[0]

                        if isinstance(expr, Traversal):
                            raise Exception("Order by Traversal is not yet supported")
                        elif isinstance(expr, str):
                            expr = """
                            (SELECT oa.value
                             FROM {schema}.object_attribute oa
                             LEFT JOIN {schema}.model_attribute ma ON ma.id = oa.attribute_id
                             WHERE oa.object_id = in_.oid AND ma.name = %(key)s)
                            """
                            params["key"] = by_mod.args[0]
                        else:
                            raise TypeError("Unsupported order by expression!")

                        order_exprs.append(expr + " " + by_mod.args[1])
                    else:
                        raise ValueError(
                            "Too many arguments given to by() modulator. "
                            "Expected 1 or 2, got %d" % len(by_mod.args))
                query += "ORDER BY " + ", ".join(order_exprs)
            else:
                # Auto-"magic" ordering
                raise Exception("auto-magic orderng is not yet supported")

            return randomize_placeholders(query.format(schema=self.schema), params)

        def run(self, in_: TraverserCollection[S]) -> SQLTraverserCollectionGenerator[S]:
            self.__provides__ = in_.__type__
            self._input = in_
            return super().run(in_)

    class RangeStep(SQLStepMixin[S, S], ComputerStep[S, S]):
        __require__ = object

        def __init__(self, graph: Graph, low: int, high: int) -> None:
            super().__init__(graph)
            self.offset = low
            self.limit = (high - low)

        def pg_query(self) -> PgQuery:
            return randomize_placeholders(
                """
                SELECT
                  in_.*
                FROM in_
                LIMIT %(limit)s OFFSET %(offset)s
                """, dict(offset=self.offset, limit=self.limit))

        def run(self, in_: TraverserCollection[S]) -> TraverserCollectionGenerator[S]:
            self._assert_require(in_)

            self.__provides__ = in_.__type__

            if isinstance(in_, SQLTraverserCollectionGenerator):
                return super().run(in_)
            else:
                return TraverserCollectionGenerator(
                    self.__provides__, itertools.islice(
                        in_, self.offset, (self.offset + self.limit)))

    class LimitStep(RangeStep):
        def __init__(self, graph: Graph, limit: int) -> None:
            super().__init__(graph, 0, limit)

    class MtimeStep(SQLStepMixin[Node, datetime.datetime], ComputerStep[Node, datetime.datetime]):
        __require__ = Node
        __provides__ = datetime.datetime
        __require_schema__ = 3

        def pg_query(self) -> PgQuery:
            return ("""
            SELECT
              o.mtime AS datetime,
              in_.path,
              in_.bindings,
              {sackop} AS sack
            FROM
              in_
            LEFT JOIN {schema}.object o ON o.id = in_.oid
            """.format(schema=self.schema, sackop="NULL"), {})

    class WhereStep(SQLStepMixin[NodeRel, NodeRel], ComputerStep[NodeRel, NodeRel]):
        __require__ = (Node, Relation)

        def __init__(self, graph: Graph, ast: TraversalAST) -> None:
            super().__init__(graph)
            self.traversal = GraphTraversalSource(graph, ast)

        def pg_query(self) -> PgQuery:
            return randomize_placeholders(
                "SELECT in_.* FROM in_ WHERE in_.oid IN (SELECT path[1] FROM filter_)", {})

        def run(self, in_: TraverserCollection[NodeRel]) -> SQLTraverserCollectionGenerator[NodeRel]:
            self._assert_require(in_)

            new_query, new_params = self.pg_query()

            if isinstance(in_, SQLTraverserCollectionGenerator):
                in_params = in_._parameters
                in_with = sql.SQL("in_ AS ({})").format(sql.SQL(in_._query))
            else:
                in_traversers = tuple(in_)

                # FIXME: This VALUES hack is ugly and possibly unsafe!
                in_params = {
                    placeholder_generator(): tuple(t.properties().values())
                    for t in in_traversers}

                in_with = sql.SQL("in_ ({}) AS (VALUES {})").format(
                    sql.SQL(", ").join(map(
                        sql.Identifier, in_traversers[0].properties().keys())),
                    sql.SQL(", ").join(sql.Placeholder(k) for k in in_params))

            from eavlib.gremlin.computer import Computer

            filter_ = Computer(self.traversal.with_traversers(
                SQLTraverserCollectionGenerator(
                    Node, self.graph, """
                    SELECT
                      in_.oid,
                      path[array_upper(path, 1):array_upper(path, 1)] AS path,
                      in_.bindings,
                      in_.sack
                    FROM in_
                    """)))._exec()

            if isinstance(filter_, SQLTraverserCollectionGenerator):
                filter_params = filter_._parameters
                filter_with = sql.SQL("filter_ AS ( {} )").format(
                    sql.SQL(filter_._query))
            else:
                filter_traversers = tuple(filter_)

                # FIXME: This VALUES hack is ugly and possibly unsafe!
                filter_params = {
                    placeholder_generator(): tuple(t.properties().values())
                    for t in filter_traversers}

                filter_with = sql.SQL("filter_ ({}) AS (VALUES {})").format(
                    sql.SQL(", ").join(map(
                        sql.Identifier, filter_traversers[0].properties().keys())),
                    sql.SQL(", ").join(sql.Placeholder(k) for k in filter_params))

            query = sql.SQL("WITH {}, {} {}") \
                       .format(in_with, filter_with, sql.SQL(new_query)) \
                       .as_string(self.graph.connection.connection)  # FIXME: Shouldn't need to access connection here

            return SQLTraverserCollectionGenerator(
                in_.__type__, self.graph, query,
                {**new_params, **in_params, **filter_params})
