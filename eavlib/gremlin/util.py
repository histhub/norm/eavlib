import enum
import random
import string
import time

from typing import Any, Dict, Tuple

PgQuery = Tuple[str, Dict[str, Any]]


def placeholder_generator(size: int = 6, chars: str = (
        string.ascii_uppercase + string.ascii_lowercase)) -> str:
    return "".join(random.choice(chars) for _ in range(size))


def randomize_placeholders(s: str, placeholders: Dict[str, Any]) -> PgQuery:
    mapping = {k: placeholder_generator() for k in placeholders}
    return (s % {k: "%%(%s)s" % mapping[k] for k in placeholders},
            {mapping[k]: v for k, v in placeholders.items()})


class Timer:
    class TimerState(enum.Enum):
        NONE = None
        RUNNING = "running"
        STOPPED = "stopped"

    def __init__(self):
        self.state = Timer.TimerState.NONE
        self._runtime = None

    def __enter__(self):
        self.start = time.time()
        self.state = Timer.TimerState.RUNNING
        return self

    def __exit__(self, exc_type=None, exc_val=None, exc_tb=None):
        end = time.time()
        self._runtime = end - self.start
        self.state = Timer.TimerState.STOPPED

    @property
    def runtime(self):
        if self._runtime:
            return self._runtime
        elif self.start:
            return time.time() - self.start
        return None
