from typing import (Any, Generator)

from eavlib.graph import Graph
from eavlib.gremlin.steps import ComputerStep, ComputerSteps
from eavlib.gremlin.traversal import GraphTraversal, Step


class ComputerStepFactory:
    def __init__(self, graph: Graph) -> None:
        self.graph = graph

    def make(self, step: Step, **kwargs: Any) -> ComputerStep:
        cls = getattr(ComputerSteps, "%s%sStep" % (
            step.op[0].capitalize(), step.op[1:]))
        if cls:
            return cls(self.graph, *step.args, **kwargs)
        raise ValueError("There is no ComputerStep for opcode %s" % step.op)


class Optimiser:
    _start_ops = ("V", "hasLabel", "has")

    _possible_chaining = {
        "has": ("has",),
        "order": ("by",),
        }

    def __init__(self, traversal: GraphTraversal) -> None:
        self.traversal = traversal

    @classmethod
    def _set_modifier(cls, options, modifier):
        if "modifiers" not in options:
            options["modifiers"] = {}
        if modifier.op not in options["modifiers"]:
            options["modifiers"][modifier.op] = []
        options["modifiers"][modifier.op].append(modifier)

    def optimise(self) -> Generator[ComputerStep, None, None]:
        step_factory = ComputerStepFactory(self.traversal.graph)

        def _step(step, **kwargs):
            return step_factory.make(step, **kwargs)

        instructions = iter(self.traversal.ast.step_instructions)

        if not instructions:
            return

        try:
            cur, options = next(instructions), {}
        except StopIteration:
            return  # empty traversal

        if cur.op == "V" and not cur.args:
            # try to optimise away very expensive "empty" V() as first step
            try:
                step = next(instructions)
            except StopIteration:
                # only one step traversal
                yield _step(cur, **options)
                return
            else:
                if step.op not in self._start_ops:
                    # second step does not support being the first step, so we
                    # yield the original first step...
                    yield _step(cur, **options)
                cur = step

        for step in instructions:
            if cur.op in self._possible_chaining:
                if step.op in self._possible_chaining[cur.op]:
                    Optimiser._set_modifier(options, step)
                    continue

            # No modifiers, optimised step complete
            yield _step(cur, **options)
            cur, options = step, {}

        yield _step(cur, **options)
