# flake8: noqa: E501

def customize_query(query, schema_name):
    return query.replace('{SCHEMA_NAME}', schema_name)


class SqlQueryDict:
    def __init__(self, schema_name):
        self.storage_deletion_query            = customize_query(self.STORAGE_DELETION,                  schema_name)
        self.storage_create_schema_query       = customize_query(self.STORAGE_CREATE_SCHEMA,             schema_name)
        self.storage_check_schema_query        = customize_query(self.STORAGE_CHECK_SCHEMA,              schema_name)
        self.storage_is_initialised_query      = customize_query(self.STORAGE_IS_INITIALISED,            schema_name)
        self.storage_read_version_query        = customize_query(self.STORAGE_READ_VERSION,              schema_name)
        self.storage_init_version_table_query  = customize_query(self.STORAGE_INIT_VERSION_TABLE,        schema_name)
        self.storage_set_version_query         = customize_query(self.STORAGE_SET_VERSION_QUERY,         schema_name)

        self.sequence_next_id_query            = customize_query(self.SEQUENCE_NEXT_ID,                  schema_name)
        self.sequence_next_ids_query           = customize_query(self.SEQUENCE_NEXT_IDS,                 schema_name)

        self.model_read_id_by_name_query       = customize_query(self.MODEL_READ_ID_BY_NAME,             schema_name)
        self.model_read_model_query            = customize_query(self.MODEL_READ_MODEL,                  schema_name)
        self.model_read_entities_query         = customize_query(self.MODEL_READ_ENTITIES,               schema_name)
        self.model_read_parents_query          = customize_query(self.MODEL_READ_PARENTS,                schema_name)
        self.model_read_attributes_query       = customize_query(self.MODEL_READ_ATTRIBUTES,             schema_name)
        self.model_read_relations_query        = customize_query(self.MODEL_READ_RELATIONS,              schema_name)
        self.model_write_model_query           = customize_query(self.MODEL_WRITE_MODEL,                 schema_name)
        self.model_write_entity_query          = customize_query(self.MODEL_WRITE_ENTITY,                schema_name)
        self.model_write_attribute_query       = customize_query(self.MODEL_WRITE_ATTRIBUTE,             schema_name)
        self.model_write_relation_query        = customize_query(self.MODEL_WRITE_RELATION,              schema_name)
        self.model_write_parent_query          = customize_query(self.MODEL_WRITE_PARENT,                schema_name)
        self.model_delete_relations_query      = customize_query(self.MODEL_DELETE_RELATIONS,            schema_name)
        self.model_delete_attributes_query     = customize_query(self.MODEL_DELETE_ATTRIBUTES,           schema_name)
        self.model_delete_parents_query        = customize_query(self.MODEL_DELETE_PARENTS,              schema_name)
        self.model_delete_entities_query       = customize_query(self.MODEL_DELETE_ENTITIES,             schema_name)
        self.model_delete_model_query          = customize_query(self.MODEL_DELETE_MODEL,                schema_name)

        self.object_read_instance_query        = customize_query(self.OBJECT_READ_OBJECT,                schema_name)
        self.object_read_all_attributes_query  = customize_query(self.OBJECT_READ_ALL_ATTRIBUTES,        schema_name)
        self.object_read_some_attributes_query = customize_query(self.OBJECT_READ_SOME_ATTRIBUTES,       schema_name)
        self.object_read_all_relations_query   = customize_query(self.OBJECT_READ_ALL_RELATIONS,         schema_name)
        self.object_read_some_relations_query  = customize_query(self.OBJECT_READ_SOME_RELATIONS,        schema_name)
        self.object_read_oids_4_entity_query   = customize_query(self.OBJECT_READ_ENTITY_IDS,            schema_name)
        self.object_read_ids_4_entities_query  = customize_query(self.OBJECT_READ_ENTITIES_IDS,          schema_name)
        self.object_read_ids_4_values_query    = customize_query(self.OBJECT_READ_IDS_FOR_VALUE,         schema_name)
        self.object_read_related_ids_query     = customize_query(self.OBJECT_READ_RELATED_IDS,           schema_name)

        self.object_write_instance_query       = customize_query(self.OBJECT_WRITE_OBJECT,               schema_name)
        self.object_write_attribute_query      = customize_query(self.OBJECT_WRITE_ATTRIBUTE,            schema_name)
        self.object_write_relation_query       = customize_query(self.OBJECT_WRITE_RELATION,             schema_name)
        self.object_write_inheritance_query    = customize_query(self.OBJECT_WRITE_INHERITANCE,          schema_name)

        self.object_delete                     = customize_query(self.OBJECT_DELETE,                     schema_name)
        self.object_delete_attribute           = customize_query(self.OBJECT_DELETE_ATTRIBUTE,           schema_name)
        self.object_delete_all_attributes      = customize_query(self.OBJECT_DELETE_ALL_ATTRIBUTES,      schema_name)
        self.object_delete_relation_instance   = customize_query(self.OBJECT_DELETE_RELATION_ONE,        schema_name)
        self.object_delete_relation            = customize_query(self.OBJECT_DELETE_RELATION,            schema_name)
        self.object_delete_all_relations       = customize_query(self.OBJECT_DELETE_ALL_RELATIONS,       schema_name)
        self.object_delete_back_relation       = customize_query(self.OBJECT_DELETE_BACK_RELATION,       schema_name)
        self.object_delete_all_back_relations  = customize_query(self.OBJECT_DELETE_ALL_BACK_RELATIONS,  schema_name)

    #####
    ####
    ###     STORAGE
    ##
    #
    STORAGE_DELETION = '''
        DROP SCHEMA IF EXISTS {SCHEMA_NAME} CASCADE;
        '''

    STORAGE_CREATE_SCHEMA = '''
        CREATE SCHEMA IF NOT EXISTS {SCHEMA_NAME};
        '''

    STORAGE_CHECK_SCHEMA = '''
        SELECT * FROM information_schema.schemata WHERE schema_name = %s;
        '''

    STORAGE_IS_INITIALISED = '''
        SELECT 1 FROM information_schema.tables WHERE table_schema = '{SCHEMA_NAME}' AND table_name = 'version';
        '''

    STORAGE_READ_VERSION = '''
        SELECT schema_version FROM {SCHEMA_NAME}.version
        '''

    STORAGE_INIT_VERSION_TABLE = '''
        CREATE TABLE IF NOT EXISTS {SCHEMA_NAME}.version (
            schema_version integer NOT NULL
        );
        '''

    STORAGE_SET_VERSION_QUERY = '''
        TRUNCATE TABLE {SCHEMA_NAME}.version;
        INSERT INTO {SCHEMA_NAME}.version (schema_version)
        VALUES (%s);
        '''

    #####
    ####
    ###     ID GENERATION
    ##
    #

    SEQUENCE_NEXT_ID = '''SELECT nextval(CONCAT('{SCHEMA_NAME}.', %s));'''
    SEQUENCE_NEXT_IDS = '''
        SELECT nextval(CONCAT('{SCHEMA_NAME}.', %s))
        FROM generate_series(1, %s);
    '''


    #####
    ####
    ###     MODEL
    ##
    #
    MODEL_READ_ID_BY_NAME = '''
        SELECT m.id
        FROM {SCHEMA_NAME}.model m
        WHERE m.name = %s;
        '''

    MODEL_READ_MODEL = '''
        SELECT
          m.id           AS model_id,
          m.name         AS model_name,
          m.description  AS model_description
        FROM {SCHEMA_NAME}.model m
        WHERE m.id = %s;
        '''

    MODEL_READ_ENTITIES = '''
        SELECT
          e.id          AS entity_id,
          e.name        AS entity_name,
          e.description AS entity_description
        FROM {SCHEMA_NAME}.model m
        JOIN {SCHEMA_NAME}.model_entity e ON m.id = e.model_id
        WHERE m.id = %s
        '''

    MODEL_READ_PARENTS = '''
        SELECT
          e.id        AS entity_id,
          i.parent_id AS parent_id
        FROM {SCHEMA_NAME}.model m
        JOIN {SCHEMA_NAME}.model_entity e ON m.id = e.model_id
        JOIN {SCHEMA_NAME}.model_inheritance i ON e.id = i.entity_id
        WHERE m.id = %s
        '''

    MODEL_READ_ATTRIBUTES = '''
        SELECT
          e.id          AS entity_id,
          a.id          AS attribute_id,
          a.name        AS attribute_name,
          a.description AS attribute_description,
          a.required    AS attribute_is_required
        FROM {SCHEMA_NAME}.model m
        JOIN {SCHEMA_NAME}.model_entity e ON m.id = e.model_id
        JOIN {SCHEMA_NAME}.model_attribute a ON e.id = a.entity_id
        WHERE m.id = %s
        '''

    MODEL_READ_RELATIONS = '''
        SELECT
          e.id          AS entity_id,
          r.id          AS relation_id,
          r.name        AS relation_name,
          r.description AS relation_description,
          r.target_id   AS relation_target_id,
          r.card_min    AS relation_card_min,
          r.card_max    AS relation_card_max
        FROM {SCHEMA_NAME}.model m
        JOIN {SCHEMA_NAME}.model_entity e ON m.id = e.model_id
        JOIN {SCHEMA_NAME}.model_relation r ON e.id = r.source_id
        WHERE m.id = %s
        '''

    MODEL_WRITE_MODEL = '''
        INSERT INTO {SCHEMA_NAME}.model
        (id, name, description)
        VALUES(%s, %s, %s);
        '''

    MODEL_WRITE_ENTITY = '''
        INSERT INTO {SCHEMA_NAME}.model_entity
        (id, name, description, model_id)
        VALUES(%s, %s, %s, %s);
        '''

    MODEL_WRITE_ATTRIBUTE = '''
        INSERT INTO {SCHEMA_NAME}.model_attribute
        (id, entity_id, name, description, required)
        VALUES(%s, %s, %s, %s, %s);
        '''

    MODEL_WRITE_RELATION = '''
        INSERT INTO {SCHEMA_NAME}.model_relation
        (id, source_id, target_id, name, description, card_min, card_max)
        VALUES(%s, %s, %s, %s, %s, %s, %s);
        '''

    MODEL_WRITE_PARENT = '''
        INSERT INTO {SCHEMA_NAME}.model_inheritance
        (entity_id, parent_id)
        VALUES(%s, %s);
        '''

    MODEL_DELETE_RELATIONS = '''
        DELETE FROM {SCHEMA_NAME}.model_relation r
        WHERE r.source_id in (
          SELECT e.id
          FROM {SCHEMA_NAME}.model m
          JOIN {SCHEMA_NAME}.model_entity e ON m.id = e.model_id
          WHERE m.id = %s
        );
        '''

    MODEL_DELETE_ATTRIBUTES = '''
        DELETE FROM {SCHEMA_NAME}.model_attribute a
        WHERE a.entity_id in (
          SELECT e.id
          FROM {SCHEMA_NAME}.model m
          JOIN {SCHEMA_NAME}.model_entity e ON m.id = e.model_id
          WHERE m.id = %s
        );
        '''

    MODEL_DELETE_PARENTS = '''
        DELETE FROM {SCHEMA_NAME}.model_inheritance i
        WHERE i.entity_id in (
          SELECT e.id
          FROM {SCHEMA_NAME}.model m
          JOIN {SCHEMA_NAME}.model_entity e ON m.id = e.model_id
          WHERE m.id = %s
        );
        '''

    MODEL_DELETE_ENTITIES = '''
        DELETE FROM {SCHEMA_NAME}.model_entity e
        WHERE e.model_id = %s;
        '''

    MODEL_DELETE_MODEL = '''
        DELETE FROM {SCHEMA_NAME}.model m
        WHERE m.id = %s;
        '''


    #####
    ####
    ###     OBJECT
    ##
    #
    OBJECT_WRITE_OBJECT      = '''INSERT INTO {SCHEMA_NAME}.object             VALUES %s;'''
    OBJECT_WRITE_ATTRIBUTE   = '''INSERT INTO {SCHEMA_NAME}.object_attribute   VALUES %s;'''
    OBJECT_WRITE_RELATION    = '''INSERT INTO {SCHEMA_NAME}.object_relation    VALUES %s;'''
    OBJECT_WRITE_INHERITANCE = '''INSERT INTO {SCHEMA_NAME}.object_inheritance VALUES %s;'''

    OBJECT_READ_OBJECT = '''
        SELECT
          o.id        AS oid,
          o.entity_id AS eid
        FROM {SCHEMA_NAME}.object o
        WHERE o.id IN %s;
        '''

    OBJECT_READ_ENTITY_IDS = '''
        SELECT
          o.id        AS oid,
          o.entity_id AS eid
        FROM {SCHEMA_NAME}.object o
        WHERE o.entity_id IN %s;
        '''

    OBJECT_READ_IDS_FOR_VALUE = '''
        SELECT
          a.object_id AS oid
        FROM {SCHEMA_NAME}.object_attribute a
        WHERE a.attribute_id = %s
          AND a.value = %s;
        '''

    OBJECT_READ_ALL_ATTRIBUTES = '''
        SELECT
          a.object_id    AS oid,
          a.attribute_id AS aid,
          a.value        AS val
        FROM {SCHEMA_NAME}.object_attribute a
        WHERE a.object_id IN %s;
        '''

    OBJECT_READ_SOME_ATTRIBUTES = '''
        SELECT
          a.object_id    AS oid,
          a.attribute_id AS aid,
          a.value        AS val
        FROM {SCHEMA_NAME}.object_attribute a
        WHERE a.object_id IN %s
          AND a.attribute_id IN %s;
        '''

    OBJECT_READ_ALL_RELATIONS = '''
        SELECT
          r.object_id   AS oid,
          r.relation_id AS rid,
          r.target_id   AS tid,
          o.entity_id   AS tent
        FROM {SCHEMA_NAME}.object_relation r
        JOIN {SCHEMA_NAME}.object o ON o.id = r.target_id
        WHERE r.object_id IN %s;
        '''

    OBJECT_READ_SOME_RELATIONS = '''
        SELECT
          r.object_id   AS oid,
          r.relation_id AS rid,
          r.target_id   AS tid,
          o.entity_id   AS tent
        FROM {SCHEMA_NAME}.object_relation r
        JOIN {SCHEMA_NAME}.object o ON o.id = r.target_id
        WHERE r.object_id IN %s
          AND r.relation_id IN %s;
        '''

    OBJECT_READ_ENTITIES_IDS = '''
        SELECT
          e.name AS entity_name,
          o.id   AS id
        FROM {SCHEMA_NAME}.object o
        JOIN {SCHEMA_NAME}.model_entity e ON e.id = o.entity_id
        WHERE o.entity_id IN %s;
        '''

    OBJECT_READ_RELATED_IDS = '''
        SELECT
          e.name      AS name,
          r.target_id AS oid
        FROM {SCHEMA_NAME}.object_relation r
        JOIN {SCHEMA_NAME}.object o ON r.target_id = o.id
        JOIN {SCHEMA_NAME}.model_entity e ON o.entity_id = e.id
        WHERE r.object_id = %s
          AND r.relation_id = %s;
        '''

    OBJECT_DELETE_ATTRIBUTE = '''
        DELETE FROM {SCHEMA_NAME}.object_attribute WHERE (object_id, attribute_id) IN (%s);
        '''

    OBJECT_DELETE_ALL_ATTRIBUTES = '''
        DELETE FROM {SCHEMA_NAME}.object_attribute WHERE object_id IN (%s);
        '''

    OBJECT_DELETE_RELATION_ONE = '''
        DELETE FROM {SCHEMA_NAME}.object_relation WHERE (object_id, relation_id, target_id) IN (%s);
        '''

    OBJECT_DELETE_RELATION = '''
        DELETE FROM {SCHEMA_NAME}.object_relation WHERE (object_id, relation_id) IN (%s);
        '''

    OBJECT_DELETE_ALL_RELATIONS = '''
        DELETE FROM {SCHEMA_NAME}.object_relation WHERE object_id IN (%s);
        '''

    OBJECT_DELETE_BACK_RELATION = '''
        DELETE FROM {SCHEMA_NAME}.object_relation WHERE (target_id, relation_id) IN (%s);
        '''

    OBJECT_DELETE_ALL_BACK_RELATIONS = '''
        DELETE FROM {SCHEMA_NAME}.object_relation WHERE target_id IN (%s);
        '''

    OBJECT_DELETE = '''
        DELETE FROM {SCHEMA_NAME}.object WHERE id IN (%s);
        '''
