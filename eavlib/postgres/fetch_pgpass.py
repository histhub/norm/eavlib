# flake8: noqa: C901

def fetch_pgpass_password(user, host, port, database):
    """Returns the password for a user, host, port, and database
    combination by looking it up in a PostgreSQL .pgpass file
    (located using the PGPASSFILE environment variable).

    Raises a runtime exception in case the PGPASSFILE variable
    is not defined, the password file cannot be read, or it
    doesn't contain a password line for the given user, host,
    port and database combination.
    """

    # print("user   = {}".format(user))
    # print("host   = {}".format(host))
    # print("port   = {}".format(port))
    # print("db     = {}".format(database))
    # Read environment variable PGPASSFILE

    common_msg = """Expecting a PGPASSFILE environment variable pointing to a
    PostgreSQL .pgpass passwords file, which is a text file with the following
    structure:
       <host>:<port>:<database>:<user>:<password>
    e.g. localhost:5432:*:master:secret, where * means any value in that field.
    """

    try:
        import os
        pgpass_filename = os.environ['PGPASSFILE']
    except KeyError:
        raise RuntimeError('PGPASSFILE variable not found!' + common_msg)

    if not len(pgpass_filename.strip()):
        raise RuntimeError(
            'PGPASSFILE environment variable is empty!' + common_msg)

    # Read .pgpass content
    try:
        with open(pgpass_filename, 'r') as pgpass_file:
            lines = pgpass_file.readlines()
    except FileNotFoundError:
        msg = "File '{}' in PGPASSFILE variable cannot be read!".format(
            pgpass_filename) + common_msg
        raise RuntimeError(msg)

    # Find the first line with password information
    password = None
    for line in lines:
        if not password:
            line_fields = line.strip().split(':')
        if line_fields[0] != '*':
            if line_fields[0] != host:
                continue
        if line_fields[1] != '*':
            if line_fields[1] != str(port):
                continue
        if line_fields[2] != '*':
            if line_fields[2] != database:
                continue
        if line_fields[3] != '*':
            if line_fields[3] != user:
                continue
        # If the password contains colons, they appear backslashed
        # in the .pgpass file. Elements from 4th field onwards have
        # to be joined removing possible trailing back-slashes.
        return ':'.join([
            w[0:-1] if w.endswith('\\') else w for w in line_fields[4:]
            ])

    fmt = 'File "{}": no password found for user={}, host={}, port={}, db={}'
    msg = fmt.format(pgpass_filename, user, host, port, database) + common_msg
    raise RuntimeError(msg)
