# flake8: noqa: E501

from psycopg2 import connect

from eavlib.core import EavConnection
from eavlib.postgres import fetch_pgpass_password
from eavlib.postgres import parse_connection_string
from .sql_query_dict import SqlQueryDict


def build_eav_connection(connection_string):
    connection_data = parse_connection_string(connection_string)
    database_passwd = fetch_pgpass_password(
        connection_data.user,
        connection_data.host,
        connection_data.port,
        connection_data.database)
    sql_query_dict = SqlQueryDict(connection_data.schema)

    # Creates a Python DB API 2.0 connection which one of the
    # constituents of the EavConnection instance.
    try:
        psycopg2_connection = connect(
            host=connection_data.host,
            port=connection_data.port,
            database=connection_data.database,
            user=connection_data.user,
            password=database_passwd)
        psycopg2_connection.set_client_encoding('UTF8')
    except Exception as e:
        error_msg = str(e)
        if error_msg.startswith('FATAL:  '):
            error_msg = error_msg[8:]
        raise RuntimeError(error_msg)

    # Check that the (postgres) schema is available.
    #
    # TODO: checking the existence of schema doesn't make sense for connections that
    #       want to create a schema, for example those used by a StorageManager.
    #
    # cursor = psycopg2_connection.cursor()
    # cursor.execute(sql_query_dict.storage_check_schema_query, (connection_data.schema,))
    # if len(cursor.fetchall()) == 0:
    #     stencil = 'schema "{}" does not exist (database "{}")'
    #     raise RuntimeError(stencil.format(connection_data.schema, connection_data.database))

    return EavConnection(psycopg2_connection, sql_query_dict)
