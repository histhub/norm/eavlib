# TODO: error message are not very informative

from re import compile
from collections import namedtuple


def parse_connection_string(connection_string):
    '''
    Parses a database connection string in the form

        "{user}@{host}:{port}/{database}.{schema}"'

    and returns its constitutive elements bundled in a
    named tuple.
    '''

    conn_regex = compile(r'^([^@]+)@([^:]+):([^/]+)/([^.]+)\.(.*)$')

    match = conn_regex.match(connection_string)
    if not match:
        error_template = "syntax error in connection string '{}'"
        raise RuntimeError(error_template.format(connection_string))

    Connection = namedtuple('Connection', 'user host port database schema')
    return Connection(match[1], match[2], match[3], match[4], match[5])
