# Metadata

__package_name__ = "eavlib"
__description__ = ("A library to work with a graph stored in a PostgreSQL "
                   "database using an EAV/CR schema")

__author__ = "SSRQ-SDS-FDS Law Sources Foundation of the Swiss Lawyers Society"
__copyright__ = ("Copyright 2018-2021 %s" % __author__)
__license__ = "GPLv3"
__maintainer__ = __author__
# Versions as per PEP 440 (https://www.python.org/dev/peps/pep-0440/)
__version_info__ = (0, 3, 1)
__version__ = "0.3.1"
