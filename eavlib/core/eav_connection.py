from collections import namedtuple

#  An EavConnection encapsulates a Python DB API 2.0 connection object
#  and a query dictionary containing the SQL queries that can be used
#  by all EAV access related objects.
#
#  Both, the connection and the queries dictionary, are dependent on
#  the underlying database or driver library, so that EavConnection
#  instances need a 'build_eav_connection' factory to be created.
EavConnection = namedtuple('EavConnection', ('connection', 'query_dict'))
