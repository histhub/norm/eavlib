import abc
import enum
import logging
import pathlib
import pkg_resources

import eavlib.connection

from typing import TYPE_CHECKING

from eavlib import __package_name__

if TYPE_CHECKING:
    from typing import (Any, Callable)
    from eavlib.connection import EAVConnection

LOG = logging.getLogger(__name__)


class StorageManager:
    """Create or destroy EAV repositories."""

    _latest_version = 6

    def __init__(self, eav_connection: "EAVConnection", debug: bool = False) \
            -> None:
        """Create a new StorageManager.

        :param eav_connection: The EAV connection to use.
        :type eav_connection: eavlib.connection.EAVConnection
        """
        self.conn = eav_connection
        if isinstance(eav_connection, eavlib.connection.PostgresEAVConnection):
            self.migrator = PostgresStorageMigrator(eav_connection, debug)
        else:
            raise NotImplementedError("Connection dialect is not supported")

    def create_schema(self) -> None:
        """Creates the schema specified in the connection URL."""
        with self.conn.cursor() as cursor:
            cursor.execute(self.conn.query_dict.storage_create_schema_query)

    def schema_exists(self) -> bool:
        """Returns if the schema to be initialised exists.

        :returns: bool -- True if the schema exists in the RDBMS.
        """
        with self.conn.cursor() as cursor:
            cursor.execute(self.conn.query_dict.storage_check_schema_query, (
                self.conn.schema,))

            return cursor.fetchone() is not None

    def create(self) -> None:
        """Creates an EAV repository ready to be used.

        :raises RuntimeError: if a repository with the same name already exists
        or the given name is not a valid database name.
        """
        with self.conn.cursor() as cursor:
            cursor.execute(
                self.conn.query_dict.storage_init_version_table_query)

            cursor.execute(self.conn.query_dict.storage_read_version_query)
            has_current_version = cursor.fetchone() is not None

            # Only set the version to 0 if the version table is empty.
            # NOTE: The version table could be non-empty if create() is called
            # on a pre-initialised schema.
            if not has_current_version:
                cursor.execute(
                    self.conn.query_dict.storage_set_version_query, (0,))

        self.conn.commit()
        self.conn.reread_metadata()

    def is_created(self) -> bool:
        """Returns if the schema referred to by the EAV connection has been
        initialised.

        :returns: bool -- True if the schema has been initialised.
        """
        with self.conn.cursor() as cursor:
            cursor.execute(self.conn.query_dict.storage_is_initialised_query)
            return cursor.fetchone() is not None

    def migration_needed(self, version: int = _latest_version) -> bool:
        """Returns whether a migration is needed to to get the schema to
        `version`.

        :param version: The target version. If omitted, the latest version.
        :type version: int
        :returns: bool -- True if a migration is needed.
        """
        return self.migrator.current_version() < version

    def migrate(self, version: int = _latest_version) -> None:
        """Migrate the schema to `version`.

        :param version: The target version. If omitted, the latest version.
        :type version: int
        """
        current = self.migrator.current_version()

        if current >= version:
            LOG.info("Schema is already at version %u. Nothing to do.",
                     current)
            return

        if version > self._latest_version:
            raise ValueError(
                "There is no migration to version %u available!" % version)

        LOG.info("Schema is currently at version: %d", current)

        for state_version in range(current, version):
            # Migrate to next version
            self.migrator.run_migration(state_version + 1)

    def destroy(self) -> None:
        """Destroys an EAV repository deleting all its contents.

        Silently doesn't raise any exception if the repository
        name doesn't exist.
        """
        with self.conn.cursor() as cursor:
            cursor.execute(self.conn.query_dict.storage_deletion_query)
        self.conn.commit()


class StorageMigrator(metaclass=abc.ABCMeta):
    Hooks = enum.Enum("Hooks", {
        "before_migration": "before_migration",
        "after_migration": "after_migration",
        "failed_migration": "failed_migration",
        "log_message": "log_message",
        })

    def __init__(self, eav_connection: "EAVConnection",
                 debug: bool) -> None:
        self.conn = eav_connection
        self.hooks = {name: list() for name in self.Hooks}

    def _call_hooks(self, name: str, *args: "Any", **kwargs: "Any") -> None:
        for hook in filter(callable, self.hooks[self.Hooks(name)]):
            hook(*args, **kwargs)

    def register_hooks(self, **kwargs: "Callable[..., None]") -> None:
        """Register hook functions to be called by the migrator.

        :param kwargs: name=hook pair of the hook to register a callback for.
        :type kwargs: callable
        """
        for name, hook in kwargs.items():
            self.hooks[self.Hooks(name)].append(hook)

    @abc.abstractmethod
    def current_version(self) -> int:
        """Returns the current schema version.

        :returns: int -- the current schema version
        """

    @abc.abstractmethod
    def run_migration(self, version: int) -> None:
        """Execute the migration scripts for a specific schema version.

        :param version: the schema version to execute migrations scripts for.
        :type version: int
        :raises: on errors.
        """


class PostgresStorageMigrator(StorageMigrator):
    migrations_dir = pkg_resources.resource_filename(
        __package_name__, "files/db/pgsql")

    def __init__(self, eav_connection: "EAVConnection",
                 debug: bool) -> None:
        super().__init__(eav_connection, debug)

        class PgNoticesWriter:
            def __init__(self, callback: "Callable[[str], None]") -> None:
                if not callable(callback):
                    raise ValueError("callback is not callable")
                self.callback = callback

            def append(self, msg: str) -> None:
                self.callback(msg)

        def log_msg(msg: str) -> None:
            self._call_hooks(self.Hooks.log_message, logging.INFO, msg)
        eav_connection.connection.notices = PgNoticesWriter(log_msg)

    def current_version(self) -> int:
        with self.conn.cursor() as cursor:
            cursor.execute(self.conn.query_dict.storage_read_version_query)
            return int(cursor.fetchone()[0])

    def run_migration(self, version: int) -> None:
        import psycopg2
        from psycopg2.extensions import ISOLATION_LEVEL_SERIALIZABLE \
            as ISOL_SERIALIZABLE

        connection = self.conn.connection
        num_scripts_executed = 0

        if connection.readonly:
            raise RuntimeError(
                "Cannot perform migration because connection is read-only")
        if connection.autocommit:
            raise RuntimeError(
                "Cannot perform migration because connection has auto-commit "
                "enabled.")

        self._call_hooks(self.Hooks.before_migration, version)
        for _ in range(5):  # up to 5 retries in case of serialization failure
            old_isolation_level = connection.isolation_level
            try:
                # Make sure we're on a fresh transaction before starting the
                # migration
                self.conn.commit()

                if old_isolation_level is not ISOL_SERIALIZABLE:
                    connection.isolation_level = ISOL_SERIALIZABLE

                # Find all migration scripts for this version and execute them
                # in alphabetical order
                for scr_path in sorted(pathlib.Path(
                        self.migrations_dir).glob("%04u_*.sql" % version)):
                    with self.conn.cursor() as cursor:
                        try:
                            with open(scr_path, "r", encoding="utf-8") as scr:
                                sql = psycopg2.sql.SQL(scr.read()).format(
                                    SCHEMA_NAME=psycopg2.sql.Identifier(
                                        self.conn.schema))
                                cursor.execute(sql)
                        except Exception as e:
                            self.conn.rollback()
                            raise RuntimeError(
                                "An error occurred while processing file %s" %
                                (scr_path)) from e

                    num_scripts_executed += 1

                if not num_scripts_executed:
                    raise ValueError(
                        "No migration to version %u found." % version)

                # Bump schema version if all scripts have successfully applied
                with self.conn.cursor() as cursor:
                    cursor.execute(
                        self.conn.query_dict.storage_set_version_query, (
                            version,))

                self.conn.commit()
            except psycopg2.OperationalError as e:
                self.conn.rollback()
                if e.pgcode != psycopg2.errorcodes.SERIALIZATION_FAILURE:
                    raise
                # silent retry
            except Exception as e:
                self._call_hooks(self.Hooks.failed_migration, version, e)
                self.conn.rollback()
                raise
            else:
                break  # break to not trigger for's else
            finally:
                # Reset connection isolation level if needed
                if connection.isolation_level is not old_isolation_level:
                    connection.isolation_level = old_isolation_level
        else:
            raise RuntimeError("Maximum number of retries exhausted")

        self._call_hooks(
            self.Hooks.after_migration, version, num_scripts_executed)

        self.conn.reread_metadata()
