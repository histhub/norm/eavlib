class RadialPathSet:
    '''
        A radial path set is an entity name and a list of path departing
        radially from it (in some EAV model).

        Radial paths consist in either a single attribute name of the
        central entity, or in one or more dot separated relation names
        that, followed by an optional attribute name. For example:

            [
                'id',
                'lang',
                'forenames.order',
                'forenames.forename.spelling',
                'surnames.order',
                'surnames.namelink.spelling',
                'surnames.surname.spelling',
                'standard_name'
            ]

        Of course, the first relation in each path is a valid relation of
        the central entity, the second a valid relation of the target en-
        tity of the previous one, and so on.

        This class is only a notational artifact and no consistency can
        be made on the soundness of the paths and actual existence of the
        central class. It is the responsibility of producers to generate
        consistent objects.

        Also no database ids are stored in radial path set, that contains
        human readable names only.
    '''

    def __init__(self, central_entity, path_list):
        '''
                central_entity      name of the entity being the radiation
                                    center

                path_list           the list of path originating from the
                                    central entity

        '''
        self.central_entity = central_entity
        self.path_list = path_list

    def __str__(self):
        items = list()
        items.append("Start entity : {}".format(self.central_entity))

        for path in self.path_list:
            items.append("  {}".format(path))

        return '\n'.join(items)


def radial_path_set_from_entity(eav_model, entity_name, stop_names=list()):
    '''
        Computes radial paths from a given entity extending through the given
        EAV model until they find an entity without any further relation or a
        relation leading to an entity in the stop_names set.

            eav_model       An EAV model instance to locate further relations
                            and attributes.

            entity_name     A valid entity name for the given model that acts
                            as radiation center

            stop_names      A list of entity names where propagation will stop

        Infinite recursions might be possible, specially when the stop_names
        list is empty.
    '''

    def __path_collector(model, start_entity, current_entity, path_prefix,
                         stop_entities):
        path_list = list()
        if len(path_prefix) > 0 and current_entity == start_entity:
            return path_list

        for attribute in current_entity.all_attributes:
            path_to_attr = "{}{}".format(path_prefix, attribute.name)
            path_list.append(path_to_attr)

        for relation in current_entity.all_relations:
            next_entity = relation.target_entity
            if next_entity in stop_entities:
                stop_entity_path_prefix = "{}{}".format(
                    path_prefix, relation.name)
                path_list.append(stop_entity_path_prefix)
            else:
                next_entity_path_prefix = "{}{}.".format(
                    path_prefix, relation.name)
                path_list.extend(__path_collector(
                    model, start_entity, next_entity, next_entity_path_prefix,
                    stop_entities))

        return path_list

    if type(stop_names) == str:
        stop_names = [stop_names]

    # Compute subclasses of the given stop classes since they are stop
    # entities as well.
    children_lists = [eav_model.entity(n).all_children for n in stop_names]
    stop_entities = set(e for sublist in children_lists for e in sublist)
    start_entity = eav_model.entity(entity_name)

    path_list = __path_collector(
        eav_model, start_entity, start_entity, '', stop_entities)
    return RadialPathSet(entity_name, path_list)
