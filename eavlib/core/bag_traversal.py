from collections import namedtuple


def smaller_depth_dict(eav_bag):
    '''
        Given an EAV bag that will act as the "root" of a web
        of other bags transitively associated to it,  returns
        a dictionary:

            { bag -> depth }

        giving for each bag the shortest path length from the
        "root" bag.
    '''

    def __smaller_depth_traverser(eav_bag, depth_dict, level):
        '''
            Helper function to build the depth dictionary.
        '''
        for nodes_list in eav_bag.relations.values():
            for node in nodes_list:

                # First time the node is visited: write path length
                # and continue traversal.
                if node not in depth_dict:
                    depth_dict[node] = level
                    __smaller_depth_traverser(node, depth_dict, level+1)
                else:
                    # Node already visited, but by a longer path from root.
                    # Adjust and continue traversal.
                    if level < depth_dict[node]:
                        depth_dict[node] = level
                        __smaller_depth_traverser(node, depth_dict, level+1)

                    # Node already visited by a shorter path: ignore
                    # and stop infinite recursion.
                    else:
                        pass

        return depth_dict

    depth_dict = {eav_bag: 0}
    __smaller_depth_traverser(eav_bag, depth_dict, 1)
    return depth_dict


def acyclic_preorder_iterator(eav_bag):
    r"""Iterator delivering this EAV bag and all the bags related transitively
    to it.

    The directed graph that these transitive relations of an EAV bag
    form might contain cycles. The presence of cycles means that some
    nodes are reachable from the initial "root" bag by more than one
    path. These nodes are reported as many times as they appear with
    the dummy flat set to True on all except the one at the shortest
    distance from the root. If there are more than one shortest path,
    the node is reported not dummy only once (see the D node below).

     Actual graph                     Reported Tree
    ---------------------------      ------------------------------

                   B                                B --- D --- E
     A -> B       / \                  A -> B      /
     A -> C      /   \                 B -> D     /
     B -> D     A     D --- E          D -> E    A
     C -> D      \   /                 A -> C     \
     D -> E       \ /                  C -> D'     \
                   C                                C --- D' (dummy)

    The reporing order is first the parent and then the child nodes in an
    undetermined order. The given bag is thus the first reported node.

    Each node is reported together with a wealth of information that consumers
    can use to build custom representations of an EAV bag.  Reported properties
    are:

      - bag       the reported node
      - depth     or distance from the root node
      - dummy     flag saying that this node (and its subnodes) is
                  completely reported elsewhere at a lower depth
      - relation  the name of the relation that led to this node
      - index     in the case of multivalued relations, the collection
                  ordinal number, or None otherwise
      - prev      the bag that is parent of this node (by the relation
                  given in the relation field)

    The previous graph is reported like this:

     bag    depth    dummy    relation    index    prev
    -----  -------  -------  ----------  -------  ------
      A       0      False      None      None     None
      B       1      False      A->B      None     A
      D       2      False      B->D      None     B
      E       3      False      D->E      None     D
      C       1      False      A->C      None     A
      D       2      True       C->D      None     C

    Note that node E is reported only once, since a traversal from D onwards is
    ruled out for the dummy nodes.

    The graph is traversed twice since a first one is used to compute the depth
    dictionary and the second for actually yielding the relations (edges). By
    computing the depths first, you are guaranteed that dummy nodes appear
    deeper in the tree resulting structure than the real nodes they point to.

    """

    class __node(namedtuple('AcyclicPreorderItem', [
            'bag',       # The reported node
            'depth',     # Distance from the root node
            'dummy',     # Flag saying that this node (and its subnodes) is
                         # completely reported elsewhere at a lower depth
            'relation',  # The name of the relation that led to this node
            'index',     # In the case of multivalued relations, the collec-
                         # tion ordinal number, or None otherwise
            'prev'       # The bag that is parent of this node (by the rela-
                         # tion given in the relation field)
            ])):
        __slots__ = ()

        def __str__(self):
            if self.index is None:
                relation_name = self.relation if self.relation else ""
            else:
                relation_name = "{}[{}]".format(self.relation, self.index)
            this_node = "{} ({})".format(self.bag.entity_name, self.bag.eav_id)
            if self.prev is None:
                prev_node = ''
            else:
                prev_node = "{} ({})".format(
                    self.prev.entity_name, self.prev.eav_id)

            stencil = "{:<2} {:<5} {:<30} {:-<30}---{:-<22}>"
            return stencil.format(
                self.depth,
                'dummy' if self.dummy else '',
                this_node,
                prev_node,
                relation_name
            )

    def __acyclic_preorder_generator(eav_bag, depth_dict, level):
        '''
            Helper recursive funtion.
        '''
        for relation_name, related_list in eav_bag.relations.items():
            if len(related_list) == 1:
                target_bag = related_list[0]
                if depth_dict[target_bag] < level:
                    # node reached by a path longer than the shortest depth,
                    # it should be reported as dummy.
                    yield __node(
                        target_bag, level, True, relation_name, None, eav_bag)
                else:
                    depth_dict[target_bag] = 0
                    yield __node(
                        target_bag, level, False, relation_name, None, eav_bag)
                    yield from __acyclic_preorder_generator(
                        target_bag, depth_dict, level+1)

            elif len(related_list) > 1:
                for i, target_bag in enumerate(related_list):
                    if depth_dict[target_bag] < level:  # already visited node
                        yield __node(
                            target_bag, level, True, relation_name, i, eav_bag)
                    else:
                        depth_dict[target_bag] = 0
                        yield __node(
                            target_bag, level, False, relation_name, i,
                            eav_bag)
                        yield from __acyclic_preorder_generator(
                            target_bag, depth_dict, level+1)
            else:
                pass

    depth_dict = smaller_depth_dict(eav_bag)
    yield __node(eav_bag, 0, False, None, None, None)
    yield from __acyclic_preorder_generator(eav_bag, depth_dict, 1)


def nodes_iterator(eav_bag):
    """Iterator that delivers the given bag (in first place) and then all bags
    related by any object property to the given bag recursively in an
    undetermined order, reporting each bag only once.
    """

    def __nodes_generator(eav_bag, visited_nodes):
        """Recursive helper function."""
        for nodes_list in eav_bag.relations.values():
            for target_node in nodes_list:
                if target_node not in visited_nodes:
                    visited_nodes.add(target_node)
                    yield target_node
                    yield from __nodes_generator(target_node, visited_nodes)

    visited_nodes = set((eav_bag,))
    yield eav_bag
    yield from __nodes_generator(eav_bag, visited_nodes)


def edges_iterator(eav_bag):
    """Generator function to iterate over all relations of an EAV bag and
    relations of related bags.
    """
    __edge = namedtuple('EavRelation', 'start relation target')

    def __edges_generator(eav_bag, visited_nodes):
        """Recursive helper function."""
        for relation_name, nodes_list in eav_bag.relations.items():
            for target_node in nodes_list:
                yield __edge(eav_bag, relation_name, target_node)
                if target_node not in visited_nodes:
                    visited_nodes.add(target_node)
                    yield from __edges_generator(target_node, visited_nodes)

    visited_nodes = set((eav_bag,))
    yield from __edges_generator(eav_bag, visited_nodes)
