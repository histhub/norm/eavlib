# flake8: noqa: E501

from enum import Enum, unique
from json import dumps as dict_to_json
from collections import namedtuple

from eavlib.core import EavBag
from .bag_traversal import nodes_iterator
from .bag_traversal import edges_iterator
from .bag_traversal import acyclic_preorder_iterator


@unique
class JsonTag(Enum):
    CLASS = '@class'
    EAV_ID = '@id'
    HREF = '@href'
    ATTRS = '@attributes'
    RELS = '@relations'


def href(prefix, entity_name, entity_id):
    return "{}/{}/{}/".format(prefix, entity_name, entity_id)


def bag_to_nested_dict(bag, prefix=None):
    """Puts the information in this bag in form of a dict of dicts that is
    readily converted into JSON.

    This function will fail if the graph of connected bags to the given bag has
    cycles. This has to be removed before calling.

    - prefix     the URI prefix for the href "meta" field, that
                 will be made of prefix / entity_name / entity_id
    """

    result = dict()

    result['@class'] = bag.entity_name
    if bag.eav_id is not None:
        result['@id'] = bag.eav_id
        if prefix:
            result['@href'] = href(prefix, bag.entity_name, bag.eav_id)

    if bag.attributes:
        result['@attributes'] = bag.attributes

    if bag.relations:
        result['@relations'] = dict()
        for name, related_objects in bag.relations.items():
            if len(related_objects) == 0:
                continue
            elif len(related_objects) == 1:
                result['@relations'][name] = bag_to_nested_dict(
                    related_objects[0], prefix)
            else:
                result['@relations'][name] = [
                    bag_to_nested_dict(obj, prefix) for obj in related_objects]

    return result


def json_to_eav_bag(json_object):
    """Creates an eav bag instance with the information contained in a JSON
    object.

    The JSON representation has attributes starting with an @ symbol that
    describe "meta" properties of the bag: its class, id, http reference. The
    '@attributes' attribute is in turn a dictionary that includes all "normal"
    attributes or information payload of the bag. The same way, the
    '@relations' dictionary includes the representations of all related bags.

    ---------------------------------------------------------------
    {
      "@class": "Person",
      "@id": 32584707,
      "@href": "https://histhub.ch/norm/api/Person/32584707",
      "@attributes": {
        "sex": "M"
      },
      "@relations": {
        "birth": {
          "@class": "Birth",
          "@id": 98745432,
          "@href": "https://histhub.ch/norm/api/Birth/98745432",
          "@attributes": {
            "year": "1992",
            "month": "09",
            "day": "12"
          }
        },
        "name": {
          "@class": "PersonName",
          "@id": 2541542,
          "@href": "https://histhub.ch/norm/api/PersonName/2541542",
          "@attributes": {
            "label": "Jean Baptiste Montgolfier"
            "forename": "Jean Baptiste",
            "surname": "Montgolfier",
            "lang": "fr"
          }
        },
        "address": {
          "@class": "Address",
          "@id": 73647721,
          "@href": "https://histhub.ch/norm/api/Address/73647721",
          "@attributes": {
            "street": "Glenmere Close",
            "postcode": "CB1 8EF"
          }
        }
      }
    }
    ---------------------------------------------------------------
    """
    if type(json_object) == dict:
        if JsonTag.CLASS.value not in json_object:
            raise RuntimeError("missing '{}' attribute".format(
                JsonTag.CLASS.value))
        bag = EavBag(
            json_object[JsonTag.CLASS.value],
            json_object.get(JsonTag.EAV_ID.value, None))
        for attribute, value in json_object.get(
                JsonTag.ATTRS.value, dict()).items():
            bag.set_attribute(attribute, value)
        for relation, sub_object in json_object.get(
                JsonTag.RELS.value, dict()).items():
            bag.add_related(relation, json_to_eav_bag(sub_object))
        return bag

    elif type(json_object) == list:
        bags_list = list()
        for sub_object in json_object:
            bags_list.append(json_to_eav_bag(sub_object))
        return bags_list

    else:
        raise RuntimeError("unexpected JSON object of type '%s'" % (
            type(json_object).__name__))


def bag_to_json_string(eav_bag, prefix=None, **kwargs):
    """Produces a JSON representation of the given bag.

    The JSON representation has attributes starting with an @ symbol that
    describe "meta" properties of the bag: its class, id, http reference. The
    '@attributes' attribute is in turn a dictionary that includes all "normal"
    attributes or information payload of the bag. The same way, the
    '@relations' dictionary includes the representations of all related bags.

    ---------------------------------------------------------------
    {
      "@class": "Person",
      "@id": 32584707,
      "@href": "https://histhub.ch/norm/api/Person/32584707",
      "@attributes": {
        "sex": "M"
      },
      "@relations": {
        "birth": {
          "@class": "Birth",
          "@id": 98745432,
          "@href": "https://histhub.ch/norm/api/Birth/98745432",
          "@attributes": {
            "year": "1992",
            "month": "09",
            "day": "12"
          }
        },
        "name": {
          "@class": "PersonName",
          "@id": 2541542,
          "@href": "https://histhub.ch/norm/api/PersonName/2541542",
          "@attributes": {
            "label": "Jean Baptiste Montgolfier"
            "forename": "Jean Baptiste",
            "surname": "Montgolfier",
            "lang": "fr"
          }
        },
        "address": {
          "@class": "Address",
          "@id": 73647721,
          "@href": "https://histhub.ch/norm/api/Address/73647721",
          "@attributes": {
            "street": "Glenmere Close",
            "postcode": "CB1 8EF"
          }
        }
      }
    }
    ---------------------------------------------------------------

    This function will fail if the graph of connected bags to the
    given bag has cycles. This has to be removed before calling.

    Positional argument:

    - prefix     the URI prefix for the href "meta" field, that
                 will be made of prefix / entity_name / entity_id

    Keyword argument:

    - indent     the amount of spaces to indent the output JSON;
                 default is 2
    """
    indent = kwargs.get('indent', 2)
    eav_dict = bag_to_nested_dict(eav_bag, prefix)
    return dict_to_json(eav_dict, indent=indent)


def entity_ids_to_list(entity_ids_iter, prefix=None):
    """Turns a list of entity (name, id) pairs into a list of dictionaries
    ready for JSON dump.

    - entity_ids_iter   iterator over entity name, entity id
                        pairs, for  example:
    - prefix            the URI prefix to be used in href
                        attributes.

    INPUT:
        ('Person', 365)
        ('Person', 367)
        ...

    OUTPUT with prefix='hppt://localhost/api:
        [
            {
                '@class' : 'Person',
                '@id'    : 365,
                '@href'  : 'http://localhost/api/Person/365'
            },
            {
                '@class' : 'Person',
                '@id'    : 367,
                '@href'  : 'http://localhost/api/Person/367'
            },
            ...
        ]
    """
    if prefix:
        return [{
            "@class": entity_name,
            "@id": entity_id,
            "@href": href(prefix, entity_name, entity_id)
            } for (entity_name, entity_id) in entity_ids_iter]
    else:
        return [{
            "@class": entity_name,
            "@id": entity_id
            } for (entity_name, entity_id) in entity_ids_iter]


def eav_to_string(eav_bag, indent=3):
    """Verbose string representation of an EAV bag."""
    nodes_stack = list()
    nesting_level = -1
    list_offset = 0
    output_record = namedtuple('OutputRecord', ('indent', 'text'))
    output_lines = list()  # list of output records

    for node in acyclic_preorder_iterator(eav_bag):
        if nesting_level >= node.depth:
            # We jump up from a deeper nesting level. Some node need
            # some closing.
            for i in range(nesting_level - node.depth + 1):
                old_node = nodes_stack.pop()
                if not old_node.bag.empty():
                    output_lines.append(
                        output_record(nesting_level+list_offset, list()))
                    output_lines[-1].text.append('}')
                    nesting_level -= 1
                if old_node.index is not None \
                   and old_node.relation != node.relation:
                    output_lines.append(
                        output_record(nesting_level+list_offset, list()))
                    output_lines[-1].text.append(']')
                    list_offset -= 1

        # After eventually having closed previous nodes, we go
        # down into a deeper nesting level.
        nesting_level += 1
        nodes_stack.append(node)
        output_lines.append(output_record(nesting_level+list_offset, list()))

        if node.relation is None:
            # Only possible in the root node.
            output_lines[-1].text.append(node.bag.entity_name)
            output_lines[-1].text.append("({})".format(node.bag.eav_id))
            if not node.bag.empty():
                output_lines[-1].text.append('{')

        else:
            # We arrived to the current node through a relation, so
            # the relation name and an = sign must prepend the bag
            # representation, except for lists.
            if node.index is None:
                # Case of single valued relations.
                output_lines[-1].text.append(node.relation)
                output_lines[-1].text.append('=')
                output_lines[-1].text.append(node.bag.entity_name)
                output_lines[-1].text.append("({})".format(node.bag.eav_id))
                if not node.bag.empty():
                    output_lines[-1].text.append('{')

            else:
                # Case of multi-valued relations.
                if node.index == 0:
                    # The name of the relation is placed only preceding
                    # the first element of a set or related objects.
                    output_lines[-1].text.append(node.relation)
                    output_lines[-1].text.append('=')
                    output_lines[-1].text.append('[')
                    list_offset += 1
                    output_lines.append(output_record(nesting_level+list_offset, list()))

                output_lines[-1].text.append("[{}]".format(node.index))
                output_lines[-1].text.append('=')
                output_lines[-1].text.append(node.bag.entity_name)
                output_lines[-1].text.append("({})".format(node.bag.eav_id))
                if not node.bag.empty():
                    output_lines[-1].text.append('{')

        for name, value in node.bag.attributes.items():
            output_lines.append(
                output_record(1+nesting_level+list_offset, list()))
            output_lines[-1].text.append(name)
            output_lines[-1].text.append('=')
            output_lines[-1].text.append(value)

    # After consuming all nodes, only closing remains.
    for i in range(len(nodes_stack)):
        old_node = nodes_stack.pop()
        if not old_node.bag.empty():
            output_lines.append(
                output_record(nesting_level+list_offset, list()))
            output_lines[-1].text.append('}')
            nesting_level -= 1
        if old_node.index is not None:
            output_lines.append(
                output_record(nesting_level+list_offset, list()))
            output_lines[-1].text.append(']')
            list_offset -= 1

    joined_texts = ((R.indent, " ".join(R.text)) for R in output_lines)
    return "\n".join(T[0]*indent*" " + T[1] for T in joined_texts)

    # nodes_iterator = acyclic_preorder_iterator(eav_bag)
    #
    # # Root node.
    # root_node = next(nodes_iterator).target
    # if root_node.empty():
    #     output_lines.append( (nesting_level, empty_root_fmt.format(root_node.entity_name, root_node.eav_id)) )
    # else:
    #     output_lines.append( (nesting_level, full_root_fmt.format(root_node.entity_name, root_node.eav_id)) )
    #     nodes_stack.append('}')
    #     for attr_name, attr_value in root_node.attributes.items():
    #         output_lines.append( (nesting_level+1, attribute_fmt.format(attr_name, attr_value)) )

    # Rest of relation nodes.
    #
    # for item in nodes_iterator:
    #     pass

    # if nodes_stack:
    #     i = 0
    #     output_lines.append( (i, nodes_stack[i]) )

    # print(output_lines)
    # return '\n'.join("{}{}".format(L[0]*indent*' ', L[1]) for L in output_lines)


def eav_to_dot(eav_bag):
    def node_name(bag):
        return "{}_{}".format(bag.entity_name, bag.eav_id)

    entity_nodes = list()
    relation_edges = list()
    attribute_nodes = list()
    attribute_edges = list()

    entity_style      = '  node [color="#000000", fontcolor="#333333", fontname=Courier, fontsize=12 ];'
    attribute_style   = '  node [color="#AA3322", fontcolor="#000000", fontname=Courier, fontsize=12, shape="box", style="filled", fillcolor="#FFFF33" ];'
    relation_style    = '  edge [color="#000000", fontcolor="#000000", fontname=Courier, fontsize=12, weight=25];'

    grph_name_stencil = 'digraph {} {{'
    eaba_node_stencil = '  {} [ label="{}\\n{}" ];'
    attr_node_stencil = '  {}_{} [ label="{}" ];'
    attr_edge_stencil = '  {} -> {}_{} [ label="{}" ];'
    eavb_edge_stencil = '  {} -> {} [ label="{}" ];'

    for node in nodes_iterator(eav_bag):
        entity_nodes.append(eaba_node_stencil.format(
            node_name(node), node.entity_name, node.eav_id))

        for attr_name, attr_value in node.attributes.items():
            attribute_nodes.append(attr_node_stencil.format(
                node_name(node), attr_name, attr_value))
            attribute_edges.append(attr_edge_stencil.format(
                node_name(node), node_name(node), attr_name, attr_name))

    for edge in edges_iterator(eav_bag):
        relation_edges.append(eavb_edge_stencil.format(
            node_name(edge.start), node_name(edge.target), edge.relation))

    lines = list()
    lines.append(grph_name_stencil.format(node_name(eav_bag)))
    lines.append('  rankdir=LR;')
    lines.append('')
    lines.append(entity_style)
    lines.extend(entity_nodes)
    lines.append('')
    lines.append(attribute_style)
    lines.extend(attribute_nodes)
    lines.append('')
    lines.append(relation_style)
    lines.extend(relation_edges)
    lines.extend(attribute_edges)
    lines.append('}')
    return '\n'.join(lines)


def eav_to_acyclic_dot(eav_bag):
    def node_name(bag):
        return "{}_{}".format(bag.entity_name, bag.eav_id)

    entity_nodes = list()
    attribute_nodes = list()
    dummy_nodes = list()

    relation_edges = list()
    attribute_edges = list()

    entity_style = '  node [color="#000000", fontcolor="#333333", fontname=Courier, fontsize=12 ];'
    dummy_style = '  node [color="#000000", fontcolor="#000000", style="dashed", fontname=Courier, fontsize=12 ];'

    attribute_style = '  node [color="#AA3322", fontcolor="#000000", fontname=Courier, fontsize=12, shape="box", style="filled", fillcolor="#FFFF33" ];'
    relation_style = '  edge [color="#000000", fontcolor="#000000", fontname=Courier, fontsize=12, weight=25];'

    graph_name_fmt = 'digraph {} {{'
    eav_bag_node_fmt = '  {} [ label="{}\\n{}" ];'
    eav_bag_edge_fmt = '  {} -> {} [ label="{}" ];'
    attrib_node_fmt = '  {}_{} [ label="{}" ];'
    attrib_edge_fmt = '  {} -> {}_{} [ label="{}" ];'
    dummy_node_fmt = '  {}_{} [ label="{}\\n{}" ];'
    dummy_edge_fmt = '  {} -> {}_{} [ label="{}" ];'

    dummy_seq = 0

    for edge in acyclic_preorder_iterator(eav_bag):
        if edge.dummy:
            dummy_nodes.append(dummy_node_fmt.format(
                node_name(edge.bag), dummy_seq, edge.bag.entity_name,
                edge.bag.eav_id))
            relation_edges.append(dummy_edge_fmt.format(
                node_name(edge.prev), node_name(edge.bag), dummy_seq,
                edge.relation))
            dummy_seq += 1
        else:
            entity_nodes.append(eav_bag_node_fmt.format(
                node_name(edge.bag), edge.bag.entity_name, edge.bag.eav_id))
            for attr_name, attr_value in edge.bag.attributes.items():
                attribute_nodes.append(attrib_node_fmt.format(
                    node_name(edge.bag), attr_name, attr_value))
                attribute_edges.append(attrib_edge_fmt.format(
                    node_name(edge.bag), node_name(edge.bag), attr_name,
                    attr_name))
            if edge.prev:
                relation_edges.append(eav_bag_edge_fmt.format(
                    node_name(edge.prev), node_name(edge.bag), edge.relation))

    lines = list()
    lines.append(graph_name_fmt.format(node_name(eav_bag)))
    lines.append('  rankdir=LR;')
    lines.append('')
    lines.append(entity_style)
    lines.extend(entity_nodes)
    lines.append('')
    lines.append(dummy_style)
    lines.extend(dummy_nodes)
    lines.append('')
    lines.append(attribute_style)
    lines.extend(attribute_nodes)
    lines.append('')
    lines.append(relation_style)
    lines.extend(relation_edges)
    lines.extend(attribute_edges)
    lines.append('}')
    return '\n'.join(lines)
