# flake8: noqa: E501

from eavlib.core import EavBag
from eavlib.core import EavModel
from eavlib.core import ModelManager
from eavlib.core import ModelVicinity

# TODO: NamedTupleCursor introduces another dependency
#       This is easily solved by using tuple indices

from psycopg2.extras import NamedTupleCursor

# TODO: execute_values introduces a dependency to psycopg2
#       To solve this, an execute_values function should be
#       provided in the factory_function module. Or the
#       EavConnection should have a cursor method that is
#       customized by subclasses. There is no problem to
#       return a NamedTupleCursor always since it is com-
#       patible with the usual cursor (TO BE CHECKED)

from psycopg2.extras import execute_values

# TODO: ponder the use of .arraysize
#       https://www.python.org/dev/peps/pep-0249/#arraysize


class DataManager:
    """Reads or writes information from or into an EAV repository."""

    def __init__(self, graph, id_generator=None):
        """Creates a new DataManager.

        :param graph: the Graph to work on
        :type graph: eavlib.graph.Graph
        :param id_generator: used to generate database identifiers.
                             A None value is acceptable for read-only uses.
        """
        self._eav_access = graph.connection.legacy_connection()
        model_manager = ModelManager(graph.connection)
        self._eav_model = EavModel(model_manager.read(graph.model_id))
        self._id_generator = id_generator

    def eav_model(self):
        """Return the EAV model used by this data manager"""
        return self._eav_model

    def id_generator(self):
        """Return the id generator used by this data manager"""
        return self._id_generator

    def _wave_load_process(self, working_objects, vicinity_or_max_level):
        """Loads information about delivered objects in a wave propagation like
        manner, that is, for an object its relations are followed to reach
        related instances and then the relations of the related are again
        followed, and so on, collec- ting at each step the needed attributes.

        The process can be blind and exhaustive, in the sense it is followed
        until a depth level is reached, or guided by a model vicinity that
        expresses in each wave what relations are to be followed and what
        attributes to be read.

            vicinity_or_max_level   either a model vicinity object or an int
                                    expressing the max depth level.

            working_objects         a prebuilt dictionary with the object_id
                                    for the objects to be loaded as keys and
                                    already instantiated empty EavBag for the
                                    corresponding object class:

                                        { object_id -> empty EavBag }

        The working_objects dictionary is used to increasingly collect all eav
        bag instances as the loading proceeds from level 0 onwards. The reason
        it is done this way instead of only keeping the bags of the previous
        level, is that the same instance could appear at different depths
        because of cycles.  In keeping global knowledge, no repeated instances
        are created and paths after a repeated instance are followed only once.
        """
        if type(vicinity_or_max_level) == ModelVicinity:
            num_waves = len(vicinity_or_max_level.waves)
            vicinity = vicinity_or_max_level
        elif type(vicinity_or_max_level) == int:
            num_waves = vicinity_or_max_level + 1
            vicinity = None
        else:
            raise RuntimeError(
                'expecting a ModelVicinity instance or an integer')

        next_wave_idset = set(working_objects.keys())

        for level in range(num_waves):
            # Don't go on if there is no instance to load any more
            if len(next_wave_idset) == 0:
                break

            curr_wave_idset = set(next_wave_idset)
            next_wave_idset = set()

            # Fetch the attributes and their values.
            attr_to_read = [n.id for n in vicinity.waves[level].attributes] if vicinity else None
            attr_data = self.read_attributes_for(curr_wave_idset, attr_to_read)
            # for attr_id in attr_to_read: print("#  -------  {}  attribute to read {}".format(level,attr_id))
            # for attr_tuple in attr_data: print("#  -------  {}      actually read {}".format(level,attr_tuple))
            for object_id, attr_id, attr_value in attr_data:
                attr_name = self._eav_model.attribute(attr_id).name
                working_objects[object_id].set_attribute(attr_name, attr_value)

            # Fetch the related instances
            rels_to_read = [n.id for n in vicinity.waves[level].relations] if vicinity else None
            rel_data = self.read_relations_for(curr_wave_idset, rels_to_read)
            # for rel_id in rels_to_read: print("#  -------  {}   RELATION TO READ {}".format(level,rel_id))
            # for rel_tuple in rel_data:  print("#  -------  {}      ACTUALLY READ {}".format(level,rel_tuple))
            for object_id, rel_id, target_id, target_entity_id in rel_data:
                if target_id not in working_objects:
                    target_entity_name = self._eav_model.entity(target_entity_id).name
                    new_bag_to_load = EavBag(target_entity_name, target_id)
                    working_objects[target_id] = new_bag_to_load
                    next_wave_idset.add(target_id)

                rel_name = self._eav_model.relation(rel_id).name
                working_objects[object_id].add_related(rel_name, working_objects[target_id])

    def read_vicinity(self, model_vicinity, ids_list=None, **kwargs):
        """
            Wave retrieval of one or more EavBag instances whose information
            content is specified by a model vicinity.

            If a list of database ids is provided, only theses objects will
            be loaded (provided that they belong to the vicinity's entity).
            If the list is empty, all instances found in the database belon-
            bing to the vicinity's class are loaded.

            Passing a True value to the 'subclasses' keyword parameter makes
            class belonging extend to subclasses.

            Return a dictionary { object_id -> eav_bag } that might be empty
            if no objects where found:

                { id -> Entity( id ) }

        """
        include_subclasses = kwargs.get('subclasses') is True
        if ids_list:
            # Read (object_id, entity_id) pairs for the object ids in the list.
            obj_data = self.read_objects_for_oids(ids_list)
        else:
            # If ids_list is empty, read (object_id, entity_id) pairs for all
            # objects whose class is the the model vicinity central entity one.
            obj_data = self.read_objects_for_eids(
                (model_vicinity.namid.id,),
                subclasses=include_subclasses)

        # Create the eav bags to be returned.
        allowed_classes = set((model_vicinity.namid.id,))
        if include_subclasses:
            allowed_classes.update(e.id for e in self._eav_model.entity(model_vicinity.namid.id).all_children)

        dict_to_return = {}
        for object_id, entity_id in obj_data:
            if entity_id in allowed_classes:
                entity_name = self._eav_model.entity(entity_id).name
                dict_to_return[object_id] = EavBag(entity_name, object_id)

        working_objects = dict(dict_to_return)
        self._wave_load_process(working_objects, model_vicinity)
        return dict_to_return

    def read_to_level(self, ids_list, max_level=0):
        """
            Wave retrieval of one or more EavBag instances specified by their
            database identifiers.

            The loading is blind in the sense that as relations are discovered
            they are traversed and further information is loaded. For this rea-
            son, the given ids to load can be for objects belonging to different
            classes.

            Level 0 means load each object together with their attribute values
            and the ids of related objects, like for example:

                Person (1024) {
                    id = 'MQ78023921'
                    sex = male
                    name = 'John Carpenter'
                    spouse = Person(1041)
                }

            Level 1 goes one layer down and loads the level 0 information plus
            the attributes and related object references for objects related to
            the initial objects. For example:

                Person (1024) {
                    id = 'MQ78023921'
                    sex = male
                    name = 'John Carpenter'
                    spouse = Person(1041) {
                        id = 'LW64897414'
                        sex = female
                        name = 'Anna Carpenter'
                        spouse = Person(1024)
                    }
                }

            Return a dictionary { object_id -> eav_bag } that might be empty if
            no objects where found. In the previous examples:

                { 1024 -> Person(1024) }
        """

        if int(max_level) < 0:
            max_level = 0

        dict_to_return = {}
        if not ids_list:
            return dict_to_return

        # Create the eav bags to be returned.
        obj_data = self.read_objects_for_oids(ids_list)
        for row in obj_data:
            object_id, entity_id = row
            entity_name = self._eav_model.entity(entity_id).name
            dict_to_return[object_id] = EavBag(entity_name, object_id)

        working_objects = dict(dict_to_return)
        self._wave_load_process(working_objects, max_level)
        return dict_to_return

    def read_ids_for_entities(self, class_id_list):
        """Returns a list of pairs (entity name, instance id) for objects whose
        classes are in the given list of classes (specified by class id)

            - class_id_list     a list of class ids
        """
        input_tuple = tuple(class_id_list)
        if not input_tuple:
            return []
        else:
            cursor = self._eav_access.connection.cursor()
            cursor.execute(
                self._eav_access.query_dict.object_read_ids_4_entities_query,
                (input_tuple, ))
            return cursor.fetchall()

    def read_ids_for_value(self, attr_id, attr_value):
        """
            Returns a list of eav ids for objects having the given value
            for the given attribute.

            - attr_id       a valid attribute id
            - attr_value    the attribute value
        """
        cursor = self._eav_access.connection.cursor()
        cursor.execute(
            self._eav_access.query_dict.object_read_ids_4_values_query,
            (attr_id, attr_value))
        return [t[0] for t in cursor]

    def read_ids_for_entity(self, entity_name_or_id):
        """
            Returns a list of pairs (entity name, instance id) for objects
            of the given class.

            - entity_name_or_id     either a name like 'Person" or a data-
                                    base numeric class id
        """
        try:
            entity_id = int(entity_name_or_id)
        except ValueError:
            entity_id = self._eav_model.entity(entity_name_or_id).id

        return self.read_ids_for_entities((entity_id,))

    def read_related_ids(self, entity_id, relation_id):
        """
            Returns the list of class names and database indentfiers for
            a given object id and relations id.

            - entity_id     the database ientifier of the source id
            - relation_id   identifier of the relation
        """
        cursor = self._eav_access.connection.cursor()
        cursor.execute(
            self._eav_access.query_dict.object_read_related_ids_query,
            (entity_id, relation_id))
        return cursor.fetchall()

    def execute_batch(self, batch_data, batch_size=100):
        '''"Low" level writing to an EAV repository.'''
        cursor = self._eav_access.connection.cursor()

        # particular attributes [ (OID, AID), ... ]
        execute_values(
            cursor,
            self._eav_access.query_dict.object_delete_attribute,
            [t for t in batch_data.del_atts if len(t) == 2],
            template='(%s, %s)',
            page_size=batch_size)

        # objects all of whose attributes are to be deleted [ (OID,), ... ]
        execute_values(
            cursor,
            self._eav_access.query_dict.object_delete_all_attributes,
            [t for t in batch_data.del_atts if len(t) == 1],
            template='(%s)',
            page_size=batch_size)

        # delete a particular relation instantiation [ (OID, RID, TID), ... ]
        execute_values(
            cursor,
            self._eav_access.query_dict.object_delete_relation_instance,
            [t for t in batch_data.del_rels if len(t) == 3],
            template='(%s, %s, %s)',
            page_size=batch_size)

        # objects all of whose relations (of one kind) are to be deleted [ (OID, RID), ... ]
        execute_values(
            cursor,
            self._eav_access.query_dict.object_delete_relation,
            [t for t in batch_data.del_rels if len(t) == 2],
            template='(%s, %s)',
            page_size=batch_size)

        # objects all of whose relations (of any kind) are to be deleted [ (OID,), ... ]
        execute_values(
            cursor,
            self._eav_access.query_dict.object_delete_all_relations,
            [t for t in batch_data.del_rels if len(t) == 1],
            template='(%s)',
            page_size=batch_size)

        # objects all of whose INCOMING relations (of one kind) are to be deleted [ (TID, RID), ... ]
        execute_values(
            cursor,
            self._eav_access.query_dict.object_delete_back_relation,
            [t for t in batch_data.del_bcks if len(t) == 2],
            template='(%s, %s)',
            page_size=batch_size)

        # objects all of whose INCOMING relations (of any kind) are to be deleted [ (TID,), ... ]
        execute_values(
            cursor,
            self._eav_access.query_dict.object_delete_all_back_relations,
            [t for t in batch_data.del_bcks if len(t) == 1],
            template='(%s)',
            page_size=batch_size)

        # delete objects [ (OID,), ... ]
        execute_values(
            cursor,
            self._eav_access.query_dict.object_delete,
            batch_data.del_objs,
            template='(%s)',
            page_size=batch_size)

        # insert new objects [ (OID, EID), ... ]
        execute_values(
            cursor,
            self._eav_access.query_dict.object_write_instance_query,
            batch_data.ins_objs,
            template='(%s, %s)',
            page_size=batch_size)

        # insert attributes  [ (OID, AID, VAL), ... ]
        execute_values(
            cursor,
            self._eav_access.query_dict.object_write_attribute_query,
            batch_data.ins_atts,
            template='(%s, %s, %s)',
            page_size=batch_size)

        # insert relations  [ (OID, RID, TID), ... ]
        execute_values(
            cursor,
            self._eav_access.query_dict.object_write_relation_query,
            batch_data.ins_rels,
            template='(%s, %s, %s)',
            page_size=batch_size)

        cursor.close()

    def commit(self):
        """
            Commit the current transaction, implicitly open since the
            latest commit or rollback.
        """
        self._eav_access.connection.commit()

    def rollback(self):
        """
            Rollback the current transaction, implicitly open since the
            latest commit or rollback.
        """
        self._eav_access.connection.rollback()

    def read_objects_for_oids(self, ids_to_read):
        """
            Read object tuples for the passed ids:
            (object_id, entity_id)
        """
        ids_tuple = tuple(ids_to_read)
        if not ids_tuple:
            return []

        with self._eav_access.connection.cursor(
                cursor_factory=NamedTupleCursor) as cursor:
            cursor.execute(
                self._eav_access.query_dict.object_read_instance_query,
                (ids_tuple,))
            return cursor.fetchall()

    def read_objects_for_eids(self, entity_ids_to_read, **kwargs):
        """
            Read tuples like:

                (object_id, entity_id)

            with all object whose entity is in the given list of entity ids.

            Passing a True value to the 'subclasses' keyword parameter makes
            class belonging extend to subclasses.
        """
        if kwargs.get('subclasses') is True:
            classes_to_load = tuple(set(
                entity.id
                for eid in entity_ids_to_read
                for entity in self._eav_model.entity(eid).all_children))
        else:
            classes_to_load = tuple(entity_ids_to_read)

        with self._eav_access.connection.cursor(
                cursor_factory=NamedTupleCursor) as cursor:
            cursor.execute(
                self._eav_access.query_dict.object_read_oids_4_entity_query,
                (classes_to_load,))
            return cursor.fetchall()

    def read_attributes_for(self, ids_to_read, atts_to_read=None):
        """
            Read attribute tuples for the passed object ids and the
            attributes ids. If atts_to_read is None, all available
            attributes are returned, otherwise only those explicitly
            mentioned. Returns back a list of tuples like:

                (object_id, attribute_id, attribute_value)
        """
        ids_tuple = tuple(ids_to_read)
        if atts_to_read is not None and len(atts_to_read) == 0:
            return list()

        with self._eav_access.connection.cursor(
                cursor_factory=NamedTupleCursor) as cursor:
            if atts_to_read is None:
                cursor.execute(
                    self._eav_access.query_dict.object_read_all_attributes_query,
                    (ids_tuple,))
            else:
                cursor.execute(
                    self._eav_access.query_dict.object_read_some_attributes_query,
                    (ids_tuple, tuple(atts_to_read)))

            return cursor.fetchall()

    def read_relations_for(self, ids_to_read, rels_to_read=None):
        """
            Read relation tuples for the passed ids objects and the
            relation ids. If rels_to_read is None, all available re-
            lations are returned, otherwise only those explicitly
            mentioned. Returns back a list of tuples like:

            (object_id, relation_id, target_id, target_entity_id)

        """
        ids_tuple = tuple(ids_to_read)
        if rels_to_read is not None and len(rels_to_read) == 0:
            return list()

        with self._eav_access.connection.cursor(
                cursor_factory=NamedTupleCursor) as cursor:
            if rels_to_read:
                cursor.execute(
                    self._eav_access.query_dict.object_read_some_relations_query,
                    (ids_tuple, tuple(rels_to_read))
                )
            else:
                cursor.execute(
                    self._eav_access.query_dict.object_read_all_relations_query,
                    (ids_tuple, )
                )
            return cursor.fetchall()
