# flake8: noqa: E501

class ChangeBatch:
    '''
        Set of low level information to be written or deleted from an
        EAV store.

        Instances of this class hold lists of objects to be deleted,
        particular attributes or relations to be deleted, or objects,
        attributes and relations to be written or updated. Objects,
        attributes and relations are specified by theis database ids.
        Client code must do translations from entity names to db ids
        before calling.

        Multiple changes (insertions, updates, or deletions) can be
        stored in a change batch. To execute them, simply invoke the
        execute_batch method on a the data manager instance.
    '''

    @staticmethod
    def _check_valid_id(id_value, method, param) -> None:
        if id_value is None:
            msg = "empty id passed to '{}' as '{}'"
            raise RuntimeError(msg.format(method, param))

    def __init__(self) -> None:
        self.ins_objs = list()  # ( OID, EID )
        self.ins_atts = list()  # ( OID, AID, VAL )
        self.ins_rels = list()  # ( OID, RID, TID )

        self.del_objs = list()  # ( OID, )
        self.del_atts = list()  # ( OID, ) or ( OID, AID )
        self.del_rels = list()  # ( OID, ) or ( OID, RID ) or ( OID, RID, TID )  forward relations
        self.del_bcks = list()  # ( TID, ) or ( TID, RID )                      backward relations

    def is_empty(self) -> bool:
        return not any((
            self.ins_objs,
            self.ins_atts,
            self.ins_rels,
            self.del_objs,
            self.del_atts,
            self.del_rels,
            self.del_bcks,
            ))

    def write_object(self, object_id, entity_id) -> None:
        '''
            A new object will be created with the given entity id.
        '''
        self._check_valid_id(object_id, 'write_object', 'object_id')
        self._check_valid_id(object_id, 'write_object', 'entity_id')
        self.ins_objs.append((object_id, entity_id))

    def write_attribute(self, object_id, attribute_id, attribute_value) -> None:
        '''
            Write an attribute value, deleting the previous one if present.
        '''
        self._check_valid_id(object_id, 'write_attribute', 'object_id')
        self._check_valid_id(object_id, 'write_attribute', 'attribute_id')
        self.del_atts.append((object_id, attribute_id))
        if attribute_value is not None:
            self.ins_atts.append((object_id, attribute_id, attribute_value))

    def write_relation(self, object_id, relation_id, target_id) -> None:
        '''
            Add a relation between two instances.
        '''
        self._check_valid_id(object_id, 'write_relation', 'object_id')
        self._check_valid_id(object_id, 'write_relation', 'relation_id')
        self.del_rels.append((object_id, relation_id, target_id))
        self.ins_rels.append((object_id, relation_id, target_id))

    def update_relation(self, object_id, relation_id, old_target_id,
                        new_target_id):
        '''
            Modifies a relation of a source object from an old instance
            to a new one.
        '''
        self._check_valid_id(object_id, 'update_relation', 'object_id')
        self._check_valid_id(object_id, 'update_relation', 'relation_id')
        self._check_valid_id(object_id, 'update_relation', 'old_target_id')
        self._check_valid_id(object_id, 'update_relation', 'new_target_id')
        self.del_rels.append((object_id, relation_id, old_target_id))
        self.ins_rels.append((object_id, relation_id, new_target_id))

    def delete_object(self, object_id):
        '''
            A particular object will be deleted.
        '''
        self._check_valid_id(object_id, 'delete_object', 'object_id')
        self.del_objs.append((object_id,))

    def delete_all_object_data(self, object_id):
        '''
            A particular object, together with all its attributes
            and relations (outgoing and incoming) will be deleted.
        '''
        self._check_valid_id(object_id, 'delete_all_object_data', 'object_id')
        self.del_objs.append((object_id,))
        self.del_atts.append((object_id,))
        self.del_rels.append((object_id,))
        self.del_bcks.append((object_id,))

    def delete_all_attributes(self, object_id):
        '''
            All attributes of the passed object will be deleted if
            any exists, otherwise nothing will happen.
        '''
        self._check_valid_id(object_id, 'delete_all_attributes', 'object_id')
        self.del_atts.append((object_id,))

    def delete_attribute(self, object_id, attribute_id):
        '''
            A particular attribute of an abject will be deleted if
            it exists, otherwise nothing will happen.
        '''
        self._check_valid_id(object_id,    'delete_attribute', 'object_id')
        self._check_valid_id(attribute_id, 'delete_attribute', 'attribute_id')
        self.del_atts.append((object_id, attribute_id))

    def delete_all_relations(self, object_id):
        '''
            All (outgoing) relations of the passed object will be
            deleted if any exists, otherwise nothing will happen.
        '''
        self._check_valid_id(object_id, 'delete_all_relations', 'object_id')
        self.del_rels.append((object_id,))

    def delete_relation(self, object_id, relation_id):
        '''
            A relation will be deleted what potentially can drop one
            or more links between objects. If the given relation doesn't
            exists, nothing will happen.
        '''
        self._check_valid_id(object_id,   'delete_relation', 'object_id')
        self._check_valid_id(relation_id, 'delete_relation', 'attribute_id')
        self.del_rels.append((object_id, relation_id))

    def delete_relation_instance(self, object_id, relation_id, target_id):
        '''
            A particular relation between 2 objects will be deleted. If
            no such relation exists, nothing will happen.
        '''
        self._check_valid_id(
            object_id, 'delete_relation_between', 'object_id')
        self._check_valid_id(
            relation_id, 'delete_relation_between', 'relation_id')
        self._check_valid_id(
            target_id, 'delete_relation_between', 'target_id')
        self.del_rels.append((object_id, relation_id, target_id))

    def delete_all_backwards_relations(self, target_id):
        '''
            All (incoming) relations of the passed object will be
            deleted if any exists, otherwise nothing will happen.
        '''
        self._check_valid_id(
            target_id, 'delete_all_backwards_relations', 'target_id')
        self.del_bcks.append((target_id,))

    def delete_backwards_relations(self, target_id, relation_id):
        '''
            All (incoming) relations of a given kind of the passed
            object will be deleted if any exists, otherwise nothing
            will happen.
            To delete a particular backward relation, the forward
            method must be used.
        '''
        self._check_valid_id(
            target_id, 'delete_all_backwards_relations', 'target_id')
        self.del_bcks.append((target_id, relation_id))

    def __str__(self):
        items = list()

        items.extend(["DELETE Obj {}"          .format(t[0])           for t in self.del_objs])
        items.extend(["DELETE Att {}, *"       .format(t[0])           for t in self.del_atts if len(t)==1])
        items.extend(["DELETE Att {}, {}"      .format(t[0], t[1])     for t in self.del_atts if len(t)==2])
        items.extend(["DELETE Rel {}, *"       .format(t[0])           for t in self.del_rels if len(t)==1])
        items.extend(["DELETE Rel {}, {}, *"   .format(t[0], t[1])     for t in self.del_rels if len(t)==2])
        items.extend(["DELETE Rel {}, {}, {}"  .format(t[0], t[1], t[2]) for t in self.del_rels if len(t)==3])
        items.extend(["DELETE Bck *, {}"       .format(t[0])           for t in self.del_bcks if len(t)==1])
        items.extend(["DELETE Bck *, {}, {}"   .format(t[0], t[1])     for t in self.del_bcks if len(t)==2])

        items.extend(["INSERT Obj {}, {}"      .format(t[0], t[1])       for t in self.ins_objs])
        items.extend(["INSERT Att {}, {}, '{}'".format(t[0], t[1], t[2]) for t in self.ins_atts if t[2] is not None])
        items.extend(["INSERT Att {}, {}, null".format(t[0], t[1], t[2]) for t in self.ins_atts if t[2] is None])
        items.extend(["INSERT Rel {}, {}, {}"  .format(t[0], t[1], t[2]) for t in self.ins_rels])

        return '\n'.join(items)

    def translated_dump(self, eav_model):
        '''
            Produce a textual dump of a change batch where attribute and
            relation ids are translated into their human readable names.
        '''
        extended_batch = ChangeBatch()
        for t in self.ins_objs:
            extended_batch.ins_objs.append((t[0], "{}:{}".format(eav_model.entity(t[1]).name, t[1])))
        for t in self.ins_atts:
            extended_batch.ins_atts.append((t[0], "{}:{}".format(eav_model.attribute(t[1]).name, t[1]), t[2]))
        for t in self.ins_rels:
            extended_batch.ins_rels.append((t[0], "{}:{}".format(eav_model.relation(t[1]).name, t[1]), t[2]))

        extended_batch.del_objs.extend(self.del_objs)
        for t in self.del_atts:
            if len(t) == 1:
                extended_batch.del_atts.append(t)
            else:
                extended_batch.del_atts.append((t[0], "{}:{}".format(eav_model.attribute(t[1]).name, t[1])))
        for t in self.del_rels:
            if len(t) == 1:
                extended_batch.del_rels.append(t)
            elif len(t) == 2:
                extended_batch.del_rels.append((t[0], "{}:{}".format(eav_model.relation(t[1]).name, t[1])))
            else:
                extended_batch.del_rels.append((t[0], "{}:{}".format(eav_model.relation(t[1]).name, t[1], t[2])))
        for t in self.del_bcks:
            if len(t) == 1:
                extended_batch.del_bcks.append(t)
            else:
                extended_batch.del_bcks.append((t[0], "{}:{}".format(eav_model.attribute(t[1]).name, t[1])))

        return str(extended_batch)
