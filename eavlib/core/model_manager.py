from psycopg2.extras import NamedTupleCursor

from eavlib.core.id_manager import SequenceIDManager
from eavlib.core.model_description import ModelDescription

try:
    from math import inf
except ImportError:
    inf = float("inf")


class CardMax:
    @staticmethod
    def model_2_db(card_max):
        if card_max < 1:
            raise ValueError(
                "maximum cardinality is not allowed to be less than 1.")
        return 0 if card_max == inf else card_max

    @staticmethod
    def db_2_model(card_max):
        return inf if card_max == 0 else card_max


class ModelManager:
    """Manages data models in an EAV repository."""

    def __init__(self, eav_conn):
        """Create a new ModelManager.

        :param eav_conn: The EAV connection to use.
        :type eav_conn: eavlib.connection.EAVConnection
        """
        self.eav_conn = eav_conn
        self.model_id_manager = SequenceIDManager(eav_conn, "model_id_seq")
        self.id_manager = SequenceIDManager(eav_conn, "model_seq")

    def id_by_name(self, model_name):
        '''
            Locate the model id of an EAV model that is specified
            by model name.
        '''
        cursor = self.eav_conn.cursor()
        cursor.execute(self.eav_conn.query_dict.model_read_id_by_name_query, (
            model_name,))
        result_list = cursor.fetchall()
        self.eav_conn.rollback()
        cursor = None

        if not result_list:
            raise RuntimeError('unknown model name "{}"'.format(model_name))
        return result_list[0][0]

    def id_from_text(self, name_or_id):
        '''
           Finds the database model id from a string that can contain
           either an integer or a model name.
        '''
        if isinstance(name_or_id, int) or (
                isinstance(name_or_id, str)
                and name_or_id.isdigit()
                and int(name_or_id) > 0):

            # An identifier was given; check that it really exists.
            model_id = int(name_or_id)
            with self.eav_conn.connection.cursor() as cursor:
                cursor.execute(
                    self.eav_conn.query_dict.model_read_model_query, (
                        model_id,))
                result_list = cursor.fetchall()
            self.eav_conn.connection.rollback()
            if not result_list:
                raise RuntimeError("unknown model id {}".format(model_id))
            return result_list[0][0]
        elif isinstance(name_or_id, str):
            # A model name was given; find the model by its name and
            # return its database identifier.
            return self.id_by_name(name_or_id)

        raise TypeError("Invalid name_or_id given")

    def read(self, model_id):
        '''
            Reads all information about an EAV data model identified
            by its id returning a ModelDescription (namedtuple).
        '''
        errors = list()
        cursor = self.eav_conn.cursor(cursor_factory=NamedTupleCursor)

        cursor.execute(self.eav_conn.query_dict.model_read_model_query, (
            model_id,))
        result = cursor.fetchall()
        if not result:
            errors.append("unknown model id {}".format(model_id))

        else:
            # to locate Entity instances by their id
            entities_dict = dict()

            model_tuple = result[0]
            model_description = ModelDescription.Model(
                model_tuple.model_id,
                model_tuple.model_name,
                model_tuple.model_description,
                list())  # of entitites

            # entities
            cursor.execute(
                self.eav_conn.query_dict.model_read_entities_query, (
                    model_id,))
            for ent_tuple in cursor.fetchall():
                entity = ModelDescription.Entity(
                    ent_tuple.entity_id,
                    ent_tuple.entity_name,
                    ent_tuple.entity_description,
                    # parents, attributes and relations to be filled later.
                    list(), list(), list())
                entities_dict[entity.id] = entity
                model_description.entities.append(entity)

            # parents
            cursor.execute(self.eav_conn.query_dict.model_read_parents_query, (
                model_id,))
            for par_tuple in cursor.fetchall():
                base_entity = entities_dict[par_tuple.entity_id]
                parent_entity = entities_dict[par_tuple.parent_id]
                base_entity.parents.append(parent_entity.name)

            # attributes
            cursor.execute(
                self.eav_conn.query_dict.model_read_attributes_query, (
                    model_id,))
            for attr_tuple in cursor.fetchall():
                attribute = ModelDescription.Attribute(
                    attr_tuple.attribute_id,
                    attr_tuple.attribute_name,
                    attr_tuple.attribute_description,
                    attr_tuple.attribute_is_required)
                entities_dict[attr_tuple.entity_id].attributes.append(
                    attribute)

            # relations
            cursor.execute(
                self.eav_conn.query_dict.model_read_relations_query, (
                    model_id,))
            for rel_tuple in cursor.fetchall():
                relation = ModelDescription.Relation(
                    rel_tuple.relation_id,
                    rel_tuple.relation_name,
                    rel_tuple.relation_description,
                    entities_dict[rel_tuple.relation_target_id].name,
                    rel_tuple.relation_card_min,
                    CardMax.db_2_model(rel_tuple.relation_card_max))
                entities_dict[rel_tuple.entity_id].relations.append(relation)

        self.eav_conn.rollback()
        cursor = None
        if errors:
            raise RuntimeError('\n'.join(errors))
        return model_description

    def write(self, model_description):
        '''
            Writes a model description into an EAV store and returns
            its database id.

            Proper database primary keys for the model objects are
            generated inside this method. Any values present in id
            fields are ignored.

            Returns the generated database id for the given model.

            Throws a runtime expection if:
             - no id generator was provided at construction time,
             - the model name already exists
        '''
        cursor = self.eav_conn.cursor()

        model_id = self.model_id_manager.next_id()
        cursor.execute(  # -> Exception if name already exists
            self.eav_conn.query_dict.model_write_model_query,
            (model_id, model_description.name, model_description.description))

        entity_ids = dict()
        for entity in model_description.entities:
            entity_id = self.id_manager.next_id()
            cursor.execute(
                self.eav_conn.query_dict.model_write_entity_query,
                (entity_id, entity.name, entity.description, model_id))
            entity_ids[entity.name] = entity_id

        for entity in model_description.entities:
            entity_id = entity_ids[entity.name]

            for attr in entity.attributes:
                attr_id = self.id_manager.next_id()
                cursor.execute(
                    self.eav_conn.query_dict.model_write_attribute_query, (
                        attr_id,
                        entity_id,
                        attr.name,
                        attr.description,
                        attr.required))

            for rel in entity.relations:
                rel_id = self.id_manager.next_id()
                target_id = entity_ids[rel.target]
                cursor.execute(
                    self.eav_conn.query_dict.model_write_relation_query, (
                        rel_id,
                        entity_id,
                        target_id,
                        rel.name,
                        rel.description,
                        rel.card_min,
                        CardMax.model_2_db(rel.card_max)))

            for parent in entity.parents:
                cursor.execute(
                    self.eav_conn.query_dict.model_write_parent_query,
                    (entity_id, entity_ids[parent]))

        self.eav_conn.commit()
        return model_id

    def delete(self, model_id):
        """Deletes a model from an EAV store."""
        cursor = self.eav_conn.cursor()
        queries = self.eav_conn.query_dict
        for query in (
                queries.model_delete_relations_query,
                queries.model_delete_attributes_query,
                queries.model_delete_parents_query,
                queries.model_delete_entities_query,
                queries.model_delete_model_query):
            cursor.execute(query, (model_id,))
        self.eav_conn.commit()
