# flake8: noqa: C901

from collections import defaultdict
from itertools import tee
from typing import TYPE_CHECKING


if TYPE_CHECKING:
    from typing import (Dict, Optional, Set, Tuple, Union)
    from eavlib.core.entity_span import EntitySpan
    from eavlib.core.model_description import ModelDescription

    Identifier = Union[str, int]

from eavlib.core import check_model_description
from eavlib.core import build_entity_span

#    CHEAT SHEET
#
#    +----------------------+   +----------------------+   +---------------+
#    | EavModel             |   | Entity               |   | Namid         |
#    |===================== |   |===================== |   | ============= |
#    | id                   |   | id                   |   | name          |
#    | name                 |   | name                 |   | id            |
#    | namid                |   | namid                |   +---------------+
#    | description          |   | description          |
#    | model_description    |   |                      |   +---------------+
#    |                      |   | _direct_parents      |   | Attribute     |
#    | _global_entity_map   |   | all_parents          |   | ============= |
#    | _global_attr_map     |   | all_children         |   | id            |
#    | _global_rel_map      |   |                      |   | name          |
#    |                      |   | own_attributes       |   | namid         |
#    | entities( )          |   | all_attributes       |   | description   |
#    | entity( i|n )        |   | _attributes_map      |   | this_entity   |
#    | has_entity( i|n )    |   |                      |   +---------------+
#    | attribute( i )       |   | own_relations        |
#    | relation( i )        |   | all_relations        |   +---------------+
#    | entity_span( t )     |   | _relations_map       |   | Relation      |
#    |                      |   | incoming_rels        |   | ============= |
#    | ename( i ) DPC       |   |                      |   | id            |
#    | rname( i ) DPC       |   | relation( i|n )      |   | name          |
#    | aname( i ) DPC       |   | attribute( i|n )     |   | namid         |
#    |                      |   | subclass_of( e )     |   | description   |
#    |                      |   |                      |   | source_entity |
#    |                      |   |                      |   | target_entity |
#    +----------------------+   +----------------------+   +---------------+
#


class EAVModelError(Exception):
    pass


class NotDeclaredError(KeyError):
    pass


class EavModel:
    """Class containing information about an EAV model ready to be queried.

    It expands the simple ModelDescription tuples that are only the minimal
    set of information about a model needed for persistency.
    """

    class Namid:
        """Utility class to tie together the name of a model element (attribute,
        relation or entity) with its database ID.

        Data structures for raw database interaction operate solely with
        numerical IDs. Having the names tied together allows for easy human
        friendly output, debugging, representations, etc.
        """
        def __init__(self, name: str, id: "Optional[int]" = None) -> None:
            self.name = name
            self.id = id

        def __repr__(self) -> str:
            return "%s(%u)" % (self.name, self.id)

    class Entity:
        def __init__(self, id: int, name: str, description: str) -> None:
            self.id = id
            self.name = name
            self.namid = EavModel.Namid(name, id)
            self.description = description

            # Lists of entity description objects somehow related to this
            # entity:
            #
            #   _direct_parents    the one or more entity objects that are
            #                      (declared) direct parents of this entity.
            #
            #   all_parents        all transitive parents of this entity,
            #                      NOT including this entity itself.
            #
            #   all_children       all transitive child entities of this
            #                      entity, INCLUDING this entity itself.
            #

            self._direct_parents = set()  # type: Set[EavModel.Entity]
            self.all_parents = set()  # type: Set[EavModel.Entity]
            self.all_children = set()  # type: Set[EavModel.Entity]

            self.all_children.add(self)

            # Lists and data structures to access attributes of this entity:
            # (map keys are either ids or names).
            #
            #   own_attributes       all attributes explicitly declared for
            #                        this entity.
            #
            #   all_attributes       all attributes declared for this entity
            #                        and those (transitively) inherited.
            #
            #   _attributes_map      all attributes declared for this entity
            #                        and those (transitively) inherited, in
            #                        a map keyed by name (and id when avai-
            #                        lable).

            self.own_attributes = set()  # type: Set[EavModel.Attribute]
            self.all_attributes = set()  # type: Set[EavModel.Attribute]
            self._attributes_map = {}  # type: Dict[str, EavModel.Attribute]

            # List and data structures to access relations of this entity:
            # (map keys are either ids or names).
            #
            #   own_relations        all relations explicitly declared for
            #                        this entity.
            #
            #   all_relations        all relations declared for this entity
            #                        and those (transitively) inherited.
            #
            #   _relations_map       all relations declared for this entity
            #                        and those (transitively) inherited, in
            #                        a map keyed by name (and id when avai-
            #                        lable).
            #
            #   incoming_rels        relations having this entity as target
            #                        as a set.

            self._relations_map = {}  # type: Dict[str, EavModel.Relation]
            self.own_relations = set()  # type: Set[EavModel.Relation]
            self.all_relations = set()  # type: Set[EavModel.Relation]
            self.incoming_rels = None  # type: Optional[Set[EavModel.Relation]]

        def attribute(self, id_or_name: "Identifier") -> "EavModel.Attribute":
            try:
                return self._attributes_map[id_or_name]
            except KeyError:
                if isinstance(id_or_name, int):
                    raise NotDeclaredError(
                        "Unknown attribute with ID: %u" % id_or_name)
                else:
                    raise NotDeclaredError(
                        "Unknown attribute: %s" % id_or_name)

        def relation(self, id_or_name: "Identifier") -> "EavModel.Relation":
            try:
                return self._relations_map[id_or_name]
            except KeyError:
                if isinstance(id_or_name, int):
                    raise NotDeclaredError(
                        "Unknown relation with ID %u" % id_or_name)
                else:
                    raise NotDeclaredError(
                        "Unknown relation: %s" % id_or_name)

        def subclass_of(self, other_entity: "EavModel.Entity") -> bool:
            return self in other_entity.all_children

    class Attribute:
        def __init__(self, id: int, name: str, description: str,
                     this_entity: "EavModel.Entity") -> None:
            self.id = id
            self.name = name
            self.namid = EavModel.Namid(name, id)
            self.description = description
            self.this_entity = this_entity

        def __eq__(self, other) -> bool:
            if isinstance(other, self.__class__):
                # FIXME: Exclude id
                return vars(self) == vars(other)
            else:
                return False

        def __hash__(self) -> int:
            return id(self)

    class Relation:
        def __init__(self, id: int, name: str, description: str,
                     source_entity: "EavModel.Entity",
                     target_entity: "EavModel.Entity") -> None:
            self.id = id
            self.name = name
            self.namid = EavModel.Namid(name, id)
            self.description = description
            self.source_entity = source_entity
            self.target_entity = target_entity

        def __eq__(self, other) -> bool:
            if isinstance(other, self.__class__):
                # FIXME: Exclude id
                return vars(self) == vars(other)
            else:
                return False

        def __hash__(self) -> int:
            return id(self)

    def __init__(self, model_description: "ModelDescription") -> None:
        self.id = model_description.id
        self.name = model_description.name
        self.namid = EavModel.Namid(
            model_description.name, model_description.id)
        self.description = model_description.description

        self.model_description = model_description

        # Model global mapping to give access to any model entity by name and,
        # when they are available, by id.
        self._global_entity_map = {}

        # Model global reverse mappings to give access to attributes and rela-
        # tions by ids, when they are available (needed when translating raw
        # db information into eav bag instances).
        self._global_attr_map = {}
        self._global_rel_map = {}

        # Flesh out model information
        self._build_model_information(model_description)

    def entities(self) -> "Tuple[EavModel.Entity, ...]":
        return tuple(set(self._global_entity_map.values()))

    def entity(self, id_or_name: "Identifier") -> "EavModel.Entity":
        try:
            return self._global_entity_map[id_or_name]
        except KeyError:
            if isinstance(id_or_name, int):
                raise NotDeclaredError(
                    "Unknown entity with ID {}" % id_or_name)
            else:
                raise NotDeclaredError(
                    "Unknown entity: %s" % id_or_name)

    def has_entity(self, id_or_name: "Identifier") -> bool:
        try:
            self.entity(id_or_name)
            return True
        except NotDeclaredError:
            return False

    def attribute(self, id: int) -> "EavModel.Attribute":
        """Returns an attribute for a given id. There is no lookup by name,
        since an attribute name is not globally unique inside a model.

        Needed when building EavBags, for translating database IDs into
        attribute names.
        """
        try:
            return self._global_attr_map[id]
        except KeyError:
            raise NotDeclaredError("Unknown attribute for ID %u" % (id))

    def relation(self, id: int) -> "EavModel.Relation":
        """Returns an relation for a given id. There is no lookup by name,
        since a relation name is not globally unique insidep a model.

        Needed when building EavBags, for translating database IDs into
        relation names.
        """
        try:
            return self._global_rel_map[id]
        except KeyError:
            raise NotDeclaredError(
                "Unknown relation with ID %u" % (id))

    def inherits_from(self, child: "Identifier", parent: "Identifier") -> bool:
        """Whether there is an inheritance relation between the two given
        entity names.
        """
        return self.entity(parent) in self.entity(child).all_parents

    def ename(self, id):
        """Returns an entity name for a given ID.

        *DEPRECATED*, should use .entity(id).name
        """
        return self.entity(id).name

    def aname(self, id):
        """Returns an attribute name for a given ID.

        *DEPRECATED*, should use .attribute(id).name
        """
        return self.attribute(id).name

    def rname(self, id):
        """Returns a relation name for a given ID.

        *DEPRECATED*, should use .relation(id).name
        """
        return self.relation(id).name

    def entity_span(self, entity_span_text: str) -> "EntitySpan":
        """Returns an entity span object representing entity span description
        given in the entity_span_text.
        """
        return build_entity_span(self, entity_span_text)

    def __eq__(self, other) -> bool:
        if isinstance(other, EavModel):
            # Model comparison
            if self.name != other.name:
                return False

            # Entity comparison
            entities_list = self._global_entity_map.values()
            if len(entities_list) != len(other._global_entity_map.values()):
                return False

            for this_entity in entities_list:
                if this_entity.name not in other._global_entity_map:
                    return False
                mirror_entity = other._global_entity_map[this_entity.name]
                if this_entity.name != mirror_entity.name:
                    return False

                # Direct parents check
                parents_list = this_entity._direct_parents
                if len(parents_list) != len(mirror_entity._direct_parents):
                    return False
                for this_parent in parents_list:
                    if this_parent.name not in other._global_entity_map:
                        return False
                    mirror_parent = other._global_entity_map[this_parent.name]
                    if this_parent.name != mirror_parent.name:
                        return False

                # Attributes check
                attr_list = this_entity._attributes_map.values()
                if len(attr_list) != len(
                        mirror_entity._attributes_map.values()):
                    return False
                for this_attr in attr_list:
                    if this_attr.name not in mirror_entity._attributes_map:
                        return False
                    mirror_attr = mirror_entity._attributes_map[this_attr.name]
                    if this_attr.name != mirror_attr.name:
                        return False

                # Relations check
                rel_list = this_entity._relations_map.values()
                if len(rel_list) != len(mirror_entity._relations_map.values()):
                    return False
                for this_rel in rel_list:
                    if this_rel.name not in mirror_entity._relations_map:
                        return False
                    mirror_rel = mirror_entity._relations_map[this_rel.name]
                    if this_rel.name != mirror_rel.name:
                        return False
                    if this_rel.target_entity.name != \
                       mirror_rel.target_entity.name:
                        return False
            return True
        return NotImplemented

    def __ne__(self, other) -> bool:
        result = self.__eq__(other)
        if result is NotImplemented:
            return NotImplemented
        return not result

    def _build_model_information(self, model_description: "ModelDescription"):
        """Uses the check_model_description function to carry out the following
        checks:

          - the model has at least one entity
          - there are no duplicated entity names
          - entities mentioned in target or parents keys do exist
          - relation and attribute names are unique inside each
            single entity
          - IDs (when present) are unique among all entities, relations
            and attributes

        and additionaly checks that:

          - that there is no circularity in the inheritance relation
          - that 2 attributes or relations (with the same name) are
            not inherited simultaneously from different inheritance
            paths.

        In case of error a RuntimeError is raised.
        """
        check_model_description(model_description)

        # Create the EavModel.Entity for all model entities and store them
        # in the _global_entity_map dictionary.
        for entity in model_description.entities:
            eav_entity = EavModel.Entity(
                entity.id, entity.name, entity.description)
            self._global_entity_map[entity.name] = eav_entity
            if entity.id is not None:
                self._global_entity_map[entity.id] = eav_entity

        for entity in model_description.entities:
            eav_entity = self._global_entity_map[entity.name]

            # Create the EavModel.Attributes objects for all attributes of
            # each entity and store them in the entity's _attributes_map
            # dictionary, populating the own_attributes list as well.
            for attribute in entity.attributes:
                eav_attribute = EavModel.Attribute(
                    attribute.id, attribute.name, attribute.description,
                    eav_entity)
                eav_entity._attributes_map[attribute.name] = eav_attribute
                eav_entity.own_attributes.add(eav_attribute)
                if attribute.id is not None:
                    eav_entity._attributes_map[attribute.id] = eav_attribute
                    self._global_attr_map[attribute.id] = eav_attribute

            # Create the EavModel.Relation objects for all relations of
            # each entity and store them in the entity's _relations_map
            # dictionary, populating the own_relations list as well.
            for relation in entity.relations:
                eav_target_entity = self._global_entity_map[relation.target]
                eav_relation = EavModel.Relation(
                    relation.id, relation.name, relation.description,
                    eav_entity, eav_target_entity)
                eav_entity._relations_map[relation.name] = eav_relation
                eav_entity.own_relations.add(eav_relation)
                if relation.id is not None:
                    eav_entity._relations_map[relation.id] = eav_relation
                    self._global_rel_map[relation.id] = eav_relation

            # Populate knowledge about the one or more parent entities of
            # this entity.
            for parent_name in entity.parents:
                eav_entity._direct_parents.add(
                    self._global_entity_map[parent_name])

        # Compute the lists of (transitive) parents classes of each entity.
        # Note that the starting entity is not in the returned list.
        #
        # A RuntimeError is raised if a circularity in the inheritance
        # relation is detected.
        def __transitive_parents(
                start_entity: "EavModel.Entity", entity: "EavModel.Entity") \
                -> "Set[EavModel.Entity]":
            """Recursive method that collects the entities that are parents
            and parents of the parents, etc. of a given entity.
            """
            entities_set = set()
            for parent_entity in entity._direct_parents:
                if parent_entity == start_entity:
                    raise EAVModelError(
                        "Circular inheritance in entity: %s" % (
                            start_entity.name))

                entities_set.add(parent_entity)
                entities_set.update(__transitive_parents(
                    start_entity, parent_entity))

            return entities_set

        for eav_entity in self.entities():
            eav_entity.all_parents.update(
                __transitive_parents(eav_entity, eav_entity))

        # Compute the lists of (transitive) children classes of each entity.
        # Note that the starting entity IS in the children list.
        for eav_entity in self.entities():
            for parent_entity in eav_entity.all_parents:
                parent_entity.all_children.add(eav_entity)

        # Compute the inherited attributes and relations.
        #
        # An attribute or relation can be inherited more than once since
        # multiple inheritance is allowed. But inheriting 2 attributes or
        # relations with the same name coming from two different classes
        # throws a runtime error.

        def pairwise(iterable):
            a, b = tee(iterable)
            next(b, None)
            return zip(a, b)

        def _all_attributes(
                eav_entity: "EavModel.Entity",
                adefns: "Optional[Dict[str, Dict[EavModel.Entity, EavModel.Attribute]]]" =
                defaultdict(dict)) -> "Set[EavModel.Attribute]":
            attrs = {
                aname: a
                for parent in eav_entity._direct_parents
                for aname, a in _all_attributes(parent, adefns).items()
                }

            conflicts = {
                attr: set(locs)
                for attr, locs in adefns.items()
                if len(locs) > 1 and not all(
                    a == b for a, b in pairwise(locs.values()))
                }
            if conflicts:
                raise EAVModelError(
                    "Some attributes of entity '%s' are inherited from more "
                    "than one parent.", conflicts)

            for attr in eav_entity.own_attributes:
                attrs[attr.name] = attr
                adefns[attr.name] = {eav_entity: attr}

            return attrs

        def _all_relations(
                eav_entity: "EavModel.Entity",
                rdefns: "Optional[Dict[str, Dict[EavModel.Entity, EavModel.Attribute]]]" =
                defaultdict(dict)) -> "Dict[str, EavModel.Relation]":
            rels = {
                rname: r
                for parent in eav_entity._direct_parents
                for rname, r in _all_relations(parent, rdefns).items()
                }

            conflicts = {
                rel: set(locs)
                for rel, locs in rdefns.items()
                if len(locs) > 1 and not all(
                    a == b for a, b in pairwise(locs.values()))
                }
            if conflicts:
                raise EAVModelError(
                    "Some relations of entity '%s' are inherited from more "
                    "than one parent.", conflicts)

            for rel in eav_entity.own_relations:
                rels[rel.name] = rel
                rdefns[rel.name] = {eav_entity: rel}

            return rels

        for eav_entity in self.entities():
            for attr_name, attr_defn in _all_attributes(eav_entity).items():
                eav_entity.all_attributes.add(attr_defn)

                eav_entity._attributes_map[attr_name] = attr_defn
                if attr_defn.id is not None:
                    eav_entity._attributes_map[attr_defn.id] = attr_defn

            for rel_name, rel_defn in _all_relations(eav_entity).items():
                eav_entity.all_relations.add(rel_defn)

                eav_entity._relations_map[rel_name] = rel_defn
                if rel_defn.id is not None:
                    eav_entity._relations_map[rel_defn.id] = rel_defn

        # Compute the incoming relations for each entity
        all_rels = [
            r
            for entity in self.entities()
            for r in entity.own_relations
            ]
        for eav_entity in self.entities():
            eav_entity.incoming_rels = set(
                r for r in all_rels if r.target_entity == eav_entity)
