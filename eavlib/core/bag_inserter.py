from eavlib.core import ChangeBatch


class BagInserter:
    """Class to insert one or more EAV bags into a change batch (according to a
    provided EAV model).
    """

    def __init__(self, eav_model):
        """Builds the bag inserter with an EavModel object that is used
        translate attribute, entity and relation names into their numeric
        database identifiers.

        Information from the delivered bags is held in a private ChangeBatch
        instance.

        A set of visited nodes precludes that repeated objects are inserted
        more than once, be it because the insert method is called twice with
        the same bag, or because a common re- lated bag is referenced more than
        one if the inserted bags.
        """
        self._visited_nodes = set()
        self._change_batch = ChangeBatch()
        self._eav_model = eav_model

    def change_batch(self):
        return self._change_batch

    def insert(self, bag):
        """Add instructions to the change batch, so that this eav bag instance
        and its related bags get written in the batch for later database
        insertion.
        Throws a runtime error when entity, attribute or relation names are
        invalid in the EAV model.
        """

        # The web of objects associated to an EAV bag doesn't need
        # to be a tree. Since cycles are possible an accounting is
        # needed in order to visit each node only once. The python
        # identity is used for that,  because the eav_id attribute
        # might not be unique, or even missing.
        bag_identity = id(bag)
        if bag_identity not in self._visited_nodes:
            self._visited_nodes.add(bag_identity)

            entity_desc = self._eav_model.entity(bag.entity_name)
            self._change_batch.write_object(bag.eav_id, entity_desc.id)

            for attribute_name in bag.attributes:
                attribute_id = entity_desc.attribute(attribute_name).id
                attribute_val = bag.get_attribute(attribute_name)
                self._change_batch.write_attribute(
                    bag.eav_id, attribute_id, attribute_val)

            for relation_name, related_bags in bag.relations.items():
                for related_bag in related_bags:
                    relation_id = entity_desc.relation(relation_name).id
                    self._change_batch.write_relation(
                        bag.eav_id, relation_id, related_bag.eav_id)
                    self.insert(related_bag)
