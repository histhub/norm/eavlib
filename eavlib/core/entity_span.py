# TODO: fingerprints might need the class name. A PersonName and
#       PersonNameVariant might have the same data

from .model_vicinity import ModelVicinity
from collections import namedtuple


class EntitySpan:
    r"""Represents a set of information related to an entity, as a nested
    tree-like structure describing what attributes and relations are allowed.

                                                +---------+
                                                | Anode   |
    +-------------+                          *  | ======= |
    | EntitySpan  |                            /|  namid  |
    | =========== |         +---------+      /  +---------+
    |  namid      |      1  | Enode   |    /
    |  enode      | - - - - | ======= |  /
    +-------------+         |  amap   |/
                            |  rmap   |\
                            +---------+  \
                                           \    +----------+
                                             \  | Rnode    |
                                               \| ======== |
                                             *  |  namid   |    1  +---------+
                                                |  target  | - - - | Enode   |
                                                +----------+       +---------+

    Each of the map attributes (amap, rmap) are dictionaries where the keys are
    names and the values Anodes and Rnodes respectively.

    Entity spans are described using a language that combines the name of an
    entity with attributes and relations of an EVA model in the way described
    by the following grammar:

        ENTITY_SPAN  ::=  entity_name ENTITY_DESC
        ENTITY_DESC  ::=  '{'  ATTRI_LIST ?  RELATN_LIST ?  '}'
        ATTRIB_LIST  ::=  '['  attribute_name *  ']'
        RELATN_LIST  ::=  RELATN_DESC +
        RELATN_DESC  ::=  relation_name  ENTITY_DESC ?

    An entity span description example could be:

        // C++ comments are allowed.
        //
        Employee {
           [ id ]
           name
           address { [city] }
        }

    For the entity named 'Employee', load only its 'id' attribute, and all
    objects related by the 'name' relation retrieving all of their attributes
    (since omission means everything), and for objects related by the 'address'
    relation, retrieve instances of them having only the 'city' attribute
    loaded (and the rest ignored).
    """
    __slots__ = ('namid', 'enode')

    AnodeTuple = namedtuple('Anode', 'namid')
    RnodeTuple = namedtuple('Rnode', 'namid target')

    class Enode(namedtuple('Enode', 'amap rmap')):
        def anodes(self):
            return self.amap.values()

        def rnodes(self):
            return self.rmap.values()

        def sanames(self):
            return sorted(self.amap.keys())

        def srnames(self):
            return sorted(self.rmap.keys())

    def __init__(self, entity, enode):
        self.namid = entity.namid
        self.enode = enode

    @classmethod
    def Anode(cls, attribute):
        return cls.AnodeTuple(attribute.namid)

    @classmethod
    def Rnode(cls, relation, enode):
        return cls.RnodeTuple(relation.namid, enode)

    def __str__(self):
        def __enode_to_str(enode):
            attr_part = ' '.join(a.namid.name for a in enode.anodes())
            rela_part = ' '.join(
                "%s %s" % (r.namid.name, __enode_to_str(r.target))
                for r in enode.rnodes())
            return "{ [ %s ] %s }" % (attr_part, rela_part)

        return "%s %s" % (self.namid.name, __enode_to_str(self.enode))

    def to_vicinity(self):
        """Generates a model vicinity object able to load the set of
        information described by this entity span.
        """
        def __enode_to_wave_list(enode, waves, level=0):
            if len(waves) <= level:
                waves.append(ModelVicinity.Wave(set(), set()))
            # Attributes
            for a in enode.anodes():
                waves[level].attributes.add(a.namid)
            # Relations
            for r in enode.rnodes():
                waves[level].relations.add(r.namid)
                __enode_to_wave_list(r.target, waves, level+1)

        waves_list = list()
        __enode_to_wave_list(self.enode, waves_list)
        return ModelVicinity(self.namid, waves_list)

    def set_fingerprint(self, eav_bag):
        """Compute fingerprints for this bag and all related sub-bags
        using the information in this entity span as a guidance.
        If the bag contains more information that the one specified
        in the entity span it is ignored.
        """
        def __fingerprint_setter(enode, bag):
            attr_figp = ','.join(
                "%s=%s" % (aname, bag.get_attribute(aname))
                for aname in enode.sanames()
                if bag.get_attribute(aname))
            rel_figps = list()
            for rname in enode.srnames():
                related_bags = bag.get_related(rname)
                if len(related_bags) == 0:
                    continue
                elif len(related_bags) == 1:
                    rel_figps.append("%s=%s" % (
                        rname,
                        __fingerprint_setter(
                            enode.rmap[rname].target, related_bags[0])))
                else:
                    fp_list = list()
                    for child_bag in related_bags:
                        fp_list.append(__fingerprint_setter(
                            enode.rmap[rname].target, child_bag))
                    rel_figps.append("%s=[%s]" % (
                        rname, ",".join(sorted(fp_list))))

            bag.fingerprint = "{%s|%s}" % (attr_figp, ",".join(rel_figps))
            return bag.fingerprint

        return __fingerprint_setter(self.enode, eav_bag)
