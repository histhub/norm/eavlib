class EavBag:
    """Class to represent any instance stored in an EAV store."""

    def __init__(self, entity_name, eav_id=None):
        '''
                entity_name   Entity type or class of this instance as
                              a textual string

                eav_id        The id that will be used as database primary
                              key identity (normally a meaningless integer).
                              The instance could still have an 'id' attribu-
                              te to store whatever application id is needed.
        '''
        self.eav_id = eav_id
        self.entity_name = entity_name
        self.attributes = {}
        self.relations = {}

    def set_attribute(self, attr_name, attr_value):
        '''
            Sets the value of an attribute identified by its attribute
            name.

            Attributes are single valued, and thus any previous value is
            overwritten.
        '''
        self.attributes[attr_name] = attr_value

    def get_attribute(self, attr_name, def_value=None):
        '''
            Return the attribute value or the provided default value if
            the attribute is not set.
        '''
        return self.attributes.get(attr_name, def_value)

    def add_related(self, rel_name, related):
        '''
            Add an eav instance or a list of them to the list of instances
            related to this object by the given relation name.

            A check is made that the single instance or all the instances,
            if a list if passed, are of type EavBag.
        '''
        if not type(related) == list:
            related = [related]
        types_set = set(map(type, related))
        if EavBag in types_set and len(types_set) == 1:
            if rel_name not in self.relations:
                self.relations[rel_name] = []
            for bag in related:
                self.relations[rel_name].append(bag)
        else:
            raise RuntimeError('related instance is not EavBag')

    def get_related(self, rel_name):
        '''
            Return the list of related instances by the given relation
            name, or an empty list of none is present.
        '''
        return self.relations.get(rel_name, [])

    def set_eav_ids(self, id_generator):
        '''
            Generate an EAV id for this instance and recursively for all
            instances that are related to it.

            Identifiers should be unique at the object level. This is not
            checked by this method; it is assumed instead that the passed
            id generator fulfills this requirement.

            To be on the secure side, identifiers already present in this
            instance or any related one are overwritten, since otherwise
            there is no guaranty that old and new identifiers are unique.
        '''
        def __set_eav_ids(bag, id_generator, visited):
            '''
                The web of objects associated to an EAV bag doesn't need
                to be a tree. Since cycles are possible an accounting is
                needed in order to visit each node only once. The python
                identity is used for that.
            '''
            bag_identity = id(bag)
            if bag_identity not in visited:
                visited.add(bag_identity)
                bag.eav_id = id_generator.next_id()
                for related_objects in bag.relations.values():
                    for related_bag in related_objects:
                        __set_eav_ids(related_bag, id_generator, visited)

        __set_eav_ids(self, id_generator, set())

    def schedule_for_insertion(self, insert_batch, eav_model):
        '''
            Write instructions into the passed change batch, so that this
            eav bag instance and its related bags into are scheduled to be
            writen. The given eav model is used translate attribute, enti-
            tity, and relation names to numeric database identifiers.

            This instance and related ones must have a value assigned to
            the eav_id slot, otherwise an exception is raised.
        '''
        def __schedule_for_insertion(bag, insert_batch, eav_model, visited):
            '''
                The web of objects associated to an EAV bag doesn't need
                to be a tree. Since cycles are possible an accounting is
                needed in order to visit each node only once. The python
                identity is used for that.
            '''
            bag_identity = id(bag)
            if bag_identity not in visited:
                visited.add(bag_identity)

                entity_desc = eav_model.entity(bag.entity_name)
                insert_batch.write_object(bag.eav_id, entity_desc.id)

                for attribute_name in bag.attributes:
                    attribute_id = entity_desc.attribute(attribute_name).id
                    attribute_val = bag.get_attribute(attribute_name)
                    insert_batch.write_attribute(
                        bag.eav_id, attribute_id, attribute_val)

                for relation_name, related_bags in bag.relations.items():
                    for related_bag in related_bags:
                        relation_id = entity_desc.relation(relation_name).id
                        insert_batch.write_relation(
                            bag.eav_id, relation_id, related_bag.eav_id)
                        __schedule_for_insertion(
                            related_bag, insert_batch, eav_model, visited)

        __schedule_for_insertion(self, insert_batch, eav_model, set())

    def schedule_for_deletion(self, delete_batch):
        '''
            Recursively records deletion instructions into the given modi-
            fication batch for this instance and related eav bags (using
            the eav model object to translate attribute, entity, and rela-
            tion names to their corresponding numeric identifiers).

            This instance and related ones must have a value assigned to
            the eav_id slot, otherwise an exception is raised.
        '''
        def __schedule_for_deletion(bag, delete_batch, visited):
            '''
                The web of objects associated to an EAV bag doesn't need
                to be a tree. Since cycles are possible an accounting is
                needed in order to visit each node only once. The python
                identity is used for that.
            '''
            bag_identity = id(bag)
            if bag_identity not in visited:
                visited.add(bag_identity)

                delete_batch.delete_all_object_data(bag.eav_id)
                for list_of_related_bags in bag.relations.values():
                    for related_bag in list_of_related_bags:
                        __schedule_for_deletion(
                            related_bag, delete_batch, visited)

        __schedule_for_deletion(self, delete_batch, set())

    def empty(self):
        '''
            True if the instance doesn't have any attribute or rela-
            tion assigned, without taking the reserved database id
            into account.
        '''
        return (len(self.attributes) + len(self.relations)) == 0

    def _depth_dict(self):
        '''
            Computes a depth dictionary: { id(node) -> depth }

            The dictionary has an entry for each node that is tran-
            sitively related to this node where the key is the node's
            unique id (as returned by id()) and the value is the
            shortest path length from the this root node to it.
        '''
        depth_dict = {id(self): 0}
        self._depth_dict_traverser(depth_dict, 1)
        return depth_dict

    def _depth_dict_traverser(self, depth_dict, level):
        """Helper method to build the depth dictionary."""""
        for nodes_list in self.relations.values():
            for node in nodes_list:
                node_id = id(node)

                # First time the node is visited: write path length
                # and continue traversal.
                if node_id not in depth_dict:
                    depth_dict[node_id] = level
                    node._depth_dict_traverser(depth_dict, level+1)
                else:
                    # Node already visited, but by a longer path to root.
                    # Adjust and continue traversal.
                    if level < depth_dict[node_id]:
                        depth_dict[node_id] = level
                        node._depth_dict_traverser(depth_dict, level+1)

                    # Node already visited by a shorter path: ignore
                    # and stop infinite recursion.
                    else:
                        pass

        return depth_dict

    def traverse(self, bag_traverser, preorder=True):
        '''
            Method to allow traversal of the tree of sub-objects made
            up by this eav bag and the bags referenced in the relations
            of this instance.

            The bag traverser is any object provided that it has a

                def process(self, bag):
                    pass

            "process" method that will be called once for every bag
            present in the tree hierarchy. Inside the method any ac-
            tion can be carried out: statistics collection, attribu-
            tes modifications, etc. Relations modification can bring
            about unexpected behavior.

            The traversal can be in "preorder" meaining that first
            is a node processed and then the hanging sub-nodes, or
            "postorder" (preorder=False), meaning first are all the
            children processed and then the parent. "Inorder" doesn't
            make sense since tree hierarchy is not thought to a binary
            tree.
        '''
        if preorder:
            bag_traverser.process(self)

        for target in self.relations.values():
            if type(target) == list:
                for node in target:
                    node.traverse(bag_traverser, preorder=preorder)
            else:
                target.traverse(bag_traverser, preorder=preorder)

        if not preorder:
            bag_traverser.process(self)

    def __str__(self):
        """Produce a JSON like string representation of this instance."""
        lines = []
        self._dump_into(lines, 0)
        return '\n'.join(lines)

    def _dump_into(self, dump_list, level=0, pad=4*' ', prepend=None):
        '''
            Recursively feeds a list with classes, attributes and
            relations descriptions.

            - dump_list     the list getting fed that will be joined and
                            returned at recursion end
            - level         a number increasing as recursion goes deeper
                            in the sub-objects graph indicating the level
                            of indentation
            - pad           multiplied by level gives the amount of white-
                            space that constitutes the indentation
            - prepend       a relation name that is to go in front of a
                            class name
        '''
        curr_indent = pad * level
        next_indent = pad * (level + 1)

        prefix = "{}{} = ".format(curr_indent, prepend) \
            if prepend is not None else curr_indent

        if self.empty():
            dump_list.append("{}{} ({})".format(
                prefix, self.entity_name, self.eav_id))

        else:
            if self.eav_id is None:
                dump_list.append("{}{} {{".format(prefix, self.entity_name))
            else:
                dump_list.append("{}{} ({}) {{".format(
                    prefix, self.entity_name, self.eav_id))

            # dump attributes
            for name, value in self.attributes.items():
                dump_list.append("{}{} = '{}'".format(
                    next_indent, name, value))

            # dump related instances
            for name, related_list in self.relations.items():
                if len(related_list) == 1:
                    related_list[0]._dump_into(dump_list, level+1, pad, name)
                elif len(related_list) > 1:
                    dump_list.append("{}{} = [".format(next_indent, name))
                    for bag in related_list:
                        bag._dump_into(dump_list, level+2, pad)
                    dump_list.append("{} ]".format(next_indent))
                else:
                    pass

            if level == 0:
                dump_list.append("}")
            else:
                dump_list.append("{}}}".format(curr_indent))

    def fingerprint(self):
        fingerprint_items = [
            '%s="%s"' % (attr_name, attr_value)
            for attr_name, attr_value in map(
                    lambda aname: (aname, self.get_attribute(attr_name)),
                    sorted(self.attributes))
            if attr_value
            ]

        for relation_name in sorted(self.relations):
            related_bags = self.get_related(relation_name)
            relation_val = "[%s]" % ",".join(sorted(
                b.fingerprint() for b in related_bags))
            fingerprint_items.append("%s=%s" % (relation_name, relation_val))

        return "{%s}" % ",".join(fingerprint_items)
