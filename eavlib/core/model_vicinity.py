from collections import namedtuple


class ModelVicinity:
    '''
        A model vicinity instance for a given entity of an EAV model is
        a data structure storing relations and attributes of that model,
        that are needed to load a particular set of information sorroun-
        ding the given entity in the given model.
    '''

    # A wave is a pair of attributes and relation (namids) lists.
    # The attributes contain information about entities that are placed at the
    # same radial distance of a central entity, and the relations are the hops
    # to follow to get to entities belonging to the next wave in the process of
    # loading a model vicinity for one or more instances of the same class.
    Wave = namedtuple('Wave', 'attributes relations')

    def __init__(self, entity_namid, waves_list):
        self.namid = entity_namid
        self.waves = waves_list

    def __str__(self):
        items = list()
        items.append("Start entity   : {} ({})".format(
            self.namid.name, self.namid.id))

        for i, wave in enumerate(self.waves):
            items.append("  Attributes {} : {}".format(
                i, ', '.join(
                    "{} ({})".format(n.name, n.id)
                    for n in wave.attributes)))
            items.append("  Relations  {} : {}".format(
                i, ', '.join(
                    "{} ({})".format(n.name, n.id)
                    for n in wave.relations)))

        return '\n'.join(items)


def model_vicinity_from_radial_path(eav_model, radial_path_set):
    '''
        Computes a model vicinity set from all the radial paths of departing
        from the central entity.

            eav_model           An EAV model instance to validate names and
                                paths, and translate their names into ids

            radial_path_set     A radial paths set, that is a central entity
                                and a list of strings representing paths con-
                                sisting in either a single attribute name of
                                the given entity, or in one or more dot sepa-
                                rated relation names that, followed by an
                                optional attribute name. For example:
                                [
                                    'lang',
                                    'forenames.order',
                                    'forenames.forename.spelling',
                                    'surnames.order',
                                    'surnames.namelink.spelling',
                                    'surnames.surname.spelling',
                                    'standard_name'
                                ]
                                Of course, the first relation in each path is
                                a valid relation of the central entity, the
                                second a valid relation of the target entity
                                of the precious one, and so on.
    '''
    start_entity = eav_model.entity(radial_path_set.central_entity)
    splitted_paths = [path.split('.') for path in radial_path_set.path_list]
    radial_depth = max(map(len, splitted_paths))
    attr_sets_list = tuple([set() for i in range(radial_depth)])
    rel_sets_list = tuple([set() for i in range(radial_depth)])

    for i, splitted_path in enumerate(splitted_paths):
        current_entity = start_entity

        # Internal names must be relations and be properly chained.
        last_index = len(splitted_path)-1
        for j in range(last_index):
            try:
                curr_relation = current_entity.relation(splitted_path[j])
                rel_sets_list[j].add(curr_relation.namid)
                current_entity = curr_relation.target_entity
            except RuntimeError:
                raise RuntimeError(
                    "'{}' in path '{}', not a valid relation name".format(
                        splitted_path[j], radial_path_set.path_list[i]))

        # The last names can be an attribute or a relation name.
        try:
            leaf_attr = current_entity.attribute(splitted_path[-1])
            attr_sets_list[last_index].add(leaf_attr.namid)
        except RuntimeError:
            try:
                leaf_rel = current_entity.relation(splitted_path[-1])
                rel_sets_list[last_index].add(leaf_rel)
            except RuntimeError:
                raise RuntimeError(
                    "'{}' at the end of path '{}', not a valid attribute or "
                    "relation name for entity '{}'".format(
                        splitted_path[-1], radial_path_set.path_list[i],
                        current_entity.name))

    waves_list = [
        ModelVicinity.Wave(
            tuple(attr_sets_list[i]),
            tuple(rel_sets_list[i]))
        for i in range(radial_depth)]
    return ModelVicinity(start_entity.namid, tuple(waves_list))
