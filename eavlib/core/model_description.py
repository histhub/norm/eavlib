# flake8: noqa: E501

from collections import namedtuple
from yaml import (load, SafeLoader)


class ModelDescription:
    """Minimal information about an EAV model needed to read it from, or write
    it into a YAML file or database (with eav schema).

    This class is only a bundling of named tuples used in the
    EAV model minimal description.

    Model
    -----
        - id, name, description are self-explanatory
        - entities, is a list of Entity named tuples

    Entity
    ------
        - id, name, description are self-explanatory
        - parents, list of parent entity names
        - attributes, list of Attribute named tuples
        - relations, list of Relation named tuples

    Attribute
    ---------
        - id, name, description are self-explanatory
        - required, True or False

    Relation
    --------
        - id, name, description are self-explanatory
        - target, name of the entity type that is this relation's target
        - card_min, zero or positive integer
        - card_max, zero or positive integer. Zero meaning unrestricted
          cardinality
    """
    Model = namedtuple("Model", ("id", "name", "description", "entities"))
    Entity = namedtuple("Entity", (
        "id", "name", "description", "parents", "attributes", "relations"))
    Attribute = namedtuple("Attribute", (
        "id", "name", "description", "required"))
    Relation = namedtuple("Relation", (
        "id", "name", "description", "target", "card_min", "card_max"))


def load_model_from_yaml(filename):
    """Builds an EAV model description from a YAML file.

    Raises either a FileNotFoundError if the filename is not accessible or a
    yaml.parser.ParserError if the YAML file has syntax problems.
    """

    # 3 helper functions

    def flatten_dict(d):
        """Helper function that creates a flattened textual representation of a
        dictionary not giving details of nested lists or dictionaries in order
        not to clutter error messages.
        """
        key_val_pairs = [
            (k, '...') if type(d[k]) in (dict, list) else (k, d[k])
            for k in d]
        return ', '.join(["{} : {}".format(k, v) for (k, v) in key_val_pairs])

    errors = list()

    def mandatory_key(dictionary, key):
        """Returns the value associated to the given mandatory key if present
        in the given dictionary; otherwise an error messages is issued.
        """
        if key in dictionary:
            return dictionary[key]
        else:
            errors.append("expecting key '{}' in {{ {} }}".format(
                key, flatten_dict(dictionary)))
            return None

    def optional_key(dictionary, key, default_value):
        """Returns the value associated to the given optional key if present in
        the given dictionary, or the pro- vided default values if absent.
        """
        value = dictionary.get(key, None)
        # Deals with the 2 cases where the get() method returns None:
        # - the key is not present in the YAML source, or
        # - the key is present but left empty (where the YAML
        #   reader associates it to None).
        return default_value if value is None else value

    # Load YAML file into a python dictionary
    try:
        with open(filename) as input_file:
            dictionary = load(input_file, Loader=SafeLoader)
    except FileNotFoundError:
        raise FileNotFoundError(
            "cannot access model description file '{}'".format(filename))

    # Builds a model description tuple from the loaded dict
    unknown_id = None
    model_dict = mandatory_key(dictionary, 'model')
    if not errors:
        name = mandatory_key(model_dict, 'name')
        description = optional_key(model_dict, 'description', None)
        entities = optional_key(model_dict, 'entities', list())

        model = ModelDescription.Model(unknown_id, name, description, list())

        for entity_dict in entities:
            name = mandatory_key(entity_dict, 'name')
            description = optional_key(entity_dict, 'description', None)
            parents = optional_key(entity_dict, 'parents', list())
            attributes = optional_key(entity_dict, 'attributes', list())
            relations = optional_key(entity_dict, 'relations', list())
            if parents:
                parents = [s.strip() for s in parents.split(',')]

            entity = ModelDescription.Entity(
                unknown_id, name, description, parents, list(), list())
            model.entities.append(entity)

            for attribute_dict in attributes:
                name = mandatory_key(attribute_dict, 'name')
                description = optional_key(attribute_dict, 'description', None)
                required = optional_key(attribute_dict, 'required', False)
                entity.attributes.append(ModelDescription.Attribute(
                    unknown_id, name, description, required))

            for relation_dict in relations:
                name = mandatory_key(relation_dict, 'name')
                description = optional_key(relation_dict, 'description', None)
                target = mandatory_key(relation_dict, 'target')
                card_min = optional_key(relation_dict, 'card_min', 0)
                card_max = optional_key(relation_dict, 'card_max', float("inf"))
                entity.relations.append(ModelDescription.Relation(
                    unknown_id, name, description, target, card_min, card_max))

    if errors:
        raise RuntimeError('\n'.join(errors))

    return model


def check_model_description(model):
    """Carries out the following consistency checks returning silently if no
    problem is found; otherwise a RuntimeError is raised containing a list of
    errors.

      - the model has at least one entity
      - there are no duplicated entity names
      - entities mentioned in target or parents keys do exist
      - relation and attribute names are unique inside each single entity
      - ids (when present) are unique among all entities, attributes and
        relations.
    """
    errors = list()

    if len(model.entities) < 1:
        errors.append('model does not define any entity')
    else:
        entity_names_set = set()
        for entity in model.entities:
            # duplicated entities
            if entity.name in entity_names_set:
                errors.append("entity '{}' is duplicated".format(entity.name))
                continue
            else:
                entity_names_set.add(entity.name)

        for entity in model.entities:
            # attribute names are unique in each entity
            attribute_names_set = set()
            for attr in entity.attributes:
                if attr.name in attribute_names_set:
                    template = "repeated attribute '{}' in entity '{}'"
                    errors.append(template.format(attr.name, entity.name))
                else:
                    attribute_names_set.add(attr.name)

            # relation names are unique in each entity
            relation_names_set = set()
            for relation in entity.relations:
                if relation.name in relation_names_set:
                    template = "repeated relation '{}' in entity '{}'"
                    errors.append(template.format(relation.name, entity.name))
                else:
                    relation_names_set.add(relation.name)

            # target entities do exist
            for relation in entity.relations:
                if relation.target not in entity_names_set:
                    template = "relation '{}' has unknown target '{}' in entity '{}'"
                    errors.append(template.format(relation.name, relation.target, entity.name))

            # parent entities do exist
            for parent in entity.parents:
                if parent not in entity_names_set:
                    errors.append("entity '{}' has unknown parent '{}'".format(
                        entity.name, parent))

        # IDs (when present) of entities, attributes and relations must be unique.
        if model.id is not None:
            ids_set = set()
            for entity in model.entities:
                if entity.id is None:
                    errors.append("Entity '{}' has no ID".format(entity.name))
                elif entity.id in ids_set:
                    errors.append("Entity '{}' has a duplicate ID {}".format(
                        entity.name, entity.id))
                else:
                    ids_set.add(entity.id)
            del ids_set

            ids_set = set()
            for attribute in entity.attributes:
                if attribute.id is None:
                    errors.append(
                        "Attribute '{}' of entity '{}' has no ID".format(
                            attribute.name, entity.name))
                elif attribute.id in ids_set:
                    errors.append(
                        "Attribute '{}' of entity '{}' has a duplicate ID {}"
                        .format(attribute.name, entity.name, attribute.id))
                else:
                    ids_set.add(attribute.id)
            del ids_set

            ids_set = set()
            for relation in entity.relations:
                if relation.id is None:
                    errors.append(
                        "Relation '{}' of entity '{}' has no ID".format(
                            relation.name, entity.name))
                elif relation.id in ids_set:
                    errors.append(
                        "Relation '{}' of entity '{}' has a duplicate ID {}"
                        .format(relation.name, entity.name, relation.id))
                else:
                    ids_set.add(relation.id)
            del ids_set

    if errors:
        raise RuntimeError('\n'.join(errors))
