# flake8: noqa: E501

from .entity_span import EntitySpan
from collections import namedtuple
from pypeg2 import (
    Parser, List, attr, comment_cpp, maybe_some, optional, some, word)


def build_entity_span(eav_model, entity_span_text):
    """Function in charge of parsing the text of an entity span and delivering
    an eavlib.core.EntitySpan object.

    The parsing is done with the pyPEG2 (https://fdik.org/pyPEG/index.html)
    library. All implementation details are inside this function that trans-
    lates the pyPEG2 AST representation into an EntitySpan instance.

    ATTENTION!! This function handle overridden properties correctly!
    Do NOT use this function to retrieve "potential" attributes or relations of
    subclasses that are overridden in the ontology!
    Overridden properties that are inherited are handled correctly.
    """

    #  Parsing Expression Grammar:
    #
    #     Entity span               ESPAN  :=  name  EDESC
    #     Entity description        EDESC  :=  {  ALIST?  RLIST?  }
    #     Attribute list            ALIST  :=  [  name  ]
    #     Relations list            RLIST  :=  RDESC+
    #     Relation description      RDESC  :=  name  EDESC?
    #

    #  Raises:
    #   - SyntaxError   if problems arise while parsing the entity span text
    #   - RuntimeError  if model entities, attributes or relations mentioned
    #                   in the span text are unknown.

    class ESPAN:
        pass

    class EDESC:
        pass

    class RDESC:
        pass

    class ALIST(List):
        pass

    class RLIST(List):
        pass

    ESPAN.grammar = attr('entity_name', word), attr('entity_desc', EDESC)
    EDESC.grammar = '{', attr('attr_list', optional(ALIST)), attr('rela_list', optional(RLIST)), '}'
    ALIST.grammar = "[", maybe_some(word), "]"
    RLIST.grammar = some(RDESC)
    RDESC.grammar = attr('rela_name', word), attr('target_desc', optional(EDESC))

    parser = Parser()
    parser.comment = comment_cpp
    unparsed, ast = parser.parse(entity_span_text, ESPAN)
    if unparsed:
        raise SyntaxError("unparsed text at EOF '%s'" % (unparsed))

    def _possible_attributes(entity):
        possible_attributes = {
            a.name: a
            for e in entity.all_children
            for a in e.own_attributes}
        possible_attributes.update({
            a.name: a for a in entity.all_attributes})
        return possible_attributes

    def _possible_relations(entity):
        possible_relations = {
            r.name: r
            for e in entity.all_children
            for r in e.own_relations}
        possible_relations.update({
            r.name: r for r in entity.all_relations})
        return possible_relations

    def __pypeg2_ast_to_entity_span(model, parsed_edesc, cur_entity, cur_path):
        possible_attributes = _possible_attributes(cur_entity)

        if parsed_edesc.attr_list is None:
            # add all attributes, if nothing is mentioned
            attributes_map = {
                n: EntitySpan.Anode(a)
                for n, a in possible_attributes.items()}
        else:
            # otherwise collect the explicitly mentioned attributes
            attributes_map = {}
            for aname in parsed_edesc.attr_list:
                try:
                    attr = possible_attributes[aname]
                    attributes_map[attr.name] = EntitySpan.Anode(attr)
                except (KeyError, RuntimeError):
                    raise RuntimeError("unknown attribute at '%s.%s'" % (
                        cur_path, aname))

        # traverse the relations
        relations_map = {}
        if parsed_edesc.rela_list:
            possible_relations = _possible_relations(cur_entity)

            for rel_desc in parsed_edesc.rela_list:
                try:
                    rname = rel_desc.rela_name
                    relation = possible_relations[rname]
                    next_entity = relation.target_entity
                except RuntimeError:
                    raise RuntimeError("unknown relation at '%s.%s'" % (
                        cur_path, rname))

                # When a relation name is alone, it is understood that what has
                # to be loaded is the target instance with all its available
                # attributes; otherwise an explicit enumeration of the desired
                # attributes has to be provided. An artificial target_desc is
                # created containing all attributes of the target entity
                # without any relations, so that recursion will take care of
                # the special case as well.
                if rel_desc.target_desc is None:
                    rel_desc.target_desc = namedtuple(
                        'FakeParse', ('attr_list', 'rela_list'))(
                            _possible_attributes(next_entity).keys(), None)

                new_path = "%s.%s" % (cur_path, rname)
                target_enode = __pypeg2_ast_to_entity_span(
                    model, rel_desc.target_desc, next_entity, new_path)
                relations_map[relation.name] = EntitySpan.Rnode(
                    relation, target_enode)

        return EntitySpan.Enode(attributes_map, relations_map)

    entity = eav_model.entity(ast.entity_name)
    enode = __pypeg2_ast_to_entity_span(
        eav_model, ast.entity_desc, entity, entity.name)
    return EntitySpan(entity, enode)
