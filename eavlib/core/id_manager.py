import typing

if typing.TYPE_CHECKING:
    from typing import Iterator

    from eavlib.connection import EAVConnection


class SequenceIDManager:
    """Generates database IDs using a sequence in the EAV repository."""

    def __init__(self, eav_connection: "EAVConnection", sequence_name: str):
        self.eav_conn = eav_connection
        self.sequence_name = sequence_name

    def __next__(self) -> int:
        """Returns the next sequence number.

        :throws Exception: if the sequence is not available
        """
        with self.eav_conn.cursor() as cursor:
            cursor.execute(self.eav_conn.query_dict.sequence_next_id_query, (
                self.sequence_name,))
            return int(cursor.fetchone()[0])

    def next_id(self) -> int:
        return next(self)

    def take(self, count: int) -> "Iterator[int]":
        """Takes multiple IDs from the sequence.

        :param count: the number of IDs to take.
        :type count: int
        :raises ValueError: if count is less than 1.
        :returns: Iterator[HistHubID] - an iterator yielding as many histHub
        IDs as requested.
        """
        if count < 1:
            raise ValueError("you must take at least 1 ID")

        with self.eav_conn.cursor() as cursor:
            cursor.execute(self.eav_conn.query_dict.sequence_next_ids_query, (
                self.sequence_name, count))
            yield from (row[0] for row in cursor)
