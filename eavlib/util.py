import sys


def quit_on_error(error_msg, error_code=1):
    """Prints an error message into standard error an quits returning the given
    error code.
    """
    error_lines = ["ERROR: %s" % _ for _ in error_msg.strip().split('\n')]
    print("\n".join(error_lines), file=sys.stderr)
    sys.exit(error_code)
