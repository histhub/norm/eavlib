import collections.abc

from eavlib.core.model_manager import ModelManager
from eavlib.connection import EAVConnection

from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from typing import (Any, Dict, Iterator, Tuple, Union)
    from eavlib.gremlin.traversal import GraphTraversalSource


class Graph:
    __slots__ = ("connection", "model_id")

    def __init__(self, eav_conn: EAVConnection, model: "Union[int, str]") \
            -> None:
        self.connection = eav_conn
        model_manager = ModelManager(eav_conn)
        self.model_id = model_manager.id_from_text(model)

    def copy(self) -> "Graph":
        new_eav_conn = self.connection.copy()
        return Graph(new_eav_conn, self.model_id)

    def traversal(self) -> "GraphTraversalSource":
        from eavlib.gremlin.traversal import GraphTraversalSource
        return GraphTraversalSource(self)

    def __repr__(self) -> str:
        return "Graph[%s, model: %s]" % (self.connection, self.model_id)


class Node:
    __slots__ = ("id",)

    def __init__(self, id: int) -> None:
        self.id = id

    def __repr__(self) -> str:
        return ("Node(%i)" % self.id)

    def __eq__(self, other) -> bool:
        if isinstance(other, self.__class__):
            return self.id == other.id
        return False

    def __hash__(self) -> int:
        return hash(self.id)


class Relation:
    __slots__ = ("outV", "relation", "inV")

    def __init__(self, subject: Node, predicate: str, object: Node) -> None:
        self.outV = subject
        self.relation = predicate
        self.inV = object

    def __repr__(self) -> str:
        return "Relation[%s-%s->%s]" % (
            self.outV, self.relation, self.inV)


class NodeAttribute:
    __slots__ = ("node", "key", "value")

    def __init__(self, key: str, value, node: Node) -> None:
        self.value = value
        self.key = key
        self.node = node

    def __repr__(self) -> str:
        return "NodeAttribute[%s->%s]" % (self.key, self.value)


class NodeLabel(str):
    def __new__(cls, label, node=None):
        lbl = super().__new__(cls, label)
        lbl.node = node
        return lbl


class NodeSelection(dict):
    pass


class NodeValueMap(collections.abc.Mapping):
    __slots__ = ("_dict", "_node", "_hash")

    def __init__(self, node: Node, value_map: "Dict[str, str]") -> None:
        self._node = node
        self._dict = dict(value_map) if value_map else dict()   # make a copy

    @property
    def node(self) -> Node:
        return self._node

    def __getitem__(self, key: str) -> str:
        return self._dict[key]

    def __dict__(self) -> "Dict[str, str]":
        return dict(self._dict)  # make a copy

    def __repr__(self) -> str:
        return repr(self._dict)

    def __len__(self) -> int:
        return len(self._dict)

    def __iter__(self) -> "Iterator[Tuple[str, str]]":
        return iter(self._dict)

    def __hash__(self) -> int:
        if not hasattr(self, "_hash"):
            self._hash = hash(frozenset(self._dict.items()))
        return self._hash

    def __eq__(self, other: "Any") -> bool:
        if isinstance(other, self.__class__):
            return self._dict == other._dict
        return False
