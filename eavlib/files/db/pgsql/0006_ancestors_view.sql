-- -*- mode: sql; sql-product: postgres; indent-tabs-mode: nil -*-

CREATE MATERIALIZED VIEW {SCHEMA_NAME}.model_ancestors
    (entity_id, parent_id)
    AS (
        WITH RECURSIVE parents (entity_id, parent_id) AS (
            SELECT id AS "entity_id", id AS "parent_id" FROM {SCHEMA_NAME}.model_entity
          UNION ALL
            SELECT entity_id, parent_id FROM {SCHEMA_NAME}.model_inheritance
          UNION ALL
            SELECT mi.entity_id, p.parent_id
            FROM {SCHEMA_NAME}.model_inheritance mi, parents p
            WHERE mi.parent_id = p.entity_id
        )
        SELECT DISTINCT * FROM parents
    )
    WITH DATA;

CREATE FUNCTION {SCHEMA_NAME}.refresh_model_ancestors() RETURNS trigger
    LANGUAGE plpgsql
    VOLATILE
    PARALLEL UNSAFE
AS $$
BEGIN
    REFRESH MATERIALIZED VIEW {SCHEMA_NAME}.model_ancestors WITH DATA;
    RETURN NULL;
END;
$$;


CREATE TRIGGER refresh_ancestors_view
    AFTER INSERT OR UPDATE OR DELETE OR TRUNCATE
    ON {SCHEMA_NAME}.model_inheritance
    FOR EACH STATEMENT
    EXECUTE PROCEDURE {SCHEMA_NAME}.refresh_model_ancestors();


DO LANGUAGE plpgsql $$
DECLARE
   version integer[] = (
      SELECT string_to_array(
         substring(version() from '^PostgreSQL (\d+(\.\d+)*)'), '.')
      );
BEGIN
   RAISE NOTICE 'Detected PostgreSQL version: %', version;

   IF version >= ARRAY[11] THEN
      EXECUTE $IDX1_11$
      CREATE INDEX model_ancestors_entity_id
          ON {SCHEMA_NAME}.model_ancestors ("entity_id") INCLUDE ("parent_id");
      $IDX1_11$;

      EXECUTE $IDX2_11$
      CREATE INDEX model_ancestors_parent_id
          ON {SCHEMA_NAME}.model_ancestors ("parent_id") INCLUDE ("entity_id");
      $IDX2_11$;
   ELSE
      EXECUTE $IDX1$
      CREATE INDEX model_ancestors_entity_id
          ON {SCHEMA_NAME}.model_ancestors ("entity_id");
      $IDX1$;

      EXECUTE $IDX2$
      CREATE INDEX model_ancestors_parent_id
          ON {SCHEMA_NAME}.model_ancestors ("parent_id");
      $IDX2$;
   END IF;
END
$$;
