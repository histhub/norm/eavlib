-- -*- mode: sql; sql-product: postgres; indent-tabs-mode: nil -*-


--- object_attribute table

DROP INDEX IF EXISTS {SCHEMA_NAME}.attributes_object_ids_index;
DROP INDEX IF EXISTS {SCHEMA_NAME}.attributes_key_value_index;

ALTER TABLE {SCHEMA_NAME}.object_attribute
    ADD PRIMARY KEY ("attribute_id", "object_id");


--- object_relation table

DROP INDEX IF EXISTS {SCHEMA_NAME}.relations_object_ids_index;
