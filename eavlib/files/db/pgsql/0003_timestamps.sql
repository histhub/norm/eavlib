-- -*- mode: sql; sql-product: postgres; indent-tabs-mode: nil -*-


-- object table
ALTER TABLE {SCHEMA_NAME}.object
    ADD COLUMN ctime TIMESTAMP WITH TIME ZONE NOT NULL
        DEFAULT CURRENT_TIMESTAMP;

ALTER TABLE {SCHEMA_NAME}.object
    ADD COLUMN mtime TIMESTAMP WITH TIME ZONE NOT NULL
        DEFAULT CURRENT_TIMESTAMP;


-- trigger
CREATE FUNCTION {SCHEMA_NAME}.update_object_mtime() RETURNS trigger
    PARALLEL UNSAFE AS
$$
BEGIN
    IF (TG_OP = 'UPDATE') THEN
        UPDATE {SCHEMA_NAME}.object o
        SET mtime = CURRENT_TIMESTAMP
        WHERE o.id = NEW.object_id OR o.id = OLD.object_id;
    ELSIF (TG_OP = 'INSERT') THEN
        UPDATE {SCHEMA_NAME}.object o
        SET mtime = CURRENT_TIMESTAMP
        WHERE o.id = NEW.object_id;
    ELSIF (TG_OP = 'DELETE') THEN
        UPDATE {SCHEMA_NAME}.object o
        SET mtime = CURRENT_TIMESTAMP
        WHERE o.id = OLD.object_id;
    END IF;

    RETURN NULL; -- result is ignored since this is an AFTER trigger
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER update_object_mtime
    AFTER INSERT OR UPDATE OR DELETE
    ON {SCHEMA_NAME}.object_attribute
    FOR EACH ROW
    -- WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE {SCHEMA_NAME}.update_object_mtime();

CREATE TRIGGER update_object_mtime
    AFTER INSERT OR UPDATE OR DELETE
    ON {SCHEMA_NAME}.object_relation
    FOR EACH ROW
    -- WHEN (OLD.* IS DISTINCT FROM NEW.*)
    EXECUTE PROCEDURE {SCHEMA_NAME}.update_object_mtime();
