-- -*- mode: sql; sql-product: postgres; indent-tabs-mode: nil -*-


--- model_attribute table

ALTER TABLE {SCHEMA_NAME}.model_attribute
    ADD CONSTRAINT model_attributes_are_unique UNIQUE
    ("entity_id", "name");


--- model_entity table

ALTER TABLE {SCHEMA_NAME}.model_entity
    ADD CONSTRAINT model_entities_are_unique UNIQUE
    ("model_id", "name");


--- object_relation table

ALTER TABLE {SCHEMA_NAME}.object_relation
    ADD CONSTRAINT object_relations_are_unique UNIQUE
    ("object_id", "relation_id", "target_id");
