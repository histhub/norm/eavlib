-- -*- mode: sql; sql-product: postgres; indent-tabs-mode: nil -*-

--
-- Name: model_id_seq; Type: SEQUENCE
--

CREATE SEQUENCE {SCHEMA_NAME}.model_id_seq
    AS integer
    INCREMENT BY 1
    START WITH 1
    NO CYCLE
    OWNED BY {SCHEMA_NAME}.model.id;

SELECT
    setval('{SCHEMA_NAME}.model_id_seq', m.max_val + 1, false)
FROM
    (SELECT COALESCE(max(id), 0) AS max_val FROM {SCHEMA_NAME}.model) m;

ALTER TABLE {SCHEMA_NAME}.model
    ALTER COLUMN id SET DEFAULT nextval('{SCHEMA_NAME}.model_id_seq');


--
-- Name: model_seq; Type: SEQUENCE
--

CREATE SEQUENCE {SCHEMA_NAME}.model_seq
    AS integer
    INCREMENT BY 1
    START WITH 1
    NO CYCLE
    OWNED BY NONE;

SELECT
    setval('{SCHEMA_NAME}.model_seq', m.max_val + 1, false)
FROM (
     SELECT
         GREATEST(
             COALESCE(max(ma.id), 0),
             COALESCE(max(me.id), 0),
             COALESCE(max(mr.id), 0)
         ) AS max_val
     FROM
        {SCHEMA_NAME}.model_attribute ma,
        {SCHEMA_NAME}.model_entity me,
        {SCHEMA_NAME}.model_relation mr
) m;

ALTER TABLE {SCHEMA_NAME}.model_attribute
    ALTER COLUMN id SET DEFAULT nextval('{SCHEMA_NAME}.model_seq');
ALTER TABLE {SCHEMA_NAME}.model_entity
    ALTER COLUMN id SET DEFAULT nextval('{SCHEMA_NAME}.model_seq');
ALTER TABLE {SCHEMA_NAME}.model_relation
    ALTER COLUMN id SET DEFAULT nextval('{SCHEMA_NAME}.model_seq');


--
-- Name: object_id_seq; Type: SEQUENCE
--

CREATE SEQUENCE {SCHEMA_NAME}.object_id_seq
    AS integer
    INCREMENT BY 1
    START WITH 1
    NO CYCLE
    OWNED BY {SCHEMA_NAME}.object.id;

SELECT
    setval('{SCHEMA_NAME}.object_id_seq', o.max_val + 1, false)
FROM
    (SELECT COALESCE(max(id), 0) AS max_val FROM {SCHEMA_NAME}.object) o;

ALTER TABLE {SCHEMA_NAME}.object
    ALTER COLUMN id SET DEFAULT nextval('{SCHEMA_NAME}.object_id_seq');
