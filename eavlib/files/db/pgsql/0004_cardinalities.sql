-- -*- mode: sql; sql-product: postgres; indent-tabs-mode: nil -*-


-- attribute required or not
ALTER TABLE {SCHEMA_NAME}.model_attribute
    ADD COLUMN required boolean NOT NULL DEFAULT false;


-- relation cardinalities
ALTER TABLE {SCHEMA_NAME}.model_relation
    ADD COLUMN card_min smallint NOT NULL DEFAULT 0 CHECK (card_min >= 0);

ALTER TABLE {SCHEMA_NAME}.model_relation
    ADD COLUMN card_max smallint NOT NULL DEFAULT 0 CHECK (card_max >= 0);
