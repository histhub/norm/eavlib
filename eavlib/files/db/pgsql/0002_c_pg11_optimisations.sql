-- -*- mode: sql; sql-product: postgres; indent-tabs-mode: nil -*-

DO LANGUAGE plpgsql $$
DECLARE
   version integer[] = (
      SELECT string_to_array(
         substring(version() from '^PostgreSQL (\d+(\.\d+)*)'), '.')
      );
BEGIN
   RAISE NOTICE 'Detected PostgreSQL version: %', version;

   IF version >= ARRAY[11] THEN
      EXECUTE $IDX1$
         DROP INDEX IF EXISTS object_entity_id_idx;
         CREATE INDEX object_entity_id_idx
             ON {SCHEMA_NAME}.object USING btree
             ("entity_id") INCLUDE ("id");
      $IDX1$;

      EXECUTE $IDX2$
         DROP INDEX IF EXISTS object_relation_object_relation_idx;
         CREATE INDEX object_relation_object_relation_idx
             ON {SCHEMA_NAME}.object_relation USING btree
             ("object_id", "relation_id") INCLUDE ("target_id");
      $IDX2$;

      EXECUTE $IDX3$
         DROP INDEX IF EXISTS object_relation_target_relation_idx;
         CREATE INDEX IF NOT EXISTS object_relation_target_relation_idx
             ON {SCHEMA_NAME}.object_relation USING btree
             ("target_id", "relation_id") INCLUDE ("object_id");
      $IDX3$;
   ELSE
      RAISE WARNING 'Your PostgreSQL version is smaller than 11. '
                    'Covering indexes cannot be used. '
                    'Please run the 0003_c_pg11_optimisations.sql scripts '
                    'after upgrading to PostgreSQL 11.';
   END IF;
END
$$;
