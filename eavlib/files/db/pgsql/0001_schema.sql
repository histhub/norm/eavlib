-- -*- mode: sql; sql-product: postgres; indent-tabs-mode: nil -*-

--
-- NOTE: Create the schema beforehand:
-- CREATE SCHEMA {SCHEMA_NAME};
--


-- Any data model used to store information in this EAV repository has an entry
-- in this table.

CREATE TABLE {SCHEMA_NAME}.model (
    id          integer NOT NULL,
    name        text    NOT NULL,
    description text,

    CONSTRAINT model_pkey PRIMARY KEY (id),
    CONSTRAINT model_name_must_be_unique UNIQUE (name)
);


-- Stores the different entities that are defined by a data model.

CREATE TABLE {SCHEMA_NAME}.model_entity (
    id          integer PRIMARY KEY,
    model_id    integer NOT NULL REFERENCES {SCHEMA_NAME}.model(id),
    name        text    NOT NULL,
    description text
);


-- Stores the inheritance relation among the entities defined by a data model.

CREATE TABLE {SCHEMA_NAME}.model_inheritance (
    entity_id integer NOT NULL REFERENCES {SCHEMA_NAME}.model_entity(id),
    parent_id integer NOT NULL REFERENCES {SCHEMA_NAME}.model_entity(id)
);


-- Stores the different attributes an entity can have.

CREATE TABLE {SCHEMA_NAME}.model_attribute (
    id          integer PRIMARY KEY,
    entity_id   integer NOT NULL REFERENCES {SCHEMA_NAME}.model_entity(id),
    name        text    NOT NULL,
    description text
);


-- Stores the relationships among entities that are defined by a data model.

CREATE TABLE {SCHEMA_NAME}.model_relation (
    id          integer PRIMARY KEY,
    source_id   integer NOT NULL REFERENCES {SCHEMA_NAME}.model_entity(id),
    target_id   integer NOT NULL REFERENCES {SCHEMA_NAME}.model_entity(id),
    name        text    NOT NULL,
    description text
);


-- Store concrete instances of model entities.

CREATE TABLE {SCHEMA_NAME}.object (
    id        integer PRIMARY KEY,
    entity_id integer NOT NULL REFERENCES {SCHEMA_NAME}.model_entity(id)
);


-- Store attributes of entity instances.

CREATE TABLE {SCHEMA_NAME}.object_attribute (
    object_id    integer NOT NULL REFERENCES {SCHEMA_NAME}.object(id),
    attribute_id integer NOT NULL REFERENCES {SCHEMA_NAME}.model_attribute(id),
    value        text    NOT NULL,

    CONSTRAINT attributes_are_single_valued UNIQUE (object_id, attribute_id)
);


-- Store relations between entity instances.

CREATE TABLE {SCHEMA_NAME}.object_relation (
    object_id   integer NOT NULL REFERENCES {SCHEMA_NAME}.object(id),
    relation_id integer NOT NULL REFERENCES {SCHEMA_NAME}.model_relation(id),
    target_id   integer NOT NULL REFERENCES {SCHEMA_NAME}.object(id)
);


-- Create indices:

CREATE INDEX attributes_object_ids_index
ON {SCHEMA_NAME}.object_attribute (object_id);

CREATE INDEX relations_object_ids_index
ON {SCHEMA_NAME}.object_relation (object_id);

CREATE INDEX attributes_key_value_index
ON {SCHEMA_NAME}.object_attribute (attribute_id, value);
