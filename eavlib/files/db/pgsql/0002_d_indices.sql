-- -*- mode: sql; sql-product: postgres; indent-tabs-mode: nil -*-


--- model_attribute table

CREATE INDEX model_attribute_name_index
    ON {SCHEMA_NAME}.model_attribute USING btree
    ("name", "entity_id");


--- model_relation table

CREATE INDEX model_relation_source_name_idx
    ON {SCHEMA_NAME}.model_relation USING btree
    ("source_id", "name");

CREATE INDEX model_relation_name_target_idx
    ON {SCHEMA_NAME}.model_relation USING btree
    ("name", "target_id");


--- object table

-- NOTE: For PgSQL < 11. For PgSQL >= 11, see 0003_c_pg11_optimisations.sql.
CREATE INDEX IF NOT EXISTS object_entity_id_idx
    ON {SCHEMA_NAME}.object USING btree
    ("entity_id");


--- object_attribute table

CREATE INDEX object_attribute_object_attribute_value_idx
    ON {SCHEMA_NAME}.object_attribute USING btree
    ("object_id", "attribute_id", "value");

ALTER TABLE {SCHEMA_NAME}.object_attribute
    CLUSTER ON object_attribute_object_attribute_value_idx;

CREATE INDEX object_attribute_key_value_idx
    ON {SCHEMA_NAME}.object_attribute USING btree
    ("attribute_id", "value");


--- object_relation table


-- NOTE: For PgSQL < 11. For PgSQL >= 11, see 0003_c_pg11_optimisations.sql.
CREATE INDEX IF NOT EXISTS object_relation_object_relation_idx
    ON {SCHEMA_NAME}.object_relation USING btree
    ("object_id", "relation_id");

ALTER TABLE {SCHEMA_NAME}.object_relation
    CLUSTER ON object_relation_object_relation_idx;

-- NOTE: For PgSQL < 11. For PgSQL >= 11, see 0003_c_pg11_optimisations.sql.
CREATE INDEX IF NOT EXISTS object_relation_target_relation_idx
    ON {SCHEMA_NAME}.object_relation USING btree
    ("target_id", "relation_id");
