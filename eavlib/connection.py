import abc
import logging
import psycopg2
import psycopg2.sql

from typing import (Any, Iterable, Optional, Type, Union)

from eavlib.core import EavConnection as LegacyEavConnection
from eavlib.postgres.sql_query_dict import SqlQueryDict
from eavlib.db.url import (make_url, URL)

LOG = logging.getLogger(__name__)


class EAVConnection(metaclass=abc.ABCMeta):
    """An EAVConnection encapsulates a Python DB API 2.0 connection and
    provides a backend agnostic way to work with databases.

    The EAVConnection class is abstract. Its __new__ method will instantiate
    the right subclass based on the URI passed to it.
    """

    __slots__ = ("connection", "schema")

    def __new__(cls, url: URL) -> "EAVConnection":
        """Creates the matching EAVConnection subclass.

        :param url: The URL to the database
        :type url: URL
        """
        if cls is not EAVConnection:
            # __new__ of subclasses will fall through to this __new__ and
            # create infinite recursion
            return object.__new__(cls)  # type: ignore

        dialect = url.get_backend_name()

        real_cls = None  # type: Optional[Type[EAVConnection]]
        if dialect == "postgresql":
            real_cls = PostgresEAVConnection

        if real_cls is not None:
            return real_cls.__new__(real_cls, url)

        raise NotImplementedError("DB dialect %s is not supported" % dialect)

    def __init__(self, url: URL) -> None:
        self.url = url

    @classmethod
    def fromstr(cls, url: str) -> "EAVConnection":
        """Creates a new EAVConnection from a URL string.

        The type is guessed automatically.

        :param url: the URL string
        :type url: str
        :returns: EAVConnection -- the EAVConnection
        """
        return EAVConnection(make_url(url))

    @classmethod
    def fromconnection(cls, conn: Any, schema: str) -> "EAVConnection":
        """Creates a new EAVConnection from an existing open
        database connection.

        The type is guessed automatically.

        NOTE: The connection is not going to be cloned.

        :param conn: the open database connection
        :param schema: the schema name to use
        :type schema: str
        :returns: EAVConnection -- the EAVConnection
        """
        if isinstance(conn, psycopg2.extensions.connection):
            return PostgresEAVConnection.fromconnection(conn, schema)

        raise NotImplementedError("Could not create an EAVConnection from %r. "
                                  "It is no known connection object")

    def copy(self) -> "EAVConnection":
        return EAVConnection(self.url)

    @abc.abstractmethod
    def legacy_connection(self) -> LegacyEavConnection:
        """

        """

    @abc.abstractmethod
    def close(self) -> None:
        """

        """

    @abc.abstractmethod
    def cursor(self, *args, **kwargs) -> Any:
        """

        """

    @abc.abstractmethod
    def dict_cursor(self, *args, **kwargs) -> Any:
        """

        """

    @abc.abstractmethod
    def commit(self, *args, **kwargs) -> None:
        """

        """

    @abc.abstractmethod
    def rollback(self, *args, **kwargs) -> None:
        """

        """

    @abc.abstractmethod
    def query(self, query: Union[str, psycopg2.sql.SQL], **kwargs: Any) \
            -> Iterable[Any]:
        """

        """

    @property
    def closed(self):
        return self.connection.closed


class PostgresEAVConnection(EAVConnection):
    def __init__(self, url: URL) -> None:
        super().__init__(url)

        try:
            self.connection = psycopg2.connect(
                **url.translate_connect_args())
            self.connection.set_client_encoding("UTF8")
        except psycopg2.OperationalError as e:
            # psycopg2 OperationalError exceptions can be prefixed with the
            # log level -> we strip it away
            error_msg = str(e)
            if error_msg.startswith("FATAL:  "):
                raise RuntimeError(error_msg[8:])
            raise

        self.schema = url.schema
        self.query_dict = SqlQueryDict(self.schema)

        self.reread_metadata()

    @classmethod
    def fromconnection(cls, conn: psycopg2.extensions.connection,
                       schema: str) -> "PostgresEAVConnection":
        """Creates a new PostgresEAVConnection from an existing open
        database connection.

        :param conn: the open database connection
        :type conn: psycopg2.extensions.connection
        :param schema: the schema name to use
        :type schema: str
        :returns: PostgresEAVConnection -- the EAVConnection
        """
        obj = object.__new__(cls)
        obj.connection = conn
        obj.schema = schema
        obj.query_dict = SqlQueryDict(schema)
        return obj

    def legacy_connection(self) -> LegacyEavConnection:
        return LegacyEavConnection(self.connection, self.query_dict)

    def reread_metadata(self) -> None:
        with self.connection.cursor() as cursor:
            cursor.execute(self.query_dict.storage_is_initialised_query)
            self.is_initialised = bool(cursor.fetchone())
            self.schema_version = None
            if self.is_initialised:
                cursor.execute(self.query_dict.storage_read_version_query)
                # NOTE: The int cast must work if the schema is
                # initialised. Otherwise the schema is to be considered broken
                # and should not be used.
                self.schema_version = int(cursor.fetchone()[0])

    def close(self) -> None:
        _conn = self.connection
        self.connection = None
        _conn.close()

    def cursor(self, *args, **kwargs) -> psycopg2.extensions.cursor:
        return self.connection.cursor(*args, **kwargs)

    def dict_cursor(self, *args, **kwargs) -> psycopg2.extensions.cursor:
        import psycopg2.extras

        # FIXME: Check what's wrong with DictCursor...
        # Sometimes the keys() are empty...
        # Switched to RealDictCursor as a workaround

        return self.connection.cursor(
            *args, **kwargs, cursor_factory=psycopg2.extras.RealDictCursor)

    def commit(self, *args, **kwargs) -> None:
        self.connection.commit(*args, **kwargs)

    def rollback(self, *args, **kwargs) -> None:
        self.connection.rollback(*args, **kwargs)

    def query(self, query: Union[str, psycopg2.sql.SQL], **kwargs: Any) \
            -> psycopg2.extensions.cursor:
        """Execute a SQL query on this EAVConnection.

        Please note that only keyworded query parameters are supported.

        Attention: This method returns an open DB cursor. It is the caller's
        job to ensure it gets closed.

        :param query: The query string to execute.
        :type query: Union[str, psycopg2.sql.SQL]
        :param **kwargs: The parameters to the query
        :returns: psycopg2.cursor -- the cursor instance after executing the
        query.
        """
        # query uses named server cursors. This allows to return an iterator
        # to be consumed by the caller as needed.

        cursor = self.dict_cursor()

        try:
            cursor.execute(query, kwargs)
        except psycopg2.ProgrammingError:
            LOG.warning(
                "There was an error, the transaction will be rolled back")
            self.connection.rollback()
            raise

        return cursor
