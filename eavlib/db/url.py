# This module originates from SQLAlchemy and has been modified to work better
# with eavlib.
#
# Copyright (c) 2005-2019 the SQLAlchemy authors and contributors
# SQLAlchemy is a trademark of Michael Bayer.
#
# SQLAlchemy is released under the MIT License:
# http://www.opensource.org/licenses/mit-license.php
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


"""Provides the :class:`~eavlib.db.url.URL` class which encapsulates
information about a database connection specification.
"""

import collections.abc
import re
import urllib.parse

from typing import (Any, AnyStr, Dict, Optional, Union)


class URL:
    """
    Represent the components of a URL used to connect to a database.

    This object is suitable to be passed directly to a
    :func:`~sqlalchemy.create_engine` call.  The fields of the URL are parsed
    from a string by the :func:`.make_url` function.  the string
    format of the URL is an RFC-1738-style string.

    All initialization parameters are available as public attributes.

    :param drivername: the name of the database backend.
      This name will correspond to a module in sqlalchemy/databases
      or a third party plug-in.

    :param username: The user name.

    :param password: database password.

    :param host: The name of the host.

    :param port: The port number.

    :param database: The database name.

    :param schema: The schema name.

    :param query: A dictionary of options to be passed to the
      dialect and/or the DBAPI upon connect.

    """

    __slots__ = (
        "drivername", "username", "password_original", "host", "port",
        "database", "schema", "query")

    def __init__(self, drivername: str, username: Optional[str] = None,
                 password: Optional[str] = None, host: Optional[str] = None,
                 port: Optional[int] = None, database: Optional[str] = None,
                 schema: Optional[str] = None, query: Optional[dict] = None):
        self.drivername = drivername
        self.username = username
        self.password_original = password
        self.host = host
        self.port = int(port) if port is not None else None
        self.database = database
        self.schema = schema
        self.query = query or dict()

    def copy(self) -> "URL":
        params = {}
        for k in self.__slots__:
            if k == "password_original":
                params["password"] = self.password
            else:
                params[k] = getattr(self, k)

        return self.__class__(**params)

    def __to_string__(self, hide_password=True) -> str:
        s = self.drivername + "://"
        if self.username is not None:
            s += _rfc_1738_quote(self.username)
            if self.password is not None:
                s += ":" + (
                    "***" if hide_password else _rfc_1738_quote(self.password)
                )
            s += "@"
        if self.host is not None:
            if ":" in self.host:
                s += "[%s]" % self.host
            else:
                s += self.host
        if self.port is not None:
            s += ":" + str(self.port)
        if self.database is not None:
            s += "/" + self.database
        if self.schema is not None:
            s += "." + self.schema
        if self.query:
            s += "?" + "&".join(
                "%s=%s" % (k, element)
                for k in sorted(self.query)
                for element in to_list(self.query[k]))
        return s

    def __str__(self) -> str:
        return self.__to_string__(hide_password=False)

    def __repr__(self) -> str:
        return self.__to_string__()

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other) -> bool:
        if isinstance(other, URL):
            return all((
                self.drivername == other.drivername,
                self.username == other.username,
                self.password == other.password,
                self.host == other.host,
                self.database == other.database,
                self.query == other.query))
        return False

    @property
    def password(self) -> Optional[str]:
        if self.password_original is not None:
            return str(self.password_original)
        return None

    @password.setter
    def password(self, password: str) -> None:
        self.password_original = password

    def get_backend_name(self) -> str:
        return self.drivername.split("+")[0]

    def get_driver_name(self) -> str:
        return self.drivername.split("+")[1]

    def translate_connect_args(self, **kwargs: Any) -> Dict[str, Any]:
        r"""Translate url attributes into a dictionary of connection arguments.

        Returns attributes of this url (`host`, `database`, `username`,
        `password`, `port`) as a plain dictionary.  The attribute names are
        used as the keys by default.  Unset or false attributes are omitted
        from the final dictionary.

        :param \**kwargs: Optional, alternate key names for url attributes.
        """

        translated = dict()
        attribute_names = ["host", "database", "username", "password", "port"]
        for sname in attribute_names:
            name = kwargs.get(sname, sname)
            if name is not None and getattr(self, sname, False):
                if name == "username":
                    # Allows to pass values directly into psycopg.connect
                    name = "user"
                translated[name] = getattr(self, sname)
        return translated


def make_url(name_or_url: Union[str, URL]) -> URL:
    """Given a string or unicode instance, produce a new URL instance.

    The given string is parsed according to the RFC 1738 spec.  If an
    existing URL object is passed, just returns the object.
    """

    if isinstance(name_or_url, str):
        return _parse_rfc1738_args(name_or_url)
    elif isinstance(name_or_url, URL):
        return name_or_url
    else:
        raise TypeError("Expected URL string or URL object.")


def _parse_rfc1738_args(name: str) -> URL:
    pattern = re.compile(
        r"""^
            (?P<name>[\w\+]+)://
            (?:
                (?P<username>[^:/]*)
                (?::(?P<password>.*))?
            @)?
            (?:
                (?:
                    \[(?P<ipv6host>[^/]+)\] |
                    (?P<ipv4host>[^/:]+)
                )?
                (?::(?P<port>[^/]*))?
            )?
            /(?:
                (?:(?P<database>[^.?]+))
                (?:\.(?P<schema>[^?]*))?
            )?
            (?:\?(?P<query>.*))?
            $""", re.X)

    m = pattern.match(name)
    if m is not None:
        components = m.groupdict()
        if components["query"] is not None:
            query = dict()
            for key, value in urllib.parse.parse_qsl(components["query"]):
                if key in query:
                    query[key] = (query[key],) + (value,)
                else:
                    query[key] = value
            components["query"] = query
            del query

        if components["username"] is not None:
            components["username"] = _rfc_1738_unquote(components["username"])

        if components["password"] is not None:
            components["password"] = _rfc_1738_unquote(components["password"])

        ipv4host = components.pop("ipv4host")
        ipv6host = components.pop("ipv6host")
        components["host"] = ipv4host or ipv6host
        name = components.pop("name")
        return URL(name, **components)
    else:
        raise ValueError(
            "Could not parse rfc1738 URL from string '%s'" % name)


def _rfc_1738_quote(text: AnyStr) -> AnyStr:
    return re.sub(r"[:@/]", lambda m: "%%%X" % ord(m.group(0)), text)


def _rfc_1738_unquote(text: str) -> str:
    return urllib.parse.unquote(text)


def _parse_keyvalue_args(name: str) -> Optional[URL]:
    m = re.match(r"(\w+)://(.*)", name)
    if m is not None:
        (name, args) = m.group(1, 2)
        opts = dict(urllib.parse.parse_qsl(args))
        return URL(name, *opts)
    return None


def to_list(x: Any, default: Optional[Any] = None) -> list:
    if x is None:
        return default
    elif not isinstance(x, collections.abc.Iterable) \
            or isinstance(x, (str, bytes)):
        return [x]
    return list(x)
