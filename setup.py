#!./venv/bin/python

import codecs
import os

from setuptools import setup, find_packages

package_name = "eavlib"
min_python_version = (3, 6)

base = os.path.abspath(os.path.dirname(__file__))

try:
    from pip._internal.req import parse_requirements
except ImportError:
    try:
        from pip.req import parse_requirements
    except ImportError:
        raise RuntimeError("Could not find pip requirements parser")


def read_file(path):
    """Read file and return contents"""
    try:
        with codecs.open(path, "r", encoding="utf-8") as f:
            return f.read()
    except IOError:
        return ""


def get_reqs():
    """Parse requirements.txt file and return list"""
    reqs = parse_requirements(
        os.path.join(base, "requirements.txt"),
        session="hack")
    return [
        r.requirement if hasattr(r, "requirement") else str(r.req)
        for r in reqs]


def get_meta(name):
    """Get metadata from project's __init__.py"""
    return getattr(__import__(package_name), name)


setup(
    name=package_name,
    version=get_meta("__version__"),
    license=get_meta("__license__"),

    description=get_meta("__description__"),
    long_description=read_file(os.path.join(base, "README.md")),

    classifiers=[
        "Development Status :: 3 - Alpha",
        "Programming Language :: Python",
        # TODO: Add missing classifiers
    ],

    install_requires=get_reqs(),

    packages=find_packages(),
    python_requires=(">=%s" % ".".join(map(str, min_python_version))),

    package_data={"eavlib": ["py.typed", "files/db/*/*.sql"]},

    scripts=[("bin/%s" % script) for script in (
        "eavsh",
        "eav_create",
        "eav_model_delete",
        "eav_model_dump",
        "eav_model_load",
        "eav_to_owl"
    )])
