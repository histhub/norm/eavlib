#!/usr/bin/env python3

import os

from argparse import (ArgumentParser, RawDescriptionHelpFormatter)
from textwrap import dedent

import eavlib.connection

from eavlib.util import quit_on_error
from eavlib.core import ModelManager

try:
    from math import inf
except ImportError:
    inf = float("inf")


def concat_props(props_list, obj):
    '''
        "{prop}: {value}"-concatenation of the given properties
        of an object as a single YAML line.
    '''
    def hacked_getattr(obj, prop):
        value = getattr(obj, prop)
        return '.inf' if value is inf else value

    return ', '.join(
        ("{}: {}".format(n, v) for (n, v) in zip(
            props_list, map(lambda _: hacked_getattr(obj, _), props_list))
         if v is not None))


def _render_model_yaml(model):
    yield 'model:'

    yield "  name        : {}   # {}".format(model.name, model.id)
    yield "  description : {}".format(model.description)
    yield '  entities    :'
    yield ''

    for entity in model.entities:
        yield "  - name: {}   # {}".format(entity.name, entity.id)
        if len(entity.parents) > 0:
            yield "    parents: {}".format(', '.join(entity.parents))
        if entity.description is not None:
            yield "    description: {}".format(entity.description)

        if len(entity.attributes) > 0:
            yield "    attributes:"
            for attr in entity.attributes:
                yield "      - {{ {} }}   # {}".format(concat_props(
                    ['name', 'required', 'description'], attr), attr.id)

        if len(entity.relations) > 0:
            yield '    relations:'
            for relation in entity.relations:
                yield "      - {{ {} }}   # {}".format(concat_props(
                    ['name', 'target', 'card_min', 'card_max',
                     'description'], relation), relation.id)


if __name__ == '__main__':
    args_parser = ArgumentParser(
        formatter_class=RawDescriptionHelpFormatter,
        description=dedent('''
        Dumps an EAV model already present in an EAV store as a YAML file.
        ---------------------------------------------------------------------
        The dumped YAML has comments showing the database id for each entity,
        attribute and relation.
        '''))
    args_parser.add_argument(
        'connection',
        help='database connection string in the form '
             '"{engine}://{user}@{host}:{port}/{dbname}.{schema}"')
    args_parser.add_argument(
        '-m', '--model',
        required=True,
        action='store',
        dest='model',
        help='model name or its numeric id')
    args = args_parser.parse_args()

    try:
        connection = eavlib.connection.EAVConnection.fromstr(args.connection)
        model_manager = ModelManager(connection)
        try:
            model_id = int(args.model)
        except ValueError:
            model_id = model_manager.id_by_name(args.model)

        model = model_manager.read(model_id)
        print(*_render_model_yaml(model), sep=os.linesep)

    except Exception as e:
        # from traceback import print_exc
        # print_exc()
        quit_on_error(str(e))
