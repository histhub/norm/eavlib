# eavlib

eavlib is part of the histHub database and comprises the parts of the code
which are responsible to create and modify the histHub database.
While the code has been written for the [histHub](http://www.histhub.ch) project
it is designed to be generic.

To get a fully histHub-compatible database, check out
[normlib](https://gitlab.com/histhub/norm/normlib.git).

The database is a data structure like a property graph built on top of a EAV/CR
model inside the PostgreSQL RDBMS.
Inside a RDBMS there can be multiple graphs (each model is its own graph).
The graph can be modified using the built-in batch APIs and queried using the
built-in [Gremlin](https://tinkerpop.apache.org)-to-SQL transpiler.

This repository also contains the following utilities:
* `eav_create` to format an empty PostgreSQL schema.
* `eav_model_load` to load a YAML-model (e.g.
  [histhub-ontology](https://gitlab.com/histhub/norm/histhub-ontology.git)) into
  the database.
* `eav_model_dump` to dump a loaded model into a YAML file.
* `eav_model_delete` to delete a model (=a graph) from the database.
* `eavsh` is an interactive REPL that can be used to inspect graphs stored in a
  database or for debugging.

## Getting started

To create your own graph using eavlib, you need:

* CPython >= 3.6
* PostgreSQL >= 10
* a model (cf. [histHub ontology](https://gitlab.com/histhub/norm/histhub-ontology.git))

Once you have these, you can install `eavlib` like a regular Python package
using `pip`:
```console
$ pip install eavlib@git+https://gitlab.com/histhub/norm/eavlib.git
```

Then, set up a database and model, e.g.:
```console
$ DB_URL=postgresql://postgres@localhost/postgres.graph
$ eav_create "${DB_URL}"
$ eav_model_load "${DB_URL}" -f /path/to/model.yaml
```
**Note:** `DB_URL` is a
[SQLAlchemy-like](https://docs.sqlalchemy.org/en/13/core/engines.html#sqlalchemy.create_engine)
URL string with extended `database.schema` notation.

Now the graph is ready to be used.

To batch load your existing data, the
[TSV-Loader](https://gitlab.com/histhub/norm/tsv-loader.git) can be used.

You can also inspect the graph using `eavsh`, e.g.:
```console
$ eavsh --url "${DB_URL}" --model mymodel
```
